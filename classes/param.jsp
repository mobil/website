<%@ page language="Java" import='java.io.*,java.net.*' %> 
<%

// String handler		= "http://192.168.0.201/mqs/test/param.php?" + request.getQueryString();
 String handler		= "http://www.mobilquattrosud.com/dbinterface/test/param.php?stream=1&" + request.getQueryString();
 String prefix		= request.getRealPath("/") + "my-apps/website/dati/";
 boolean creaFileXml= true;
 
 String delParamName="deleteDataId";
 	
  if(request.getParameter("stream") != null)
 	creaFileXml= false;
 	
  if(request.getParameter(delParamName) != null)
  {
  	String fileName = prefix + request.getParameter(delParamName);
  	File f = new File(fileName);

    //Il file non esiste  = -1
    if (!f.exists())
    {
           out.println("-1");
           return;
    }
    //Non ho i diritti di scrittura = -2
    if (!f.canWrite())
    {
      out.println("-2");
      return;
    }
    // Tentativo di cancellazione del file
    if(f.delete())
	out.println("1");
    else
    	out.println("0");
    return;
  }
 
 URL url = null;
 try
 {
 	url = new URL(handler);
 	InputStream stream = url.openStream();
 	byte[] arr = new byte[stream.available()];
 	int letti = 0;
 	StringBuilder sb = new StringBuilder();
	String  tmp = null;
 	while((letti = stream.read(arr))!= -1)
 	{
		tmp = new String(arr, 0, letti);
 		sb.append(tmp.replace("\n",""));
 		//out.println("\n<br />letti: "+letti+ " = <br />" + tmp);
 	}
 	//out.println("\n<br />letti: "+letti);
 	//stream.read(arr);
 	if(creaFileXml)
		process(sb.toString(), request, out);
	else
		out.println(sb.toString());
 }
 catch(MalformedURLException e)
 {
 	out.println("La url è malformata!");
 }
 catch(IOException e)
 {
 	out.println("<IOEx> Non riesco a leggere lo stream");
 }
 %>
<%!
 public void process(String inner, HttpServletRequest request, javax.servlet.jsp.JspWriter out) throws IOException
 {
    try  
    {     
            int number = (int) (Math.random() * 10000);
            String prefix = request.getRealPath("/") + "my-apps/website/dati/";//basePathD;
            String name = number + ".xml";
            String path = prefix + name;
            PrintWriter updates = new PrintWriter(new BufferedWriter(new FileWriter(path)), true);
            updates.println(inner);
            updates.close();
            out.println(name);
	    //StringBuilder sb = new StringBuilder("<Richiesta numero=\"");
	    //sb.append(number);
	    //sb.append("\">");
	    //sb.append(inner);
	    //updates = new PrintWriter(new BufferedWriter(new FileWriter(prefix+"logger.log")), true);
	    //updates.println(sb.toString());
            //updates.close();
            //out.println("<br/>" + request.getRealPath("/") + "<br/>");
            //out.println( path + "<br/>");
    }  
    catch(Exception e)
    {   
            out.println(e.toString());
            return;
    }
 }
%>