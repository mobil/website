LzResourceLibrary.lzfocusbracket_rsrc={ptype:"sr",frames:["lps/components/lz/resources/focus/focus_top_lft.png","lps/components/lz/resources/focus/focus_top_rt.png","lps/components/lz/resources/focus/focus_bot_lft.png","lps/components/lz/resources/focus/focus_bot_rt.png"],width:7,height:7,sprite:"lps/components/lz/resources/focus/focus_top_lft.sprite.png",spriteoffset:0};LzResourceLibrary.lzfocusbracket_shdw={ptype:"sr",frames:["lps/components/lz/resources/focus/focus_top_lft_shdw.png","lps/components/lz/resources/focus/focus_top_rt_shdw.png","lps/components/lz/resources/focus/focus_bot_lft_shdw.png","lps/components/lz/resources/focus/focus_bot_rt_shdw.png"],width:9,height:9,sprite:"lps/components/lz/resources/focus/focus_top_lft_shdw.sprite.png",spriteoffset:7};LzResourceLibrary.lzscrollbar_xthumbleft_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/scrollthumb_x_lft.png"],width:1,height:12,spriteoffset:16};LzResourceLibrary.lzscrollbar_xthumbmiddle_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/autoPng/scrollthumb_x_mid.png"],width:6,height:12,spriteoffset:28};LzResourceLibrary.lzscrollbar_xthumbright_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/scrollthumb_x_rt.png"],width:1,height:12,spriteoffset:40};LzResourceLibrary.lzscrollbar_xthumbgripper_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/thumb_x_gripper.png"],width:440,height:9,spriteoffset:52};LzResourceLibrary.lzscrollbar_xbuttonleft_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_lft_up.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_lft_mo.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_lft_dn.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_lft_dsbl.png"],width:13,height:12,sprite:"lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_lft_up.sprite.png",spriteoffset:61};LzResourceLibrary.lzscrollbar_xbuttonright_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_rt_up.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_rt_mo.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_rt_dn.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_rt_dsbl.png"],width:13,height:12,sprite:"lps/components/lz/resources/scrollbar/autoPng/scrollbtn_x_rt_up.sprite.png",spriteoffset:73};LzResourceLibrary.lzscrollbar_xtrack_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/x_scrolltrack.png","lps/components/lz/resources/scrollbar/x_scrolltrack_dn.png","lps/components/lz/resources/scrollbar/x_scrolltrack_dsbl.png"],width:1,height:12,sprite:"lps/components/lz/resources/scrollbar/x_scrolltrack.sprite.png",spriteoffset:85};LzResourceLibrary.lzbutton_face_rsc={ptype:"sr",frames:["lps/components/lz/resources/button/simpleface_up.png","lps/components/lz/resources/button/simpleface_mo.png","lps/components/lz/resources/button/simpleface_dn.png","lps/components/lz/resources/button/autoPng/simpleface_dsbl.png"],width:2,height:18,sprite:"lps/components/lz/resources/button/simpleface_up.sprite.png",spriteoffset:97};LzResourceLibrary.lzbutton_bezel_inner_rsc={ptype:"sr",frames:["lps/components/lz/resources/autoPng/bezel_inner_up.png","lps/components/lz/resources/autoPng/bezel_inner_up.png","lps/components/lz/resources/autoPng/bezel_inner_dn.png","lps/components/lz/resources/autoPng/outline_dsbl.png"],width:500,height:500,sprite:"lps/components/lz/resources/autoPng/bezel_inner_up.sprite.png",spriteoffset:115};LzResourceLibrary.lzbutton_bezel_outer_rsc={ptype:"sr",frames:["lps/components/lz/resources/autoPng/bezel_outer_up.png","lps/components/lz/resources/autoPng/bezel_outer_up.png","lps/components/lz/resources/autoPng/bezel_outer_dn.png","lps/components/lz/resources/autoPng/transparent.png","lps/components/lz/resources/autoPng/default_outline.png"],width:500,height:500,sprite:"lps/components/lz/resources/autoPng/bezel_outer_up.sprite.png",spriteoffset:615};LzResourceLibrary.lzscrollbar_ythumbtop_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/scrollthumb_y_top.png"],width:12,height:1,spriteoffset:1115};LzResourceLibrary.lzscrollbar_ythumbmiddle_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/autoPng/scrollthumb_y_mid.png"],width:12,height:6,spriteoffset:1116};LzResourceLibrary.lzscrollbar_ythumbbottom_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/autoPng/scrollthumb_y_bot.png"],width:12,height:2,spriteoffset:1122};LzResourceLibrary.lzscrollbar_ythumbgripper_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/thumb_y_gripper.png"],width:9,height:440,spriteoffset:1124};LzResourceLibrary.lzscrollbar_ybuttontop_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_top_up.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_top_mo.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_top_dn.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_top_dsbl.png"],width:12,height:13,sprite:"lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_top_up.sprite.png",spriteoffset:1564};LzResourceLibrary.lzscrollbar_ybuttonbottom_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_bot_up.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_bot_mo.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_bot_dn.png","lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_bot_dsbl.png"],width:12,height:13,sprite:"lps/components/lz/resources/scrollbar/autoPng/scrollbtn_y_bot_up.sprite.png",spriteoffset:1577};LzResourceLibrary.lzscrollbar_ytrack_rsc={ptype:"sr",frames:["lps/components/lz/resources/scrollbar/y_scrolltrack.png","lps/components/lz/resources/scrollbar/y_scrolltrack_dn.png","lps/components/lz/resources/scrollbar/y_scrolltrack_dsbl.png"],width:12,height:1,sprite:"lps/components/lz/resources/scrollbar/y_scrolltrack.sprite.png",spriteoffset:1590};LzResourceLibrary.shadowTR={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/pop_shadow_flush_top_rt.png","lps/components/lz/resources/floatinglist/pop_shadow_corner_top_rt.png","lps/components/lz/resources/floatinglist/pop_shadow_oval_top_rt.png"],width:15,height:14,sprite:"lps/components/lz/resources/floatinglist/pop_shadow_flush_top_rt.sprite.png",spriteoffset:1591};LzResourceLibrary.shadowMR={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/pop_shadow_mid_rt.png"],width:15,height:1,spriteoffset:1605};LzResourceLibrary.shadowBL={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/autoPng/pop_shadow_bot_lft.png"],width:13,height:14,spriteoffset:1606};LzResourceLibrary.shadowBM={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/pop_shadow_bot_mid.png"],width:1,height:14,spriteoffset:1620};LzResourceLibrary.shadowBR={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/autoPng/pop_shadow_bot_rt.png"],width:15,height:14,spriteoffset:1634};LzResourceLibrary.menucap_lft={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/autoPng/topmenu_lft.png","lps/components/lz/resources/floatinglist/autoPng/botmenu_lft.png"],width:7,height:10,sprite:"lps/components/lz/resources/floatinglist/autoPng/topmenu_lft.sprite.png",spriteoffset:1648};LzResourceLibrary.menucap_mid={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/topmenu_mid.png","lps/components/lz/resources/floatinglist/botmenu_mid.png"],width:1,height:11,sprite:"lps/components/lz/resources/floatinglist/topmenu_mid.sprite.png",spriteoffset:1658};LzResourceLibrary.menucap_rt={ptype:"sr",frames:["lps/components/lz/resources/floatinglist/autoPng/topmenu_rt.png","lps/components/lz/resources/floatinglist/autoPng/botmenu_rt.png"],width:7,height:10,sprite:"lps/components/lz/resources/floatinglist/autoPng/topmenu_rt.sprite.png",spriteoffset:1669};LzResourceLibrary.menuarrow_rsrc={ptype:"sr",frames:["lps/components/lz/resources/menu/autoPng/menu_arrow_rt.png","lps/components/lz/resources/menu/autoPng/menu_arrow_lft.png"],width:6,height:8,sprite:"lps/components/lz/resources/menu/autoPng/menu_arrow_rt.sprite.png",spriteoffset:1679};LzResourceLibrary.$LZ1={ptype:"ar",frames:["assets/details/taglink_icon.gif"],width:8.990936279296875,height:8.990936279296875,spriteoffset:1687};LzResourceLibrary.$LZ2={ptype:"ar",frames:["assets/clipboard/tplft.png"],width:94,height:14,spriteoffset:1696};LzResourceLibrary.$LZ3={ptype:"ar",frames:["assets/clipboard/tp.png"],width:1,height:11,spriteoffset:1710};LzResourceLibrary.$LZ4={ptype:"ar",frames:["assets/clipboard/tprgt.png"],width:32,height:22,spriteoffset:1721};LzResourceLibrary.$LZ5={ptype:"ar",frames:["assets/clipboard/lft.png"],width:91,height:1,spriteoffset:1743};LzResourceLibrary.$LZ6={ptype:"ar",frames:["assets/clipboard/rgt.png"],width:25,height:1,spriteoffset:1744};LzResourceLibrary.$LZ7={ptype:"ar",frames:["assets/search/title-left.gif"],width:2.996978759765625,height:22.976837158203125,spriteoffset:1745};LzResourceLibrary.$LZ8={ptype:"ar",frames:["assets/search/title-mid.gif"],width:0.998992919921875,height:22.976837158203125,spriteoffset:1768};LzResourceLibrary.$LZ9={ptype:"ar",frames:["assets/search/title-right.gif"],width:1.99798583984375,height:22.976837158203125,spriteoffset:1791};LzResourceLibrary.$LZ10={ptype:"ar",frames:["assets/search/content-mid.gif"],width:0.998992919921875,height:22.976837158203125,spriteoffset:1814};LzResourceLibrary.$LZ11={ptype:"ar",frames:["assets/search/content-right.gif"],width:1.99798583984375,height:22.976837158203125,spriteoffset:1837};LzResourceLibrary.spinner_rsc={ptype:"ar",frames:["assets/spinner/pict4.png","assets/spinner/pict1.png","assets/spinner/pict2.png","assets/spinner/pict6.png","assets/spinner/pict5.png","assets/spinner/pict3.png"],width:119,height:119,sprite:"assets/spinner/pict4.sprite.png",spriteoffset:1860};LzResourceLibrary.mybutton_rsc={ptype:"ar",frames:["views/resources/button/mybutton_mouseup.png","views/resources/button/mybutton_mouseover.png","views/resources/button/mybutton_mousedown.png","views/resources/button/mybutton_disabled.png"],width:32,height:32,sprite:"views/resources/button/mybutton_mouseup.sprite.png",spriteoffset:1979};LzResourceLibrary.icon_plus_rsc={ptype:"ar",frames:["assets/clipboard/grow_plus_up.gif","assets/clipboard/grow_plus_ov.gif","assets/clipboard/grow_plus_dn.gif"],width:13,height:13,sprite:"assets/clipboard/grow_plus_up.sprite.png",spriteoffset:2011};LzResourceLibrary.icon_minus_rsc={ptype:"ar",frames:["assets/clipboard/grow_minus_up.gif","assets/clipboard/grow_minus_ov.gif","assets/clipboard/grow_minus_dn.gif"],width:13,height:13,sprite:"assets/clipboard/grow_minus_up.sprite.png",spriteoffset:2025};LzResourceLibrary.hidedetails_rsc={ptype:"ar",frames:["views/resources/hidedetails/hidedetails.png","views/resources/hidedetails/hidedetails_mouseover.png","views/resources/hidedetails/hidedetails_mousedown.png"],width:11,height:10,sprite:"views/resources/hidedetails/hidedetails.sprite.png",spriteoffset:2039};LzResourceLibrary.prevpgae_rsc={ptype:"ar",frames:["assets/tools/page-left-up.gif","assets/tools/page-left-ov.gif","assets/tools/page-left-dn.gif"],width:30,height:21,sprite:"assets/tools/page-left-up.sprite.png",spriteoffset:2049};LzResourceLibrary.nextpage_rsc={ptype:"ar",frames:["assets/tools/page-right-up.gif","assets/tools/page-right-ov.gif","assets/tools/page-right-dn.gif"],width:30,height:21,sprite:"assets/tools/page-right-up.sprite.png",spriteoffset:2071};LzResourceLibrary.transparent_rsc={ptype:"ar",frames:["assets/transparent.png","assets/transparent.png","assets/transparent.png"],width:1,height:1,sprite:"assets/transparent.sprite.png",spriteoffset:2093};LzResourceLibrary.$LZ12={ptype:"ar",frames:["assets/beveled-divider.gif"],width:0.998992919921875,height:1.99798583984375,spriteoffset:2094};LzResourceLibrary.$LZ13={ptype:"ar",frames:["assets/details/hide.png"],width:98,height:15,spriteoffset:2096};LzResourceLibrary.$LZ14={ptype:"ar",frames:["assets/shadow.png"],width:1,height:6,spriteoffset:2111};LzResourceLibrary.$LZ15={ptype:"ar",frames:["assets/branding/big_mobil.png"],width:387,height:299,spriteoffset:2117};LzResourceLibrary.$LZ16={ptype:"ar",frames:["assets/branding/lil_mobil.png"],width:115,height:89,spriteoffset:2416};LzResourceLibrary.$LZ17={ptype:"ar",frames:["assets/branding/logo1r.png"],width:195,height:60,spriteoffset:2505};LzResourceLibrary.$LZ18={ptype:"ar",frames:["assets/branding/logo2r.png"],width:230,height:65,spriteoffset:2565};LzResourceLibrary.$LZ19={ptype:"ar",frames:["assets/branding/logo3r.png"],width:143,height:69,spriteoffset:2630};LzResourceLibrary.$LZ20={ptype:"ar",frames:["assets/tools/zoom-bkgnd.gif"],width:131.8670654296875,height:30.968780517578125,spriteoffset:2699};LzResourceLibrary.$LZ21={ptype:"ar",frames:["assets/tools/zoom-thumb.png"],width:11,height:25,spriteoffset:2730};LzResourceLibrary.$LZ22={ptype:"ar",frames:["assets/powerup/OpenLaszlo_Logo.png"],width:273,height:60,spriteoffset:2755};LzResourceLibrary.$LZ23={ptype:"ar",frames:["assets/powerup/openMultimediaWebSuite.png"],width:61,height:60,spriteoffset:2815};LzResourceLibrary.$LZ24={ptype:"ar",frames:["assets/powerup/mysql.png"],width:116,height:60,spriteoffset:2875};LzResourceLibrary.$LZ25={ptype:"ar",frames:["assets/powerup/php.png"],width:115,height:60,spriteoffset:2935};LzResourceLibrary.$LZ26={ptype:"ar",frames:["assets/powerup/qt.png"],width:50,height:60,spriteoffset:2995};LzResourceLibrary.$LZ27={ptype:"ar",frames:["assets/powerup/kubuntu.png"],width:221,height:60,spriteoffset:3055};LzResourceLibrary.$LZ28={ptype:"ar",frames:["assets/powerup/inkscape.png"],width:62,height:60,spriteoffset:3115};LzResourceLibrary.$LZ29={ptype:"ar",frames:["assets/powerup/gplv3.png"],width:121,height:60,spriteoffset:3175};LzResourceLibrary.$LZ30={ptype:"ar",frames:["assets/powerup/lgplv3.png"],width:139,height:60,spriteoffset:3235};LzResourceLibrary.$LZ31={ptype:"ar",frames:["views/resources/error_no_txt.png"],width:306,height:78,spriteoffset:3295};LzResourceLibrary.__allcss={path:"app.sprite.png"};var interior=null;var gResultsCountLabel=null;var photoscontainer=null;var details=null;var divider=null;var bottom=null;var links=null;var topview=null;var large_cam=null;var little_cam=null;var logo1=null;var logo2=null;var logo3=null;var gSearch=null;var srch=null;var tls=null;var tools=null;var spnr=null;var scrn=null;var gClipboard=null;var gDragged=null;var aboutusView=null;var mapView=null;var storyView=null;var creditView=null;var gError=null;var gPhV=null;var gBud=null;var gGlobals=null;var gDataMan=null;var photods=null;var infods=null;var trademarkds=null;var catalogds=null;var statds=null;Class.make("$lzc$class_m4",LzCanvas,["anm_multipler",void 0,"isopen",void 0,"isUpToDate",void 0,"$m1",function($0){
with(this){
this.setAttribute("toHomeDel",new LzDelegate(this,"toHome"))
}},"toHomeDel",void 0,"$m2",function($0){
with(this){
this.setAttribute("requestData_del",new LzDelegate(this,"requestData"))
}},"requestData_del",void 0,"selectedItem",void 0,"$m3",function($0){
with(this){
var $1=lz.Browser.getInitArg("dburl");var $2=lz.Browser.getInitArg("dataurl");var $3=lz.Browser.getInitArg("imageurl");var $4=lz.Browser.getInitArg("baseurl");var $5=lz.Browser.getInitArg("initPage");if($1!=null&&$1!="undefined"&&$1!=""){
gGlobals.setAttribute("RESTSRC",$1)
};if($2!=null&&$2!="undefined"&&$2!=""){
gGlobals.setAttribute("DATAURL",$2)
};if($3!=null&&$3!="undefined"&&$3!=""){
gGlobals.setAttribute("IMAGESRC",$3)
};if($4!=null&&$4!="undefined"&&$4!=""){
gGlobals.setAttribute("BASEURL",$4)
};if($5!=null&&$5!="undefined"&&$5!=""){
selectItem($5)
};setAttribute("isUpToDate",true);if(canvas.width<900)animate("width",900,100*anm_multipler,false,{motion:"easeout"});if(canvas.height<600)animate("height",600,100*anm_multipler,false,{motion:"easeout"});gDebug(this,"oninit","\n BASEURL: "+gGlobals.BASEURL+"\n RESTSRC:"+gGlobals.RESTSRC+"\n DATAURL:"+gGlobals.DATAURL+"\n IMAGESRC:"+gGlobals.IMAGESRC+"\n\n")
}},"setOpen",function($0){
with(this){
if($0){
topview.anm.doStart();bottom.anm.doStart();isopen=true;links.setAttribute("visible",false);interior.setAttribute("visible",true);return bottom.anm
}else{
toHomeDel.register(photoscontainer.lyt.transitiontogrid_anm,"onstop");photoscontainer.lyt.transitiontogrid_anm.doStart()
}}},"freshNClean",function(){
with(this){
interior.setAttribute("visible",false);creditView.setAttribute("visible",false);aboutusView.setAttribute("visible",false);mapView.setAttribute("visible",false);storyView.setAttribute("visible",false);spnr.setAttribute("visible",false)
}},"toHome",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};toHomeDel.unregisterAll();freshNClean();topview.anmb.doStart();bottom.anmb.doStart();isopen=false;links.setAttribute("visible",true);return bottom.anmb
}},"selectItem",function($0){
with(this){
selectedItem=$0;photoscontainer.lyt.setAttribute("currentpage",1);tls.resetOnLoad();if(!canvas.isopen){
this.requestData_del.unregisterAll();var $1=canvas.setOpen(true);this.requestData_del.register($1,"onstop");return
};this.requestData()
}},"requestData",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(selectedItem=="aboutus"){
aboutusView.setAttribute("visible",true);return
};if(selectedItem=="where"){
mapView.setAttribute("visible",true);return
};if(selectedItem=="story"){
storyView.setAttribute("visible",true);return
};if(selectedItem=="credit"){
creditView.setAttribute("visible",true);return
};tls.displayPage_del.register(photods,"ondata");var $1=[{argname:"catalogId",argvalue:selectedItem}];gDataMan.doDSXRequest(photods,$1);tls.setAttribute("visible",true);tls.setAttribute("opacity",1)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzCanvas.attributes)]);canvas=new $lzc$class_m4(null,{$delegates:["oninit","$m3",null],__LZproxied:"true",anm_multipler:1,appbuilddate:"2010-04-13T14:46:07Z",bgcolor:13619151,embedfonts:true,fontname:"Verdana,Vera,sans-serif",fontsize:11,fontstyle:"plain",height:"100%",isUpToDate:false,isopen:false,lpsbuild:"trunk@15776 (15776)",lpsbuilddate:"2010-02-17T09:08:05Z",lpsrelease:"Latest",lpsversion:"5.0.x",proxied:false,requestData_del:new LzOnceExpr("$m2"),runtime:"dhtml",selectedItem:"",title:"Mobil Quattro Sud",toHomeDel:new LzOnceExpr("$m1"),width:"100%"});Class.make("$lzc$class_html",LzView,["scrollbars",void 0,"loading",void 0,"ready",void 0,"history",void 0,"minimumheight",void 0,"minimumwidth",void 0,"mouseevents",void 0,"$lzc$set_mouseevents",function($0){
with(this){
this.mouseevents=$0;if(this["iframeid"]){
lz.embed.iframemanager.setSendMouseEvents(this.iframeid,$0)
}else{
this.__mouseevents=$0
};if(this["onmouseevents"]&&this.onmouseevents.ready)this.onmouseevents.sendEvent($0)
}},"shownativecontextmenu",void 0,"$lzc$set_shownativecontextmenu",function($0){
with(this){
this.shownativecontextmenu=$0;if(this["iframeid"]){
lz.embed.iframemanager.setShowNativeContextMenu(this.iframeid,$0,true)
}else{
this.__shownativecontextmenu=$0
};if(this["onshownativecontextmenu"]&&this.onshownativecontextmenu.ready)this.onshownativecontextmenu.sendEvent($0)
}},"$m5",function($0){
var $1=this.parent;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}},"$m6",function(){
return [this,"parent"]
},"target",void 0,"framename",void 0,"onready",void 0,"$lzc$set_target",function($0){
with(this){
if($0==null)return;this.target=$0;if(this["_posdel"]){
this._posdel.unregisterAll()
}else{
this._posdel=new LzDelegate(this,"__updatepos")
};if($0!=this){
this._posdel.register(this,"onwidth");this._posdel.register(this,"onheight")
};this._posdel.register(this.target,"onwidth");this._posdel.register(this.target,"onheight");this.__updatepos(null);if(this["ontarget"])this.ontarget.sendEvent($0)
}},"$lzc$set_visible",function($0){
with(this){
this.visible=$0;if(this["iframeid"])lz.embed.iframemanager.setVisible(this.iframeid,$0);if(this["onvisible"])this.onvisible.sendEvent($0)
}},"src",void 0,"onsrc",void 0,"$lzc$set_src",function($0){
with(this){
this.src=$0;this.setAttribute("loading",true);if(this["iframeid"]){
lz.embed.iframemanager.setSrc(this.iframeid,$0,this.history)
}else{
this.__srcset=$0
};this.onsrc.sendEvent($0)
}},"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);var $0=lz.embed.iframemanager.create(this,this.framename,this.scrollbars,this.sprite.__LZdiv,0,canvas);if($0)this.setiframeid($0)
}},"__updatepos",function($0){
with(this){
if(!this["iframeid"])return;var $1=null;var $2=null;var $3=this.sprite.getZ();var $4=Math.max(this.width,this.minimumwidth);var $5=Math.max(this.height,this.minimumheight);lz.embed.iframemanager.setPosition(this.iframeid,$1,$2,$4,$5,this.visible,$3)
}},"setiframeid",function($0){
with(this){
this.iframeid=$0;if(this["isfront"])this.bringToFront();if(this["__srcset"])lz.embed.iframemanager.setSrc($0,this.__srcset,this.history);this.__updatepos(null);this.setAttribute("clickable",true);this.setAttribute("ready",true);if(this["__mouseevents"])lz.embed.iframemanager.setSendMouseEvents($0,this.__mouseevents);if(this["__shownativecontextmenu"])lz.embed.iframemanager.setShowNativeContextMenu($0,this.__shownativecontextmenu)
}},"__gotload",function(){
if(this["src"]==null)return;this.setAttribute("loading",false);this.__updatepos(null);this.onload.sendEvent()
},"bringToFront",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["bringToFront"]||this.nextMethod(arguments.callee,"bringToFront")).call(this);if(this["isfront"]==true||!this["iframeid"])return;this.isfront=true;lz.embed.iframemanager.setZ(this.iframeid,this.sprite.getZ())
}},"sendToBack",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["sendToBack"]||this.nextMethod(arguments.callee,"sendToBack")).call(this);if(this["isfront"]==false||!this["iframeid"])return;this.isfront=false;if(this["iframeid"])lz.embed.iframemanager.sendToBack(this.iframeid);lz.embed.iframemanager.setZ(this.iframeid,this.sprite.getZ())
}},"scrollBy",function($0,$1){
with(this){
if(this["iframeid"])lz.embed.iframemanager.scrollBy(this.iframeid,$0,$1)
}},"callJavascript",function($0,$1){
with(this){
switch(arguments.length){
case 1:
$1=null;

};var $2=Array.prototype.slice.call(arguments,2);lz.embed.iframemanager.callJavascript(this.iframeid,$0,$1,$2)
}},"restoreSelection",function(){
with(this){
lz.embed.iframemanager.restoreSelection(this.iframeid)
}},"storeSelection",function(){
with(this){
lz.embed.iframemanager.storeSelection(this.iframeid)
}},"destroy",function(){
with(this){
if(this["iframeid"])lz.embed.iframemanager.__destroy(this.iframeid);if(this._posdel){
this._posdel.unregisterAll()
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
}},"$m7",function($0){
with(this){
if(!this["iframeid"])return;LzMouseKernel.disableMouseTemporarily()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","html","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmouseover","$m7",null],clickable:true,framename:"",history:true,loading:false,minimumheight:0,minimumwidth:0,mouseevents:true,onload:LzDeclaredEvent,onready:LzDeclaredEvent,onsrc:LzDeclaredEvent,ready:false,scrollbars:true,shownativecontextmenu:true,target:new LzAlwaysExpr("$m5","$m6"),visible:true},$lzc$class_html.attributes)
}}})($lzc$class_html);lz.colors.offwhite=15921906;lz.colors.gray10=1710618;lz.colors.gray20=3355443;lz.colors.gray30=5066061;lz.colors.gray40=6710886;lz.colors.gray50=8355711;lz.colors.gray60=10066329;lz.colors.gray70=11776947;lz.colors.gray80=13421772;lz.colors.gray90=15066597;lz.colors.iceblue1=3298963;lz.colors.iceblue2=5472718;lz.colors.iceblue3=12240085;lz.colors.iceblue4=14017779;lz.colors.iceblue5=15659509;lz.colors.palegreen1=4290113;lz.colors.palegreen2=11785139;lz.colors.palegreen3=12637341;lz.colors.palegreen4=13888170;lz.colors.palegreen5=15725032;lz.colors.gold1=9331721;lz.colors.gold2=13349195;lz.colors.gold3=15126388;lz.colors.gold4=16311446;lz.colors.sand1=13944481;lz.colors.sand2=14276546;lz.colors.sand3=15920859;lz.colors.sand4=15986401;lz.colors.ltpurple1=6575768;lz.colors.ltpurple2=12038353;lz.colors.ltpurple3=13353453;lz.colors.ltpurple4=15329264;lz.colors.grayblue=12501704;lz.colors.graygreen=12635328;lz.colors.graypurple=10460593;lz.colors.ltblue=14540287;lz.colors.ltgreen=14548957;Class.make("$lzc$class_basefocusview",LzView,["active",void 0,"$lzc$set_active",function($0){
with(this){
setActive($0)
}},"target",void 0,"$lzc$set_target",function($0){
with(this){
setTarget($0)
}},"duration",void 0,"_animatorcounter",void 0,"ontarget",void 0,"_nexttarget",void 0,"onactive",void 0,"_xydelegate",void 0,"_widthdel",void 0,"_heightdel",void 0,"_delayfadeoutDL",void 0,"_dofadeout",void 0,"_onstopdel",void 0,"reset",function(){
with(this){
this.setAttribute("x",0);this.setAttribute("y",0);this.setAttribute("width",canvas.width);this.setAttribute("height",canvas.height);setTarget(null)
}},"setActive",function($0){
this.active=$0;if(this.onactive)this.onactive.sendEvent($0)
},"doFocus",function($0){
with(this){
this._dofadeout=false;this.bringToFront();if(this.target)this.setTarget(null);this.setAttribute("visibility",this.active?"visible":"hidden");this._nexttarget=$0;if(visible){
this._animatorcounter+=1;var $1=null;var $2;var $3;var $4;var $5;if($0["getFocusRect"])$1=$0.getFocusRect();if($1){
$2=$1[0];$3=$1[1];$4=$1[2];$5=$1[3]
}else{
$2=$0.getAttributeRelative("x",canvas);$3=$0.getAttributeRelative("y",canvas);$4=$0.getAttributeRelative("width",canvas);$5=$0.getAttributeRelative("height",canvas)
};var $6=this.animate("x",$2,duration);this.animate("y",$3,duration);this.animate("width",$4,duration);this.animate("height",$5,duration);if(this.capabilities["minimize_opacity_changes"]){
this.setAttribute("visibility","visible")
}else{
this.animate("opacity",1,500)
};if(!this._onstopdel)this._onstopdel=new LzDelegate(this,"stopanim");this._onstopdel.register($6,"onstop")
};if(this._animatorcounter<1){
this.setTarget(this._nexttarget);var $1=null;var $2;var $3;var $4;var $5;if($0["getFocusRect"])$1=$0.getFocusRect();if($1){
$2=$1[0];$3=$1[1];$4=$1[2];$5=$1[3]
}else{
$2=$0.getAttributeRelative("x",canvas);$3=$0.getAttributeRelative("y",canvas);$4=$0.getAttributeRelative("width",canvas);$5=$0.getAttributeRelative("height",canvas)
};this.setAttribute("x",$2);this.setAttribute("y",$3);this.setAttribute("width",$4);this.setAttribute("height",$5)
}}},"stopanim",function($0){
with(this){
this._animatorcounter-=1;if(this._animatorcounter<1){
this._dofadeout=true;if(!this._delayfadeoutDL)this._delayfadeoutDL=new LzDelegate(this,"fadeout");lz.Timer.addTimer(this._delayfadeoutDL,1000);this.setTarget(_nexttarget);this._onstopdel.unregisterAll()
}}},"fadeout",function($0){
with(this){
if(_dofadeout){
if(this.capabilities["minimize_opacity_changes"]){
this.setAttribute("visibility","hidden")
}else{
this.animate("opacity",0,500)
}};this._delayfadeoutDL.unregisterAll()
}},"setTarget",function($0){
with(this){
this.target=$0;if(!this._xydelegate){
this._xydelegate=new LzDelegate(this,"followXY")
}else{
this._xydelegate.unregisterAll()
};if(!this._widthdel){
this._widthdel=new LzDelegate(this,"followWidth")
}else{
this._widthdel.unregisterAll()
};if(!this._heightdel){
this._heightdel=new LzDelegate(this,"followHeight")
}else{
this._heightdel.unregisterAll()
};if(this.target==null)return;var $1=$0;var $2=0;while($1!=canvas){
this._xydelegate.register($1,"onx");this._xydelegate.register($1,"ony");$1=$1.immediateparent;$2++
};this._widthdel.register($0,"onwidth");this._heightdel.register($0,"onheight");followXY(null);followWidth(null);followHeight(null)
}},"followXY",function($0){
with(this){
var $1=null;if(target["getFocusRect"])$1=target.getFocusRect();if($1){
this.setAttribute("x",$1[0]);this.setAttribute("y",$1[1])
}else{
this.setAttribute("x",this.target.getAttributeRelative("x",canvas));this.setAttribute("y",this.target.getAttributeRelative("y",canvas))
}}},"followWidth",function($0){
with(this){
var $1=null;if(target["getFocusRect"])$1=target.getFocusRect();if($1){
this.setAttribute("width",$1[2])
}else{
this.setAttribute("width",this.target.width)
}}},"followHeight",function($0){
with(this){
var $1=null;if(target["getFocusRect"])$1=target.getFocusRect();if($1){
this.setAttribute("height",$1[3])
}else{
this.setAttribute("height",this.target.height)
}}},"$m8",function(){
with(this){
var $0=lz.Focus;return $0
}},"$m9",function($0){
with(this){
this.setActive(lz.Focus.focuswithkey);if($0){
this.doFocus($0)
}else{
this.reset();if(this.active){
this.setActive(false)
}}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basefocusview","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onstop","stopanim",null,"onfocus","$m9","$m8"],_animatorcounter:0,_delayfadeoutDL:null,_dofadeout:false,_heightdel:null,_nexttarget:null,_onstopdel:null,_widthdel:null,_xydelegate:null,active:false,duration:400,initstage:"late",onactive:LzDeclaredEvent,ontarget:LzDeclaredEvent,options:{ignorelayout:true},target:null,visible:false},$lzc$class_basefocusview.attributes)
}}})($lzc$class_basefocusview);Class.make("$lzc$class_m26",LzView,["$m10",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m11",function(){
with(this){
return [classroot,"offset"]
}},"$m12",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m13",function(){
with(this){
return [classroot,"offset"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m27",LzView,["$m14",function($0){
with(this){
var $1=parent.width-width+classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m15",function(){
with(this){
return [parent,"width",this,"width",classroot,"offset"]
}},"$m16",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m17",function(){
with(this){
return [classroot,"offset"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,frame:2,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,frame:2,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m28",LzView,["$m18",function($0){
with(this){
var $1=-classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m19",function(){
with(this){
return [classroot,"offset"]
}},"$m20",function($0){
with(this){
var $1=parent.height-height+classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m21",function(){
with(this){
return [parent,"height",this,"height",classroot,"offset"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,frame:3,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,frame:3,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m29",LzView,["$m22",function($0){
with(this){
var $1=parent.width-width+classroot.offset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m23",function(){
with(this){
return [parent,"width",this,"width",classroot,"offset"]
}},"$m24",function($0){
with(this){
var $1=parent.height-height+classroot.offset;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m25",function(){
with(this){
return [parent,"height",this,"height",classroot,"offset"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,frame:4,opacity:0.25,resource:"lzfocusbracket_shdw",x:1,y:1},"class":LzView},{attrs:{$classrootdepth:2,frame:4,resource:"lzfocusbracket_rsrc"},"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_focusoverlay",$lzc$class_basefocusview,["offset",void 0,"topleft",void 0,"topright",void 0,"bottomleft",void 0,"bottomright",void 0,"doFocus",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["doFocus"]||this.nextMethod(arguments.callee,"doFocus")).call(this,$0);if(visible)this.bounce()
}},"bounce",function(){
with(this){
this.animate("offset",12,duration/2);this.animate("offset",5,duration)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","focusoverlay","children",[{attrs:{$classrootdepth:1,name:"topleft",x:new LzAlwaysExpr("$m10","$m11"),y:new LzAlwaysExpr("$m12","$m13")},"class":$lzc$class_m26},{attrs:{$classrootdepth:1,name:"topright",x:new LzAlwaysExpr("$m14","$m15"),y:new LzAlwaysExpr("$m16","$m17")},"class":$lzc$class_m27},{attrs:{$classrootdepth:1,name:"bottomleft",x:new LzAlwaysExpr("$m18","$m19"),y:new LzAlwaysExpr("$m20","$m21")},"class":$lzc$class_m28},{attrs:{$classrootdepth:1,name:"bottomright",x:new LzAlwaysExpr("$m22","$m23"),y:new LzAlwaysExpr("$m24","$m25")},"class":$lzc$class_m29}],"attributes",new LzInheritedHash($lzc$class_basefocusview.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({offset:5},$lzc$class_focusoverlay.attributes)
}}})($lzc$class_focusoverlay);Class.make("$lzc$class__componentmanager",LzNode,["focusclass",void 0,"keyhandlers",void 0,"lastsdown",void 0,"lastedown",void 0,"defaults",void 0,"currentdefault",void 0,"defaultstyle",void 0,"ondefaultstyle",void 0,"init",function(){
with(this){
var $0=this.focusclass;if(typeof canvas.focusclass!="undefined"){
$0=canvas.focusclass
};if($0!=null){
canvas.__focus=new (lz[$0])(canvas);canvas.__focus.reset()
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this)
}},"_lastkeydown",void 0,"upkeydel",void 0,"$m30",function(){
with(this){
var $0=lz.Keys;return $0
}},"dispatchKeyDown",function($0){
with(this){
var $1=false;if($0==32){
this.lastsdown=null;var $2=lz.Focus.getFocus();if($2 instanceof lz.basecomponent){
$2.doSpaceDown();this.lastsdown=$2
};$1=true
}else if($0==13&&this.currentdefault){
this.lastedown=this.currentdefault;this.currentdefault.doEnterDown();$1=true
};if($1){
if(!this.upkeydel)this.upkeydel=new LzDelegate(this,"dispatchKeyTimer");this._lastkeydown=$0;lz.Timer.addTimer(this.upkeydel,50)
}}},"dispatchKeyTimer",function($0){
if(this._lastkeydown==32&&this.lastsdown!=null){
this.lastsdown.doSpaceUp();this.lastsdown=null
}else if(this._lastkeydown==13&&this.currentdefault&&this.currentdefault==this.lastedown){
this.currentdefault.doEnterUp()
}},"findClosestDefault",function($0){
with(this){
if(!this.defaults){
return null
};var $1=null;var $2=null;var $3=this.defaults;$0=$0||canvas;var $4=lz.ModeManager.getModalView();for(var $5=0;$5<$3.length;$5++){
var $6=$3[$5];if($4&&!$6.childOf($4)){
continue
};var $7=this.findCommonParent($6,$0);if($7&&(!$1||$7.nodeLevel>$1.nodeLevel)){
$1=$7;$2=$6
}};return $2
}},"findCommonParent",function($0,$1){
while($0.nodeLevel>$1.nodeLevel){
$0=$0.immediateparent;if(!$0.visible)return null
};while($1.nodeLevel>$0.nodeLevel){
$1=$1.immediateparent;if(!$1.visible)return null
};while($0!=$1){
$0=$0.immediateparent;$1=$1.immediateparent;if(!$0.visible||!$1.visible)return null
};return $0
},"makeDefault",function($0){
with(this){
if(!this.defaults)this.defaults=[];this.defaults.push($0);this.checkDefault(lz.Focus.getFocus())
}},"unmakeDefault",function($0){
with(this){
if(!this.defaults)return;for(var $1=0;$1<this.defaults.length;$1++){
if(this.defaults[$1]==$0){
this.defaults.splice($1,1);this.checkDefault(lz.Focus.getFocus());return
}}}},"$m31",function(){
with(this){
var $0=lz.Focus;return $0
}},"checkDefault",function($0){
with(this){
if(!($0 instanceof lz.basecomponent)||!$0.doesenter){
if($0 instanceof lz.inputtext&&$0.multiline){
$0=null
}else{
$0=this.findClosestDefault($0)
}};if($0==this.currentdefault)return;if(this.currentdefault){
this.currentdefault.setAttribute("hasdefault",false)
};this.currentdefault=$0;if($0){
$0.setAttribute("hasdefault",true)
}}},"$m32",function(){
with(this){
var $0=lz.ModeManager;return $0
}},"$m33",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(lz.Focus.getFocus()==null){
this.checkDefault(null)
}}},"setDefaultStyle",function($0){
this.defaultstyle=$0;if(this.ondefaultstyle)this.ondefaultstyle.sendEvent($0)
},"getDefaultStyle",function(){
with(this){
if(this.defaultstyle==null){
this.defaultstyle=new (lz.style)(canvas,{isdefault:true})
};return this.defaultstyle
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","_componentmanager","attributes",new LzInheritedHash(LzNode.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onkeydown","dispatchKeyDown","$m30","onfocus","checkDefault","$m31","onmode","$m33","$m32"],_lastkeydown:0,currentdefault:null,defaults:null,defaultstyle:null,focusclass:"focusoverlay",keyhandlers:null,lastedown:null,lastsdown:null,ondefaultstyle:LzDeclaredEvent,upkeydel:null},$lzc$class__componentmanager.attributes)
}}})($lzc$class__componentmanager);Class.make("$lzc$class_style",LzNode,["isstyle",void 0,"$m34",function($0){
with(this){
this.setAttribute("canvascolor",LzColorUtils.convertColor("null"))
}},"canvascolor",void 0,"$lzc$set_canvascolor",function($0){
with(this){
setCanvasColor($0)
}},"$m35",function($0){
with(this){
this.setAttribute("textcolor",LzColorUtils.convertColor("gray10"))
}},"textcolor",void 0,"$lzc$set_textcolor",function($0){
with(this){
setStyleAttr($0,"textcolor")
}},"$m36",function($0){
with(this){
this.setAttribute("textfieldcolor",LzColorUtils.convertColor("white"))
}},"textfieldcolor",void 0,"$lzc$set_textfieldcolor",function($0){
with(this){
setStyleAttr($0,"textfieldcolor")
}},"$m37",function($0){
with(this){
this.setAttribute("texthilitecolor",LzColorUtils.convertColor("iceblue1"))
}},"texthilitecolor",void 0,"$lzc$set_texthilitecolor",function($0){
with(this){
setStyleAttr($0,"texthilitecolor")
}},"$m38",function($0){
with(this){
this.setAttribute("textselectedcolor",LzColorUtils.convertColor("black"))
}},"textselectedcolor",void 0,"$lzc$set_textselectedcolor",function($0){
with(this){
setStyleAttr($0,"textselectedcolor")
}},"$m39",function($0){
with(this){
this.setAttribute("textdisabledcolor",LzColorUtils.convertColor("gray60"))
}},"textdisabledcolor",void 0,"$lzc$set_textdisabledcolor",function($0){
with(this){
setStyleAttr($0,"textdisabledcolor")
}},"$m40",function($0){
with(this){
this.setAttribute("basecolor",LzColorUtils.convertColor("offwhite"))
}},"basecolor",void 0,"$lzc$set_basecolor",function($0){
with(this){
setStyleAttr($0,"basecolor")
}},"$m41",function($0){
with(this){
this.setAttribute("bgcolor",LzColorUtils.convertColor("white"))
}},"bgcolor",void 0,"$lzc$set_bgcolor",function($0){
with(this){
setStyleAttr($0,"bgcolor")
}},"$m42",function($0){
with(this){
this.setAttribute("hilitecolor",LzColorUtils.convertColor("iceblue4"))
}},"hilitecolor",void 0,"$lzc$set_hilitecolor",function($0){
with(this){
setStyleAttr($0,"hilitecolor")
}},"$m43",function($0){
with(this){
this.setAttribute("selectedcolor",LzColorUtils.convertColor("iceblue3"))
}},"selectedcolor",void 0,"$lzc$set_selectedcolor",function($0){
with(this){
setStyleAttr($0,"selectedcolor")
}},"$m44",function($0){
with(this){
this.setAttribute("disabledcolor",LzColorUtils.convertColor("gray30"))
}},"disabledcolor",void 0,"$lzc$set_disabledcolor",function($0){
with(this){
setStyleAttr($0,"disabledcolor")
}},"$m45",function($0){
with(this){
this.setAttribute("bordercolor",LzColorUtils.convertColor("gray40"))
}},"bordercolor",void 0,"$lzc$set_bordercolor",function($0){
with(this){
setStyleAttr($0,"bordercolor")
}},"$m46",function($0){
this.setAttribute("bordersize",1)
},"bordersize",void 0,"$lzc$set_bordersize",function($0){
with(this){
setStyleAttr($0,"bordersize")
}},"$m47",function($0){
with(this){
this.setAttribute("menuitembgcolor",LzColorUtils.convertColor("textfieldcolor"))
}},"menuitembgcolor",void 0,"isdefault",void 0,"$lzc$set_isdefault",function($0){
with(this){
_setdefault($0)
}},"onisdefault",void 0,"_setdefault",function($0){
with(this){
this.isdefault=$0;if(isdefault){
lz._componentmanager.service.setDefaultStyle(this);if(this["canvascolor"]!=null){
canvas.setAttribute("bgcolor",this.canvascolor)
}};if(this.onisdefault)this.onisdefault.sendEvent(this)
}},"onstylechanged",void 0,"setStyleAttr",function($0,$1){
this[$1]=$0;if(this["on"+$1])this["on"+$1].sendEvent($1);if(this.onstylechanged)this.onstylechanged.sendEvent(this)
},"setCanvasColor",function($0){
with(this){
if(this.isdefault&&$0!=null){
canvas.setAttribute("bgcolor",$0)
};this.canvascolor=$0;if(this.onstylechanged)this.onstylechanged.sendEvent(this)
}},"extend",function($0){
with(this){
var $1=new (lz.style)();$1.canvascolor=this.canvascolor;$1.textcolor=this.textcolor;$1.textfieldcolor=this.textfieldcolor;$1.texthilitecolor=this.texthilitecolor;$1.textselectedcolor=this.textselectedcolor;$1.textdisabledcolor=this.textdisabledcolor;$1.basecolor=this.basecolor;$1.bgcolor=this.bgcolor;$1.hilitecolor=this.hilitecolor;$1.selectedcolor=this.selectedcolor;$1.disabledcolor=this.disabledcolor;$1.bordercolor=this.bordercolor;$1.bordersize=this.bordersize;$1.menuitembgcolor=this.menuitembgcolor;$1.isdefault=this.isdefault;for(var $2 in $0){
$1[$2]=$0[$2]
};new LzDelegate($1,"_forwardstylechanged",this,"onstylechanged");return $1
}},"_forwardstylechanged",function($0){
if(this.onstylechanged)this.onstylechanged.sendEvent(this)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","style","attributes",new LzInheritedHash(LzNode.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({basecolor:new LzOnceExpr("$m40"),bgcolor:new LzOnceExpr("$m41"),bordercolor:new LzOnceExpr("$m45"),bordersize:new LzOnceExpr("$m46"),canvascolor:new LzOnceExpr("$m34"),disabledcolor:new LzOnceExpr("$m44"),hilitecolor:new LzOnceExpr("$m42"),isdefault:false,isstyle:true,menuitembgcolor:new LzOnceExpr("$m47"),onisdefault:LzDeclaredEvent,onstylechanged:LzDeclaredEvent,selectedcolor:new LzOnceExpr("$m43"),textcolor:new LzOnceExpr("$m35"),textdisabledcolor:new LzOnceExpr("$m39"),textfieldcolor:new LzOnceExpr("$m36"),texthilitecolor:new LzOnceExpr("$m37"),textselectedcolor:new LzOnceExpr("$m38")},$lzc$class_style.attributes)
}}})($lzc$class_style);canvas.LzInstantiateView({"class":lz.script,attrs:{script:function(){
lz._componentmanager.service=new (lz._componentmanager)(canvas,null,null,true)
}}},1);Class.make("$lzc$class_statictext",LzText,["$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","statictext","attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_basecomponent",LzView,["enabled",void 0,"$lzc$set_focusable",function($0){
with(this){
_setFocusable($0)
}},"_focusable",void 0,"onfocusable",void 0,"text",void 0,"doesenter",void 0,"$lzc$set_doesenter",function($0){
this._setDoesEnter($0)
},"$m48",function($0){
var $1=this.enabled&&(this._parentcomponent?this._parentcomponent._enabled:true);if($1!==this["_enabled"]||!this.inited){
this.setAttribute("_enabled",$1)
}},"$m49",function(){
return [this,"enabled",this,"_parentcomponent",this._parentcomponent,"_enabled"]
},"_enabled",void 0,"$lzc$set__enabled",function($0){
this._setEnabled($0)
},"_parentcomponent",void 0,"_initcomplete",void 0,"isdefault",void 0,"$lzc$set_isdefault",function($0){
this._setIsDefault($0)
},"onisdefault",void 0,"hasdefault",void 0,"_setEnabled",function($0){
with(this){
this._enabled=$0;var $1=this._enabled&&this._focusable;if($1!=this.focusable){
this.focusable=$1;if(this.onfocusable.ready)this.onfocusable.sendEvent()
};if(_initcomplete)_showEnabled();if(this.on_enabled.ready)this.on_enabled.sendEvent()
}},"_setFocusable",function($0){
this._focusable=$0;if(this.enabled){
this.focusable=this._focusable;if(this.onfocusable.ready)this.onfocusable.sendEvent()
}else{
this.focusable=false
}},"construct",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);var $2=this.immediateparent;while($2!=canvas){
if(lz.basecomponent["$lzsc$isa"]?lz.basecomponent.$lzsc$isa($2):$2 instanceof lz.basecomponent){
this._parentcomponent=$2;break
};$2=$2.immediateparent
}}},"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this._initcomplete=true;this._mousedownDel=new LzDelegate(this,"_doMousedown",this,"onmousedown");if(this.styleable){
_usestyle()
};if(!this["_enabled"])_showEnabled()
}},"_doMousedown",function($0){},"doSpaceDown",function(){
return false
},"doSpaceUp",function(){
return false
},"doEnterDown",function(){
return false
},"doEnterUp",function(){
return false
},"_setIsDefault",function($0){
with(this){
this.isdefault=this["isdefault"]==true;if(this.isdefault==$0)return;if($0){
lz._componentmanager.service.makeDefault(this)
}else{
lz._componentmanager.service.unmakeDefault(this)
};this.isdefault=$0;if(this.onisdefault.ready){
this.onisdefault.sendEvent($0)
}}},"_setDoesEnter",function($0){
with(this){
this.doesenter=$0;if(lz.Focus.getFocus()==this){
lz._componentmanager.service.checkDefault(this)
}}},"updateDefault",function(){
with(this){
lz._componentmanager.service.checkDefault(lz.Focus.getFocus())
}},"$m50",function($0){
this.setAttribute("style",null)
},"style",void 0,"$lzc$set_style",function($0){
with(this){
styleable?setStyle($0):(this.style=null)
}},"styleable",void 0,"_style",void 0,"onstyle",void 0,"_styledel",void 0,"_otherstyledel",void 0,"setStyle",function($0){
with(this){
if(!styleable)return;if($0!=null&&!$0["isstyle"]){
var $1=this._style;if(!$1){
if(this._parentcomponent){
$1=this._parentcomponent.style
}else $1=lz._componentmanager.service.getDefaultStyle()
};$0=$1.extend($0)
};this._style=$0;if($0==null){
if(!this._otherstyledel){
this._otherstyledel=new LzDelegate(this,"_setstyle")
}else{
this._otherstyledel.unregisterAll()
};if(this._parentcomponent&&this._parentcomponent.styleable){
this._otherstyledel.register(this._parentcomponent,"onstyle");$0=this._parentcomponent.style
}else{
this._otherstyledel.register(lz._componentmanager.service,"ondefaultstyle");$0=lz._componentmanager.service.getDefaultStyle()
}}else if(this._otherstyledel){
this._otherstyledel.unregisterAll();this._otherstyledel=null
};_setstyle($0)
}},"_usestyle",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this._initcomplete&&this["style"]&&this.style.isinited){
this._applystyle(this.style)
}},"_setstyle",function($0){
with(this){
if(!this._styledel){
this._styledel=new LzDelegate(this,"_usestyle")
}else{
_styledel.unregisterAll()
};if($0){
_styledel.register($0,"onstylechanged")
};this.style=$0;_usestyle();if(this.onstyle.ready)this.onstyle.sendEvent(this.style)
}},"_applystyle",function($0){},"setTint",function($0,$1,$2){
switch(arguments.length){
case 2:
$2=0;

};if($0.capabilities.colortransform){
if($1!=""&&$1!=null){
var $3=$1;var $4=$3>>16&255;var $5=$3>>8&255;var $6=$3&255;$4+=51;$5+=51;$6+=51;$4=$4/255*100;$5=$5/255*100;$6=$6/255*100;$0.setColorTransform({ra:$4,ga:$5,ba:$6,rb:$2,gb:$2,bb:$2})
}}},"on_enabled",void 0,"_showEnabled",function(){},"acceptValue",function($0,$1){
switch(arguments.length){
case 1:
$1=null;

};this.setAttribute("text",$0)
},"presentValue",function($0){
switch(arguments.length){
case 0:
$0=null;

};return this.text
},"$lzc$presentValue_dependencies",function($0,$1,$2){
switch(arguments.length){
case 2:
$2=null;

};return [this,"text"]
},"applyData",function($0){
this.acceptValue($0)
},"updateData",function(){
return this.presentValue()
},"destroy",function(){
with(this){
if(this["isdefault"]&&this.isdefault){
lz._componentmanager.service.unmakeDefault(this)
};if(this._otherstyledel){
this._otherstyledel.unregisterAll();this._otherstyledel=null
};if(this._styledel){
this._styledel.unregisterAll();this._styledel=null
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
}},"toString",function(){
var $0="";var $1="";var $2="";if(this["id"]!=null)$0="  id="+this.id;if(this["name"]!=null)$1=' named "'+this.name+'"';if(this["text"]&&this.text!="")$2="  text="+this.text;return this.constructor.tagname+$1+$0+$2
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basecomponent","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_enabled:new LzAlwaysExpr("$m48","$m49"),_focusable:true,_initcomplete:false,_otherstyledel:null,_parentcomponent:null,_style:null,_styledel:null,doesenter:false,enabled:true,focusable:true,hasdefault:false,on_enabled:LzDeclaredEvent,onfocusable:LzDeclaredEvent,onisdefault:LzDeclaredEvent,onstyle:LzDeclaredEvent,style:new LzOnceExpr("$m50"),styleable:true,text:""},$lzc$class_basecomponent.attributes)
}}})($lzc$class_basecomponent);Class.make("$lzc$class_basebutton",$lzc$class_basecomponent,["normalResourceNumber",void 0,"overResourceNumber",void 0,"downResourceNumber",void 0,"disabledResourceNumber",void 0,"$m51",function($0){
this.setAttribute("maxframes",this.totalframes)
},"maxframes",void 0,"resourceviewcount",void 0,"$lzc$set_resourceviewcount",function($0){
this.setResourceViewCount($0)
},"respondtomouseout",void 0,"$m52",function($0){
this.setAttribute("reference",this)
},"reference",void 0,"$lzc$set_reference",function($0){
with(this){
setreference($0)
}},"onresourceviewcount",void 0,"_msdown",void 0,"_msin",void 0,"setResourceViewCount",function($0){
this.resourceviewcount=$0;if(this._initcomplete){
if($0>0){
if(this.subviews){
this.maxframes=this.subviews[0].totalframes;if(this.onresourceviewcount){
this.onresourceviewcount.sendEvent()
}}}}},"_callShow",function(){
if(this._msdown&&this._msin&&this.maxframes>=this.downResourceNumber){
this.showDown()
}else if(this._msin&&this.maxframes>=this.overResourceNumber){
this.showOver()
}else this.showUp()
},"$m53",function(){
with(this){
var $0=lz.ModeManager;return $0
}},"$m54",function($0){
if($0&&(this._msdown||this._msin)&&!this.childOf($0)){
this._msdown=false;this._msin=false;this._callShow()
}},"$lzc$set_frame",function($0){
with(this){
if(this.resourceviewcount>0){
for(var $1=0;$1<resourceviewcount;$1++){
this.subviews[$1].setAttribute("frame",$0)
}}else{
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_frame"]||this.nextMethod(arguments.callee,"$lzc$set_frame")).call(this,$0)
}}},"doSpaceDown",function(){
if(this._enabled){
this.showDown()
}},"doSpaceUp",function(){
if(this._enabled){
this.onclick.sendEvent();this.showUp()
}},"doEnterDown",function(){
if(this._enabled){
this.showDown()
}},"doEnterUp",function(){
if(this._enabled){
if(this.onclick){
this.onclick.sendEvent()
};this.showUp()
}},"$m55",function($0){
if(this.isinited){
this.maxframes=this.totalframes;this._callShow()
}},"init",function(){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this.setResourceViewCount(this.resourceviewcount);this._callShow()
},"$m56",function($0){
this.setAttribute("_msin",true);this._callShow()
},"$m57",function($0){
this.setAttribute("_msin",false);this._callShow()
},"$m58",function($0){
this.setAttribute("_msdown",true);this._callShow()
},"$m59",function($0){
this.setAttribute("_msdown",false);this._callShow()
},"_showEnabled",function(){
with(this){
reference.setAttribute("clickable",this._enabled);showUp()
}},"showDown",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.setAttribute("frame",this.downResourceNumber)
},"showUp",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(!this._enabled&&this.disabledResourceNumber){
this.setAttribute("frame",this.disabledResourceNumber)
}else{
this.setAttribute("frame",this.normalResourceNumber)
}},"showOver",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.setAttribute("frame",this.overResourceNumber)
},"setreference",function($0){
this.reference=$0;if($0!=this)this.setAttribute("clickable",false)
},"_applystyle",function($0){
with(this){
setTint(this,$0.basecolor)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basebutton","attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmode","$m54","$m53","ontotalframes","$m55",null,"onmouseover","$m56",null,"onmouseout","$m57",null,"onmousedown","$m58",null,"onmouseup","$m59",null],_msdown:false,_msin:false,clickable:true,disabledResourceNumber:4,downResourceNumber:3,focusable:false,maxframes:new LzOnceExpr("$m51"),normalResourceNumber:1,onclick:LzDeclaredEvent,onresourceviewcount:LzDeclaredEvent,overResourceNumber:2,reference:new LzOnceExpr("$m52"),resourceviewcount:0,respondtomouseout:true,styleable:false},$lzc$class_basebutton.attributes)
}}})($lzc$class_basebutton);Class.make("$lzc$class_basebuttonrepeater",$lzc$class_basebutton,["_lasttime",void 0,"stillDownDelegate",void 0,"isMouseDown",void 0,"onmousestilldown",void 0,"stillDownEventGenerator",function($0){
with(this){
var $1=new Date().getTime();var $2=$1-this._lasttime;this._lasttime=$1;if(this.isMouseDown){
var $3;if($2>600){
$3=500
}else{
$3=50;this.onmousestilldown.sendEvent()
};lz.Timer.resetTimer(this.stillDownDelegate,$3)
}}},"$m60",function($0){
with(this){
this._lasttime=new Date().getTime();this.isMouseDown=true;if(!this.stillDownDelegate){
this.stillDownDelegate=new LzDelegate(this,"stillDownEventGenerator")
};lz.Timer.addTimer(this.stillDownDelegate,500)
}},"$m61",function($0){
with(this){
this.isMouseDown=false;lz.Timer.removeTimer(this.stillDownDelegate)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basebuttonrepeater","attributes",new LzInheritedHash($lzc$class_basebutton.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousedown","$m60",null,"onmouseup","$m61",null],_lasttime:0,clickable:true,isMouseDown:false,onmousestilldown:LzDeclaredEvent,stillDownDelegate:null},$lzc$class_basebuttonrepeater.attributes)
}}})($lzc$class_basebuttonrepeater);Class.make("$lzc$class_basescrollbar",$lzc$class_basecomponent,["$m62",function($0){
var $1=null;if($1!==this["scrolltarget"]||!this.inited){
this.setAttribute("scrolltarget",$1)
}},"$m63",function(){
return []
},"scrolltarget",void 0,"axis",void 0,"sizeAxis",void 0,"otherSizeAxis",void 0,"scrollattr",void 0,"scrollmax",void 0,"onscrollmax",void 0,"pagesize",void 0,"stepsize",void 0,"scrollable",void 0,"focusview",void 0,"usemousewheel",void 0,"$lzc$set_usemousewheel",function($0){
with(this){
if($0==this.usemousewheel)return;this.usemousewheel=$0;if(this._mwUpdateDel)this._mwUpdateDel.unregisterAll();if($0){
this._mwUpdateDel=new LzDelegate(this,"mousewheelUpdate",lz.Keys,"onmousewheeldelta")
}}},"mousewheelevent_on",void 0,"mousewheelevent_off",void 0,"mousewheelactive",void 0,"onscrollable",void 0,"$m64",function($0){
var $1=this.enabled&&this.scrollable&&(this._parentcomponent?this._parentcomponent._enabled:true);if($1!==this["_enabled"]||!this.inited){
this.setAttribute("_enabled",$1)
}},"$m65",function(){
return [this,"enabled",this,"scrollable",this,"_parentcomponent",this._parentcomponent,"_enabled"]
},"usetargetsize",void 0,"othersb",void 0,"thumb",void 0,"_mwActivateDel",void 0,"_mwDeactivateDel",void 0,"_mwUpdateDel",void 0,"clipSizeDel",void 0,"targetHeightDel",void 0,"targetPosDel",void 0,"heightDel",void 0,"heightConstraint",void 0,"widthConstraint",void 0,"init",function(){
with(this){
this.sizeAxis=this.axis=="x"?"width":"height";this.otherSizeAxis=this.axis=="x"?"height":"width";if(this.scrollattr==""){
this.scrollattr=this.axis
};var $0=false;if(!this.scrolltarget){
var $1=immediateparent.subviews.length;for(var $2=0;$2<$1;$2++){
var $3=immediateparent.subviews[$2];if($3 instanceof lz.view){
if(!($3 instanceof lz.basescrollbar)){
if(!this.scrolltarget)this.scrolltarget=$3
}else{
if($3!=this){
this.setAttribute("othersb",$3)
}}}};if(this.axis=="y"){
this.setAttribute("align","right")
}else{
this.setAttribute("valign","bottom")
};$0=true
};if(!this.focusview){
if(this.scrolltarget&&this.scrolltarget["focusable"]){
this.focusview=this.scrolltarget
}else if(this.immediateparent["focusable"]){
this.focusview=this.immediateparent
}};if(this.focusview){
this._mwActivateDel=new LzDelegate(this,"activateMouseWheel",this.focusview,this.mousewheelevent_on);this._mwDeactivateDel=new LzDelegate(this,"deactivateMouseWheel",this.focusview,this.mousewheelevent_off)
};if(this.sizeAxis=="width"){
if(!hassetwidth){
this.widthConstraint.setAttribute("applied",true)
}};if(this.sizeAxis=="height"){
if(!hassetheight){
this.heightConstraint.setAttribute("applied",true)
}};if(!this.scrolltarget){
this.setAttribute("enabled",false)
}else{
this.clipSizeDel=new LzDelegate(this,"scrollbarSizeUpdate",this.scrolltarget.immediateparent,"on"+this.sizeAxis);if(this.scrollmax==null){
this.usetargetsize=true;this.targetHeightDel=new LzDelegate(this,"targetSizeUpdate",this.scrolltarget,"on"+this.sizeAxis);this.scrollmax=scrolltarget[this.sizeAxis];if($0&&this.othersb){
this.scrollmax+=this[this.otherSizeAxis]
}}else{
this.targetHeightDel=new LzDelegate(this,"scrollbarSizeUpdate",this,"onscrollmax")
};var $4;if(this.scrollattr=="yscroll"){
$4="onscrolly"
}else $4="on"+this.scrollattr;this.targetPosDel=new LzDelegate(this,"targetPosUpdate",this.scrolltarget,$4);this.heightDel=new LzDelegate(this,"scrollbarSizeUpdate",this.scrolltrack,"on"+this.sizeAxis);scrollbarSizeUpdate(null)
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this)
}},"destroy",function(){
if(this._mwActivateDel){
this._mwActivateDel.unregisterAll();this._mwActivateDel=null
};if(this._mwDeactivateDel){
this._mwDeactivateDel.unregisterAll();this._mwDeactivateDel=null
};if(this._mwUpdateDel){
this._mwUpdateDel.unregisterAll();this._mwUpdateDel=null
};if(this.clipSizeDel){
this.clipSizeDel.unregisterAll();this.clipSizeDel=null
};if(this.targetHeightDel){
this.targetHeightDel.unregisterAll();this.targetHeightDel=null
};if(this.targetPosDel){
this.targetPosDel.unregisterAll();this.targetPosDel=null
};if(this.heightDel){
this.heightDel.unregisterAll();this.heightDel=null
};this.scrolltarget=null;this.focusview=null;this.othersb=null;(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
},"activateMouseWheel",function($0){
this.setAttribute("mousewheelactive",true)
},"deactivateMouseWheel",function($0){
this.setAttribute("mousewheelactive",false)
},"mousewheelUpdate",function($0){
if(this.axis!="y")return;if(this.mousewheelactive||this.scrolltarget&&this.scrolltarget.immediateparent&&this.scrolltarget.immediateparent.isMouseOver()){
this.step(-$0)
}},"targetSizeUpdate",function($0){
if(this.scrolltarget){
var $1=this.scrolltarget[this.sizeAxis];if(this.othersb&&this.othersb.visible){
$1+=this[this.otherSizeAxis]
};this.setAttribute("scrollmax",$1);this.scrollbarSizeUpdate(null)
}},"scrollbarSizeUpdate",function($0){
this.updateThumbSize();if(this.scrolltarget.immediateparent[this.sizeAxis]-this.scrollmax>=0){
return
};var $1=this.scrolltarget[this.scrollattr]+this.scrollmax;if($1<this.scrolltarget.immediateparent[this.sizeAxis]){
var $2=this.scrolltarget.immediateparent[this.sizeAxis]-this.scrollmax;this.scrolltarget.setAttribute(this.scrollattr,$2)
}else{
this.updateThumbPos()
};this.pagesize=this[this.sizeAxis]
},"targetPosUpdate",function($0){
this.updateThumbPos()
},"updateThumbPos",function(){
with(this){
var $0=0;if(this.scrollmax>0&&this.scrolltrack&&this.scrolltarget){
$0=Math.min(Math.ceil(-this.scrolltarget[this.scrollattr]/this.scrollmax*this.scrolltrack[this.sizeAxis]),this.scrolltrack[this.sizeAxis]-this.thumb[this.sizeAxis])
};this.thumb.setAttribute(this.axis,$0)
}},"_showEnabled",function(){
with(this){
if(!_enabled){
this.thumb.setAttribute(sizeAxis,0)
}else updateThumbSize();this.thumb.setAttribute("visible",_enabled);if(scrolltarget)this.scrolltarget.setAttribute(scrollattr,0)
}},"updateThumbSize",function(){
with(this){
if(this.scrollmax<=this.scrolltarget.immediateparent[this.sizeAxis]){
if(this.scrollable){
this.setAttribute("scrollable",false);if(this.othersb)this.othersb.targetSizeUpdate(null)
};return
}else{
if(!this.scrollable){
this.setAttribute("scrollable",true);if(this.othersb)this.othersb.targetSizeUpdate(null)
}};var $0=0;if(this.scrollmax>0&&this.scrolltrack&&this.scrolltarget){
$0=Math.floor(this.scrolltarget.immediateparent[this.sizeAxis]/this.scrollmax*this.scrolltrack[this.sizeAxis])
};if($0<14)$0=14;thumb.setAttribute(this.sizeAxis,$0)
}},"setPosRelative",function($0){
with(this){
if(!this.scrolltarget)return;var $1=this.scrolltarget[this.scrollattr]-$0;if($1>0)$1=0;var $2=Math.max(this.scrollmax-this.scrolltarget.immediateparent[this.sizeAxis],0);if($1<-$2)$1=-$2;this.scrolltarget.setAttribute(this.scrollattr,$1)
}},"step",function($0){
this.setPosRelative($0*this.stepsize)
},"page",function($0){
this.setPosRelative($0*this.pagesize)
},"_applystyle",function($0){
with(this){
if(this.style!=null){
setTint(this,this.style.basecolor)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basescrollbar","children",[{attrs:{$classrootdepth:1,$m66:function($0){
var $1=this.othersb&&this.othersb.visible?this.immediateparent.height-this.othersb.height:this.immediateparent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}},$m67:function(){
return [this,"othersb",this.othersb,"visible",this.immediateparent,"height",this.othersb,"height"]
},height:new LzAlwaysExpr("$m66","$m67"),name:"heightConstraint"},"class":LzState},{attrs:{$classrootdepth:1,$m68:function($0){
var $1=this.othersb&&this.othersb.visible?this.immediateparent.width-this.othersb.width:this.immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}},$m69:function(){
return [this,"othersb",this.othersb,"visible",this.immediateparent,"width",this.othersb,"width"]
},name:"widthConstraint",width:new LzAlwaysExpr("$m68","$m69")},"class":LzState}],"attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_enabled:new LzAlwaysExpr("$m64","$m65"),_mwActivateDel:null,_mwDeactivateDel:null,_mwUpdateDel:null,axis:"y",clipSizeDel:null,focusable:false,focusview:null,heightDel:null,mousewheelactive:false,mousewheelevent_off:"onblur",mousewheelevent_on:"onfocus",onscrollable:LzDeclaredEvent,onscrollmax:LzDeclaredEvent,othersb:null,pagesize:null,scrollable:true,scrollattr:"",scrollmax:null,scrolltarget:new LzAlwaysExpr("$m62","$m63"),stepsize:10,targetHeightDel:null,targetPosDel:null,thumb:null,usemousewheel:true,usetargetsize:false},$lzc$class_basescrollbar.attributes)
}}})($lzc$class_basescrollbar);Class.make("$lzc$class_basescrollthumb",$lzc$class_basecomponent,["target",void 0,"axis",void 0,"trackscroll",void 0,"targetscroll",void 0,"init",function(){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this.classroot.thumb=this
},"destroy",function(){
this.classroot.thumb=null;this.target=null;(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
},"startDrag",function($0){
switch(arguments.length){
case 0:
$0=null;

};var $1=this.classroot;$1.targetPosDel.disable();var $2=$1.sizeAxis;this.target=$1.scrolltarget;this.axis=$1.axis;this.trackscroll=this.immediateparent[$2]-this[$2];this.targetscroll=$1.scrollmax-this.target.immediateparent[$2];this[this.axis+"thumbdrag"].setAttribute("applied",true)
},"stopDrag",function($0){
switch(arguments.length){
case 0:
$0=null;

};this[this.axis+"thumbdrag"].setAttribute("applied",false);this.classroot.targetPosDel.enable()
},"ythumbdrag",void 0,"xthumbdrag",void 0,"thumbControl",function($0){
with(this){
var $1=$0-this.doffset;if($1<=0){
$1=0
}else if($1>this.trackscroll){
$1=this.trackscroll
};var $2=Math.round(-$1/this.trackscroll*this.targetscroll);if($2!=this.target[this.classroot.scrollattr]){
this.target.setAttribute(this.classroot.scrollattr,$2)
};return $1
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basescrollthumb","children",[{attrs:{$classrootdepth:1,$m70:function($0){
this.setAttribute("doffset",this.getMouse("y"))
},$m71:function($0){
var $1=this.thumbControl(this.immediateparent.getMouse("y"));if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}},$m72:function(){
return [].concat(this["$lzc$thumbControl_dependencies"]?this["$lzc$thumbControl_dependencies"](this,this,this.immediateparent.getMouse("y")):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"y"):[])
},doffset:new LzOnceExpr("$m70"),name:"ythumbdrag",y:new LzAlwaysExpr("$m71","$m72")},"class":LzState},{attrs:{$classrootdepth:1,$m73:function($0){
this.setAttribute("doffset",this.getMouse("x"))
},$m74:function($0){
var $1=this.thumbControl(this.immediateparent.getMouse("x"));if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}},$m75:function(){
return [].concat(this["$lzc$thumbControl_dependencies"]?this["$lzc$thumbControl_dependencies"](this,this,this.immediateparent.getMouse("x")):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"x"):[])
},doffset:new LzOnceExpr("$m73"),name:"xthumbdrag",x:new LzAlwaysExpr("$m74","$m75")},"class":LzState}],"attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousedown","startDrag",null,"onmouseup","stopDrag",null],axis:"",clickable:true,focusable:false,styleable:false,target:null,targetscroll:0,trackscroll:0},$lzc$class_basescrollthumb.attributes)
}}})($lzc$class_basescrollthumb);Class.make("$lzc$class_basescrollarrow",$lzc$class_basebuttonrepeater,["direction",void 0,"$m76",function($0){
this.classroot.step(this.direction)
},"$m77",function($0){
this.classroot.step(this.direction)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basescrollarrow","attributes",new LzInheritedHash($lzc$class_basebuttonrepeater.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousedown","$m76",null,"onmousestilldown","$m77",null],clickable:true,direction:1},$lzc$class_basescrollarrow.attributes)
}}})($lzc$class_basescrollarrow);Class.make("$lzc$class_basescrolltrack",$lzc$class_basebuttonrepeater,["direction",void 0,"$m78",function($0){
this.classroot.page(this.direction)
},"$m79",function($0){
this.classroot.page(this.direction)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basescrolltrack","attributes",new LzInheritedHash($lzc$class_basebuttonrepeater.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousedown","$m78",null,"onmousestilldown","$m79",null],clickable:true,direction:1},$lzc$class_basescrolltrack.attributes)
}}})($lzc$class_basescrolltrack);Class.make("$lzc$class_stableborderlayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"reset",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked||this.subviews&&this.subviews.length<2){
return
};this.subviews[1].setAttribute(this.axis,this.subviews[0][this.sizeAxis]);this.update()
},"update",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked||this.subviews.length<3){
return
};if($0==null){
var $1=this.immediateparent;if($1.usegetbounds){
$1=$1.getBounds()
};$0=$1[this.sizeAxis]
};this.lock();var $2=this.subviews[1];var $3=this.subviews[2];var $4=$3.usegetbounds?$3.getBounds():$3;var $5=$2.usegetbounds?$2.getBounds():$2;$3.setAttribute(this.axis,$0-$4[this.sizeAxis]-0.1);$2.setAttribute(this.sizeAxis,$0-$4[this.sizeAxis]-$5[this.axis]+0.1);this.locked=false
},"addSubview",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0);if(this.subviews.length==2){
var $1=this.subviews[0];if($1.usegetbounds){
$1=$1.getBounds()
};this.subviews[1].setAttribute(this.axis,$1[this.sizeAxis]);$0.setAttribute(this.sizeAxis,0)
}else if(this.subviews.length>2){
this.update()
}},"setAxis",function($0){
this.axis=$0;this.sizeAxis=$0=="x"?"width":"height";if(this.updateDelegate)this.updateDelegate.register(this.immediateparent,"on"+this.sizeAxis)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","stableborderlayout","attributes",new LzInheritedHash(LzLayout.attributes)]);Class.make("$lzc$class_m104",$lzc$class_basescrolltrack,["$m82",function($0){
with(this){
var $1=parent.thumb.x;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m83",function(){
with(this){
return [parent.thumb,"x"]
}},"$m84",function($0){
with(this){
var $1=parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m85",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_basescrolltrack.attributes)]);Class.make("$lzc$class_m105",LzView,["$m86",function($0){
with(this){
var $1=Math.min(200,parent.width-16);if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m87",function(){
with(this){
return [parent,"width"].concat(Math["$lzc$min_dependencies"]?Math["$lzc$min_dependencies"](this,Math,200,parent.width-16):[])
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m106",$lzc$class_basescrolltrack,["$m88",function($0){
with(this){
var $1=parent.thumb.x+parent.thumb.width;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m89",function(){
with(this){
return [parent.thumb,"x",parent.thumb,"width"]
}},"$m90",function($0){
with(this){
var $1=parent.width-parent.thumb.x-parent.thumb.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m91",function(){
with(this){
return [parent,"width",parent.thumb,"x",parent.thumb,"width"]
}},"$m92",function($0){
with(this){
var $1=parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m93",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_basescrolltrack.attributes)]);Class.make("$lzc$class_m107",LzView,["$m96",function($0){
with(this){
var $1=parent.width;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m97",function(){
with(this){
return [parent,"width"]
}},"$m98",function($0){
with(this){
var $1=parent._enabled||parent.othersb._enabled?12434877:parent.disabledbgcolor;if($1!==this["bgcolor"]||!this.inited){
this.setAttribute("bgcolor",$1)
}}},"$m99",function(){
with(this){
return [parent,"_enabled",parent.othersb,"_enabled",parent,"disabledbgcolor"]
}},"$m100",function($0){
with(this){
var $1=parent.height;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m101",function(){
with(this){
return [parent,"height"]
}},"$m102",function($0){
with(this){
var $1=parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m103",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_hscrollbar",$lzc$class_basescrollbar,["disabledbgcolor",void 0,"$m80",function(){
with(this){
var $0=canvas;return $0
}},"$m81",function($0){
this._showEnabled()
},"_showEnabled",function(){
with(this){
if(!_enabled){
var $0=this.disabledbgcolor;if($0==null){
var $1=immediateparent;while($1.bgcolor==null&&$1!=canvas){
$1=$1.immediateparent
};$0=$1.bgcolor;if($0==null)$0=16777215
};this.setAttribute("bgcolor",$0)
}else{
this.setAttribute("bgcolor",5855577)
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_showEnabled"]||this.nextMethod(arguments.callee,"_showEnabled")).call(this)
}},"leftarrow",void 0,"scrolltrack",void 0,"patch",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","hscrollbar","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,name:"leftarrow"},children:[{attrs:{$classrootdepth:2,direction:-1,resource:"lzscrollbar_xbuttonleft_rsc",x:1,y:1},"class":$lzc$class_basescrollarrow}],"class":LzView},{attrs:{$classrootdepth:1,bottom:void 0,height:12,name:"scrolltrack",thumb:void 0,top:void 0,y:1},children:[{attrs:{$classrootdepth:2,direction:-1,disabledResourceNumber:3,downResourceNumber:2,height:new LzAlwaysExpr("$m84","$m85"),name:"top",overResourceNumber:0,resource:"lzscrollbar_xtrack_rsc",stretches:"width",width:new LzAlwaysExpr("$m82","$m83")},"class":$lzc$class_m104},{attrs:{$classrootdepth:2,name:"thumb",y:0},children:[{attrs:{$classrootdepth:3,resource:"lzscrollbar_xthumbleft_rsc"},"class":LzView},{attrs:{$classrootdepth:3,resource:"lzscrollbar_xthumbmiddle_rsc",stretches:"both"},"class":LzView},{attrs:{$classrootdepth:3,resource:"lzscrollbar_xthumbright_rsc"},"class":LzView},{attrs:{$classrootdepth:3,axis:"x"},"class":$lzc$class_stableborderlayout},{attrs:{$classrootdepth:3,align:"center",clip:true,resource:"lzscrollbar_xthumbgripper_rsc",width:new LzAlwaysExpr("$m86","$m87"),y:1},"class":$lzc$class_m105}],"class":$lzc$class_basescrollthumb},{attrs:{$classrootdepth:2,disabledResourceNumber:3,downResourceNumber:2,height:new LzAlwaysExpr("$m92","$m93"),name:"bottom",overResourceNumber:0,resource:"lzscrollbar_xtrack_rsc",stretches:"width",width:new LzAlwaysExpr("$m90","$m91"),x:new LzAlwaysExpr("$m88","$m89")},"class":$lzc$class_m106}],"class":LzView},{attrs:{$classrootdepth:1,rightarrow:void 0,width:14},children:[{attrs:{$classrootdepth:2,name:"rightarrow",resource:"lzscrollbar_xbuttonright_rsc",y:1},"class":$lzc$class_basescrollarrow}],"class":LzView},{attrs:{$classrootdepth:1,axis:"x"},"class":$lzc$class_stableborderlayout},{attrs:{$classrootdepth:1,$m94:function($0){
with(this){
var $1=parent.othersb;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}}},$m95:function(){
with(this){
return [parent,"othersb"]
}},applied:new LzAlwaysExpr("$m94","$m95"),patch:void 0},children:[{attrs:{$classrootdepth:1,bgcolor:new LzAlwaysExpr("$m98","$m99"),height:new LzAlwaysExpr("$m102","$m103"),name:"patch",width:new LzAlwaysExpr("$m100","$m101"),x:new LzAlwaysExpr("$m96","$m97")},"class":$lzc$class_m107}],"class":LzState}],$lzc$class_basescrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_basescrollbar.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m81","$m80"],axis:"x",bgcolor:LzColorUtils.convertColor("0x595959"),disabledbgcolor:null,height:14},$lzc$class_hscrollbar.attributes)
}}})($lzc$class_hscrollbar);Class.make("$lzc$class_swatchview",LzView,["ctransform",void 0,"color",void 0,"construct",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);this.capabilities=new LzInheritedHash(this.capabilities);this.capabilities.colortransform=true;if($1["width"]==null){
$1["width"]=this.immediateparent.width
};if($1["height"]==null){
$1["height"]=this.immediateparent.height
};if($1["fgcolor"]==null&&$1["bgcolor"]==null){
$1["fgcolor"]=16777215
}}},"$lzc$set_fgcolor",function($0){
this.setAttribute("bgcolor",$0)
},"$lzc$set_bgcolor",function($0){
with(this){
this.color=$0;if(this.ctransform!=null){
var $1=$0>>16&255;var $2=$0>>8&255;var $3=$0&255;$1=$1*ctransform["ra"]/100+ctransform["rb"];$1=Math.min($1,255);$2=$2*ctransform["ga"]/100+ctransform["gb"];$2=Math.min($2,255);$3=$3*ctransform["ba"]/100+ctransform["bb"];$3=Math.min($3,255);$0=Math.floor($3+($2<<8)+($1<<16))
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_bgcolor"]||this.nextMethod(arguments.callee,"$lzc$set_bgcolor")).call(this,$0)
}},"setColorTransform",function($0){
this.ctransform=$0;this.setAttribute("bgcolor",this.color)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","swatchview","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({color:16777215,ctransform:null},$lzc$class_swatchview.attributes)
}}})($lzc$class_swatchview);Class.make("$lzc$class_m153",LzView,["$m119",function($0){
with(this){
var $1=parent.width-1;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m120",function(){
with(this){
return [parent,"width"]
}},"$m121",function($0){
with(this){
var $1=parent.height-1;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m122",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m154",LzView,["$m123",function($0){
with(this){
var $1=parent.width-3;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m124",function(){
with(this){
return [parent,"width"]
}},"$m125",function($0){
with(this){
var $1=parent.height-3;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m126",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m155",LzView,["$m127",function($0){
with(this){
var $1=parent.width-4;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m128",function(){
with(this){
return [parent,"width"]
}},"$m129",function($0){
with(this){
var $1=parent.height-4;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m130",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m156",LzView,["$m131",function($0){
with(this){
var $1=parent.parent.width-2;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m132",function(){
with(this){
return [parent.parent,"width"]
}},"$m133",function($0){
with(this){
var $1=parent.parent.height-2;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m134",function(){
with(this){
return [parent.parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m157",LzView,["$m135",function($0){
with(this){
var $1=parent.parent.height-2;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m136",function(){
with(this){
return [parent.parent,"height"]
}},"$m137",function($0){
with(this){
var $1=parent.parent.width-3;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m138",function(){
with(this){
return [parent.parent,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m158",LzView,["$m139",function($0){
with(this){
var $1=parent.parent.width-1;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m140",function(){
with(this){
return [parent.parent,"width"]
}},"$m141",function($0){
with(this){
var $1=parent.parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m142",function(){
with(this){
return [parent.parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m159",LzView,["$m143",function($0){
with(this){
var $1=parent.parent.height-1;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m144",function(){
with(this){
return [parent.parent,"height"]
}},"$m145",function($0){
with(this){
var $1=parent.parent.width-1;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m146",function(){
with(this){
return [parent.parent,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m160",LzText,["$m147",function($0){
with(this){
var $1=parent.text_x+parent.titleshift;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m148",function(){
with(this){
return [parent,"text_x",parent,"titleshift"]
}},"$m149",function($0){
with(this){
var $1=parent.text_y+parent.titleshift;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m150",function(){
with(this){
return [parent,"text_y",parent,"titleshift"]
}},"$m151",function($0){
with(this){
var $1=parent.text;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m152",function(){
with(this){
return [parent,"text"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_button",$lzc$class_basebutton,["text_padding_x",void 0,"text_padding_y",void 0,"$m108",function($0){
var $1=this.width/2-this._title.width/2;if($1!==this["text_x"]||!this.inited){
this.setAttribute("text_x",$1)
}},"$m109",function(){
return [this,"width",this._title,"width"]
},"text_x",void 0,"$m110",function($0){
var $1=this.height/2-this._title.height/2;if($1!==this["text_y"]||!this.inited){
this.setAttribute("text_y",$1)
}},"$m111",function(){
return [this,"height",this._title,"height"]
},"text_y",void 0,"$m112",function($0){
var $1=this._title.width+2*this.text_padding_x;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}},"$m113",function(){
return [this._title,"width",this,"text_padding_x"]
},"$m114",function($0){
var $1=this._title.height+2*this.text_padding_y;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}},"$m115",function(){
return [this._title,"height",this,"text_padding_y"]
},"buttonstate",void 0,"$m116",function($0){
var $1=this.buttonstate==1?0:1;if($1!==this["titleshift"]||!this.inited){
this.setAttribute("titleshift",$1)
}},"$m117",function(){
return [this,"buttonstate"]
},"titleshift",void 0,"leftalign",void 0,"_showEnabled",function(){
with(this){
showUp();setAttribute("clickable",_enabled)
}},"showDown",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.hasdefault){
this._outerbezel.setAttribute("frame",5)
}else{
this._outerbezel.setAttribute("frame",this.downResourceNumber)
};this._face.setAttribute("frame",this.downResourceNumber);this._innerbezel.setAttribute("frame",this.downResourceNumber);setAttribute("buttonstate",2)
}},"showUp",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(_enabled){
if(this.hasdefault){
this._outerbezel.setAttribute("frame",5)
}else{
this._outerbezel.setAttribute("frame",this.normalResourceNumber)
};this._face.setAttribute("frame",this.normalResourceNumber);this._innerbezel.setAttribute("frame",this.normalResourceNumber);if(this.style)this._title.setAttribute("fgcolor",this.style.textcolor)
}else{
if(this.style)this._title.setAttribute("fgcolor",this.style.textdisabledcolor);this._face.setAttribute("frame",this.disabledResourceNumber);this._outerbezel.setAttribute("frame",this.disabledResourceNumber);this._innerbezel.setAttribute("frame",this.disabledResourceNumber)
};setAttribute("buttonstate",1)
}},"showOver",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.hasdefault){
this._outerbezel.setAttribute("frame",5)
}else{
this._outerbezel.setAttribute("frame",this.overResourceNumber)
};this._face.setAttribute("frame",this.overResourceNumber);this._innerbezel.setAttribute("frame",this.overResourceNumber);setAttribute("buttonstate",1)
}},"$m118",function($0){
with(this){
if(this._initcomplete){
if(this.buttonstate==1)showUp()
}}},"_applystyle",function($0){
with(this){
if(this.style!=null){
this.textcolor=$0.textcolor;this.textdisabledcolor=$0.textdisabledcolor;if(enabled){
_title.setAttribute("fgcolor",$0.textcolor)
}else{
_title.setAttribute("fgcolor",$0.textdisabledcolor)
};setTint(_outerbezel,$0.basecolor);setTint(_innerbezel,$0.basecolor);setTint(_face,$0.basecolor)
}}},"_outerbezel",void 0,"_innerbezel",void 0,"_face",void 0,"_innerbezelbottom",void 0,"_outerbezelbottom",void 0,"_title",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","button","children",[{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0x919191"),height:new LzAlwaysExpr("$m121","$m122"),name:"_outerbezel",width:new LzAlwaysExpr("$m119","$m120"),x:0,y:0},"class":$lzc$class_m153},{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0xffffff"),height:new LzAlwaysExpr("$m125","$m126"),name:"_innerbezel",width:new LzAlwaysExpr("$m123","$m124"),x:1,y:1},"class":$lzc$class_m154},{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m129","$m130"),name:"_face",resource:"lzbutton_face_rsc",stretches:"both",width:new LzAlwaysExpr("$m127","$m128"),x:2,y:2},"class":$lzc$class_m155},{attrs:{$classrootdepth:1,name:"_innerbezelbottom"},children:[{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0x585858"),height:new LzAlwaysExpr("$m133","$m134"),width:1,x:new LzAlwaysExpr("$m131","$m132"),y:1},"class":$lzc$class_m156},{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0x585858"),height:1,width:new LzAlwaysExpr("$m137","$m138"),x:1,y:new LzAlwaysExpr("$m135","$m136")},"class":$lzc$class_m157}],"class":LzView},{attrs:{$classrootdepth:1,name:"_outerbezelbottom"},children:[{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0xffffff"),height:new LzAlwaysExpr("$m141","$m142"),opacity:0.7,width:1,x:new LzAlwaysExpr("$m139","$m140"),y:0},"class":$lzc$class_m158},{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0xffffff"),height:1,opacity:0.7,width:new LzAlwaysExpr("$m145","$m146"),x:0,y:new LzAlwaysExpr("$m143","$m144")},"class":$lzc$class_m159}],"class":LzView},{attrs:{$classrootdepth:1,name:"_title",resize:true,text:new LzAlwaysExpr("$m151","$m152"),x:new LzAlwaysExpr("$m147","$m148"),y:new LzAlwaysExpr("$m149","$m150")},"class":$lzc$class_m160}],"attributes",new LzInheritedHash($lzc$class_basebutton.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onhasdefault","$m118",null],buttonstate:1,clickable:true,doesenter:true,focusable:true,height:new LzAlwaysExpr("$m114","$m115"),leftalign:false,maxframes:4,pixellock:true,styleable:true,text_padding_x:11,text_padding_y:4,text_x:new LzAlwaysExpr("$m108","$m109"),text_y:new LzAlwaysExpr("$m110","$m111"),titleshift:new LzAlwaysExpr("$m116","$m117"),width:new LzAlwaysExpr("$m112","$m113")},$lzc$class_button.attributes)
}}})($lzc$class_button);Class.make("$lzc$class_basevaluecomponent",$lzc$class_basecomponent,["value",void 0,"type",void 0,"getValue",function(){
return this.value==null?this.text:this.value
},"$lzc$getValue_dependencies",function($0,$1){
return [this,"value",this,"text"]
},"acceptValue",function($0,$1){
switch(arguments.length){
case 1:
$1=null;

};if($1==null)$1=this.type;(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["acceptValue"]||this.nextMethod(arguments.callee,"acceptValue")).call(this,$0,$1);this.acceptAttribute("value",$1,$0)
},"presentValue",function($0){
switch(arguments.length){
case 0:
$0=null;

};if($0==null)$0=this.type;return this.presentTypeValue($0,this.getValue())
},"$lzc$presentValue_dependencies",function($0,$1,$2){
switch(arguments.length){
case 2:
$2=null;

};return [this,"type"].concat(this.$lzc$getValue_dependencies($0,$1))
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basevaluecomponent","attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({type:"",value:null},$lzc$class_basevaluecomponent.attributes)
}}})($lzc$class_basevaluecomponent);Class.make("$lzc$class_baseformitem",$lzc$class_basevaluecomponent,["_parentform",void 0,"submitname",void 0,"$m161",function($0){
with(this){
var $1=enabled;if($1!==this["submit"]||!this.inited){
this.setAttribute("submit",$1)
}}},"$m162",function(){
return [this,"enabled"]
},"submit",void 0,"changed",void 0,"$lzc$set_changed",function($0){
this.setChanged($0)
},"$lzc$set_value",function($0){
this.setValue($0,false)
},"onchanged",void 0,"onvalue",void 0,"rollbackvalue",void 0,"ignoreform",void 0,"init",function(){
if(this.submitname=="")this.submitname=this.name;if(this.submitname==""){};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);var $0=this.findForm();if($0!=null){
$0.addFormItem(this);this._parentform=$0
}},"destroy",function(){
if(this._parentform)this._parentform.removeFormItem(this);(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
},"setChanged",function($0,$1){
with(this){
switch(arguments.length){
case 1:
$1=null;

};if(!this._initcomplete){
this.changed=false;return
};var $2=this.changed;this.changed=$0;if(this.changed!=$2){
if(this.onchanged)this.onchanged.sendEvent(this.changed)
};if(!$1&&this.changed&&!ignoreform){
if(this["_parentform"]&&this._parentform["changed"]!=undefined&&!this._parentform.changed){
this._parentform.setChanged($0,false)
}};if(!$1&&!this.changed&&!ignoreform){
if(this["_parentform"]&&this._parentform["changed"]!=undefined&&this._parentform.changed){
this._parentform.setChanged($0,true)
}}}},"rollback",function(){
if(this.rollbackvalue!=this["value"]){
this.setAttribute("value",this.rollbackvalue)
};this.setAttribute("changed",false)
},"commit",function(){
this.rollbackvalue=this.value;this.setAttribute("changed",false)
},"setValue",function($0,$1){
switch(arguments.length){
case 1:
$1=null;

};var $2=this.value!=$0;this.value=$0;if($1||!this._initcomplete){
this.rollbackvalue=$0
};this.setChanged($2&&!$1&&this.rollbackvalue!=$0);if(this["onvalue"])this.onvalue.sendEvent($0)
},"acceptValue",function($0,$1){
switch(arguments.length){
case 1:
$1=null;

};if($1==null)$1=this.type;this.setValue(this.acceptTypeValue($1,$0),true)
},"findForm",function(){
with(this){
if(_parentform!=null){
return _parentform
}else{
var $0=this.immediateparent;var $1=null;while($0!=canvas){
if($0["formdata"]){
$1=$0;break
};$0=$0.immediateparent
};return $1
}}},"toXML",function($0){
with(this){
var $1=this.value;if($0){
if(typeof $1=="boolean")$1=$1-0
};return lz.Browser.xmlEscape(this.submitname)+'="'+lz.Browser.xmlEscape($1)+'"'
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","baseformitem","attributes",new LzInheritedHash($lzc$class_basevaluecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_parentform:null,changed:false,ignoreform:false,onchanged:LzDeclaredEvent,onvalue:LzDeclaredEvent,rollbackvalue:null,submit:new LzAlwaysExpr("$m161","$m162"),submitname:"",value:null},$lzc$class_baseformitem.attributes)
}}})($lzc$class_baseformitem);Class.make("$lzc$class_listselector",LzSelectionManager,["multiselect",void 0,"_forcemulti",void 0,"isRangeSelect",function($0){
return this.multiselect&&(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["isRangeSelect"]||this.nextMethod(arguments.callee,"isRangeSelect")).call(this,$0)
},"isMultiSelect",function($0){
return this._forcemulti||this.multiselect&&(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["isMultiSelect"]||this.nextMethod(arguments.callee,"isMultiSelect")).call(this,$0)
},"select",function($0){
with(this){
if(this.multiselect&&(Array["$lzsc$isa"]?Array.$lzsc$isa($0):$0 instanceof Array)){
this._forcemulti=true;for(var $1=0;$1<$0.length;$1++){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["select"]||this.nextMethod(arguments.callee,"select")).call(this,$0[$1])
};this._forcemulti=false
}else{
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["select"]||this.nextMethod(arguments.callee,"select")).call(this,$0)
}}},"getValue",function(){
with(this){
var $0=this.getSelection();if($0.length==0)return null;if($0.length==1&&!multiselect){
return $0[0].getValue()
};var $1=[];for(var $2=0;$2<$0.length;$2++){
$1[$2]=$0[$2].getValue()
};return $1
}},"getText",function(){
with(this){
var $0=this.getSelection();if($0.length==0)return null;if($0.length==1&&!multiselect){
return $0[0].text
};var $1=[];for(var $2=0;$2<$0.length;$2++){
$1[$2]=$0[$2].text
};return $1
}},"getNumItems",function(){
if(!this.immediateparent.subviews)return 0;return this.immediateparent.subviews.length
},"getNextSubview",function($0,$1){
with(this){
switch(arguments.length){
case 1:
$1=1;

};if(typeof $1=="undefined")$1=1;var $2=this.immediateparent.subviews;if(!$0){
if($1>0){
return $2[0]
}else{
return $2[$2.length-1]
}};var $3;var $4=$2.length;for(var $5=0;$5<$4;$5++){
var $6=$2[$5];if($6==$0){
var $7=$5+$1;if($7<0){
$3=$2[0]
}else if($7>=$4){
$3=$2[$4-1]
}else{
$3=$2[$7]
};break
}};ensureItemInView($3);return $3
}},"ensureItemInView",function($0){
with(this){
if(!$0){
return
};var $1=immediateparent.parent;var $2=false;if($0.y+$0.height>$1.height-immediateparent.y){
var $3=$1.height-immediateparent.y-($0.y+$0.height);var $4=Math.max($1.height-immediateparent.height,immediateparent.y+$3);immediateparent.setAttribute("y",$4);$2=true
}else if(immediateparent.y*-1>$0.y){
var $3=immediateparent.y*-1-$0.y;var $4=Math.min(0,immediateparent.y+$3);immediateparent.setAttribute("y",$4);$2=true
};if($2){
this._updatefromscrolling=true
}}},"_updatefromscrolling",void 0,"allowhilite",function($0){
if(this._updatefromscrolling){
if($0!=null)this._updatefromscrolling=false;return false
};return true
},"getItemByIndex",function($0){
return this.parent._contentview.subviews[$0]
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","listselector","attributes",new LzInheritedHash(LzSelectionManager.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_forcemulti:false,_updatefromscrolling:false,multiselect:false},$lzc$class_listselector.attributes)
}}})($lzc$class_listselector);Class.make("$lzc$class_datalistselector",LzDataSelectionManager,["multiselect",void 0,"_forcemulti",void 0,"isRangeSelect",function($0){
return this.multiselect&&(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["isRangeSelect"]||this.nextMethod(arguments.callee,"isRangeSelect")).call(this,$0)
},"isMultiSelect",function($0){
return this._forcemulti||this.multiselect&&(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["isMultiSelect"]||this.nextMethod(arguments.callee,"isMultiSelect")).call(this,$0)
},"select",function($0){
with(this){
if(this.multiselect&&(Array["$lzsc$isa"]?Array.$lzsc$isa($0):$0 instanceof Array)){
this._forcemulti=true;for(var $1=0;$1<$0.length;$1++){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["select"]||this.nextMethod(arguments.callee,"select")).call(this,$0[$1])
};this._forcemulti=false
}else{
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["select"]||this.nextMethod(arguments.callee,"select")).call(this,$0)
}}},"getValue",function(){
with(this){
var $0=this.getSelection();if($0.length==0)return null;var $1=this.immediateparent.subviews[0]._valuedatapath;if(!$1)$1=this.immediateparent.subviews[0]._textdatapath;if(!$1)$1="text()";if($0.length==1&&!multiselect){
return $0[0].xpathQuery($1)
}else{
var $2=[];for(var $3=0;$3<$0.length;$3++){
$2[$3]=$0[$3].xpathQuery($1)
};return $2
}}},"getText",function(){
with(this){
var $0=this.getSelection();if($0.length==0)return null;var $1=this.immediateparent.subviews[0]._textdatapath;if(!$1)$1="text()";if($0.length==1&&!multiselect){
return $0[0].xpathQuery($1)
}else{
var $2=[];for(var $3=0;$3<$0.length;$3++){
$2[$3]=$0[$3].xpathQuery($1)
};return $2
}}},"getNumItems",function(){
with(this){
if(!this.cloneManager){
var $0=immediateparent.subviews;if($0==null||$0.length==0){
return 0
}else{
this.cloneManager=$0[0].cloneManager
}};if(this.cloneManager!=null){
if(!this.cloneManager["nodes"]){
return 0
}else{
return this.cloneManager.nodes.length
}}else if($0[0].data){
return 1
}else{
return 0
}}},"getNextSubview",function($0,$1){
with(this){
switch(arguments.length){
case 1:
$1=1;

};var $2=immediateparent.subviews[0].cloneManager["clones"];if($0==null){
var $3=$1==-1&&parent.shownitems!=-1?parent.shownitems-1:0;return $2[$3]
};var $4=findIndex($0);if($4==-1)return null;var $5=immediateparent.subviews[0].cloneManager.nodes;if(!$1)$1=1;$4+=$1;if($4==-1)$4=0;if($4==$5.length)$4=$5.length-1;_ensureItemInViewByIndex($4);var $6=$5[$4];var $7=immediateparent.subviews;for(var $8=0;$8<$7.length;$8++){
if($7[$8].datapath.p==$6){
return $7[$8]
}}}},"findIndex",function($0){
with(this){
if(!immediateparent.subviews[0].cloneManager){
if($0 instanceof lz.view){
return immediateparent.subviews[0]==$0?0:-1
}else{
return immediateparent.subviews[0].datapath.p==$0.p?0:-1
}};var $1;if($0 instanceof lz.view){
$1=$0.datapath.p
}else{
$1=$0.p
};var $2=immediateparent.subviews[0].cloneManager.nodes;var $3=-1;if($2!=null){
for(var $4=0;$4<$2.length;$4++){
if($2[$4]==$1){
$3=$4;break
}}};return $3
}},"ensureItemInView",function($0){
with(this){
if(!$0)return;var $1=findIndex($0);if($1!=-1)_ensureItemInViewByIndex($1)
}},"_ensureItemInViewByIndex",function($0){
with(this){
var $1=this.immediateparent;var $2=$1.subviews;if(!$2||$2.length==0){
return
};var $3=$2[0].height;var $4=$0*$3;var $5=0;if($0>0){
var $6=$2[0].cloneManager;if(parent["spacing"]){
$5=parent.spacing
}else if($6&&$6["spacing"]){
$5=$6.spacing
};$4+=$5*($0-1)
};var $7=false;var $8=$1.parent.height;var $9=$1.y;if($4+$3>$8-$9){
var $a=$8-$9-($4+$3+$5);var $b=Math.max($8-$1.height,$9+$a);$1.setAttribute("y",$b);$7=true
}else if($9*-1>$4){
var $a=$9*-1-$4-$5;var $b=Math.min(0,$9+$a);$1.setAttribute("y",$b);$7=true
};if($7){
this._updatefromscrolling=true
}}},"getItemByIndex",function($0){
with(this){
var $1=immediateparent.subviews;if(!$1||$1.length==0)return null;this._ensureItemInViewByIndex($0);var $2=$1[0].cloneManager;if($2==null){
return $0==0?$1[0]:undefined
};var $3=$2.clones[0].datapath.xpathQuery("position()")-1;return $2.clones[$0-$3]
}},"getItemByData",function($0){
with(this){
return $0?getItemByIndex(this.getItemIndexByData($0)):null
}},"getItemIndexByData",function($0){
with(this){
if($0){
var $1=immediateparent.subviews;if($1[0].cloneManager){
var $2=$1[0].cloneManager["nodes"];if($2!=null){
for(var $3=0;$3<$2.length;$3++){
if($2[$3]==$0){
return $3
}}}}else if($1[0].datapath.p==$0){
return 0
}};return null
}},"_updatefromscrolling",void 0,"allowhilite",function($0){
if(this._updatefromscrolling){
if($0!=null)this._updatefromscrolling=false;return false
};return true
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","datalistselector","attributes",new LzInheritedHash(LzDataSelectionManager.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_forcemulti:false,_updatefromscrolling:false,multiselect:false},$lzc$class_datalistselector.attributes)
}}})($lzc$class_datalistselector);Class.make("$lzc$class_baselist",$lzc$class_baseformitem,["itemclassname",void 0,"__itemclass",void 0,"defaultselection",void 0,"multiselect",void 0,"toggleselected",void 0,"dataoption",void 0,"_hiliteview",void 0,"_contentview",void 0,"_initialselection",void 0,"_selector",void 0,"__focusfromchild",void 0,"onselect",void 0,"onitemclassname",void 0,"doEnterDown",function(){
with(this){
if((lz.view["$lzsc$isa"]?lz.view.$lzsc$isa(this._hiliteview):this._hiliteview instanceof lz.view)&&this._hiliteview.enabled){
this._hiliteview.setAttribute("selected",true)
}}},"doEnterUp",function(){
return
},"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);if(this._contentview==null){
if(this.defaultplacement!=null){
this._contentview=this.searchSubnodes("name",this.defaultplacement)
}else{
this._contentview=this
}};if(this.dataoption=="lazy"||this.dataoption=="resize"){
this._selector=new (lz.datalistselector)(this,{multiselect:this.multiselect,toggle:toggleselected})
}else{
this._selector=new (lz.listselector)(this,{multiselect:this.multiselect,toggle:toggleselected})
};if(this._initialselection!=null){
this.select(this._initialselection)
}else if(this.defaultselection!=null){
selectItemAt(defaultselection)
}}},"_doFocus",function($0){
with(this){
if(this["_selector"]!=null){
var $1=this._selector.getSelection();if($1&&$1.length>0){
if(lz.view["$lzsc$isa"]?lz.view.$lzsc$isa($1[0]):$1[0] instanceof lz.view){
this._hiliteview=$1[0];this._hiliteview.setHilite(true)
}}}}},"_doblur",function($0){
with(this){
if(lz.view["$lzsc$isa"]?lz.view.$lzsc$isa(this._hiliteview):this._hiliteview instanceof lz.view){
this._hiliteview.setHilite(false)
};this._hiliteview=null
}},"setHilite",function($0){
with(this){
if(this._selector.allowhilite($0)){
if(lz.view["$lzsc$isa"]?lz.view.$lzsc$isa(this._hiliteview):this._hiliteview instanceof lz.view)this._hiliteview.setHilite(false);this._hiliteview=$0;if(lz.view["$lzsc$isa"]?lz.view.$lzsc$isa($0):$0 instanceof lz.view){
$0.setHilite(true)
}}}},"_dokeydown",function($0){
with(this){
var $1=this._hiliteview;if($1==null){
$1=getSelection();if(this.multiselect)$1=$1[$1.length-1]
};if(this.focusable&&$0==32){
if((lz.view["$lzsc$isa"]?lz.view.$lzsc$isa($1):$1 instanceof lz.view)&&$1.enabled){
$1.setAttribute("selected",true);$1.setHilite(true);this._hiliteview=$1
};return
};if(this.focusable&&$0>=37&&$0<=40){
this.setAttribute("doesenter",true);var $2;if($0==39||$0==40){
$2=_selector.getNextSubview($1)
};if($0==37||$0==38){
$2=_selector.getNextSubview($1,-1)
};if(lz.view["$lzsc$isa"]?lz.view.$lzsc$isa($1):$1 instanceof lz.view){
$1.setHilite(false)
};if($2.enabled&&_selector.isRangeSelect($2)){
$2.setAttribute("selected",true)
};$2.setHilite(true);this._hiliteview=$2
}}},"getValue",function(){
with(this){
return _selector.getValue()
}},"getText",function(){
with(this){
if(_initcomplete)return _selector.getText();return null
}},"getSelection",function(){
with(this){
if(this._initcomplete){
var $0=this._selector.getSelection();if(multiselect){
return $0
}else{
if($0.length==0){
return null
}else{
return $0[0]
}}}else{
return this._initialselection
}}},"selectNext",function(){
with(this){
moveSelection(1)
}},"selectPrev",function(){
with(this){
moveSelection(-1)
}},"moveSelection",function($0){
with(this){
if(!$0)$0=1;var $1=this._selector.getSelection();var $2;if($1.length==0){
$2=this._contentview.subviews[0]
}else{
var $3=$1[0];$2=this._selector.getNextSubview($3,$0)
};var $4=lz.Focus.getFocus();select($2);if($3&&$4&&$4.childOf($3)){
lz.Focus.setFocus($2)
}}},"getNumItems",function(){
if(this["_selector"]==null)return 0;return this._selector.getNumItems()
},"getItemAt",function($0){
with(this){
if(_contentview.subviews[$0]){
return getItem(_contentview.subviews[$0].getValue())
};return null
}},"getItem",function($0){
with(this){
if(_contentview!=null&&_contentview.subviews!=null){
for(var $1=0;$1<_contentview.subviews.length;$1++){
var $2=_contentview.subviews[$1];if($2.getValue()==$0){
return $2
}}};return null
}},"addItem",function($0,$1){
switch(arguments.length){
case 1:
$1=null;

};new (this.__itemclass)(this,{text:$0,value:$1})
},"$lzc$set_itemclassname",function($0){
with(this){
this.itemclassname=$0;this.__itemclass=lz[$0];if(onitemclassname.ready){
this.onitemclassname.sendEvent($0)
}}},"removeItem",function($0){
with(this){
var $1=getItem($0);_removeitem($1)
}},"removeItemAt",function($0){
with(this){
var $1=_contentview.subviews[$0];_removeitem($1)
}},"removeAllItems",function(){
with(this){
while(_contentview.subviews.length!=0){
for(var $0=0;$0<_contentview.subviews.length;$0++){
_removeitem(_contentview.subviews[$0])
}}}},"_removeitem",function($0){
if($0){
if($0.selected)this._selector.unselect($0);$0.destroy()
}},"selectItem",function($0){
with(this){
var $1=getItem($0);if($1){
select($1)
}}},"selectItemAt",function($0){
with(this){
if(this._selector!=null){
var $1=this._selector.getItemByIndex($0);select($1)
}}},"clearSelection",function(){
if(this._initcomplete){
this._selector.clearSelection()
}else{
this._initialselection=null;this.defaultselection=null
}},"select",function($0){
with(this){
if($0==null){

}else if(this._initcomplete){
this._selector.select($0);if(!this.multiselect){
this.setAttribute("value",$0.getValue())
}}else{
if(multiselect){
if(this._initialselection==null)this._initialselection=[];this._initialselection.push($0)
}else{
this._initialselection=$0
}};if((lz.view["$lzsc$isa"]?lz.view.$lzsc$isa(this._hiliteview):this._hiliteview instanceof lz.view)&&this._hiliteview["enabled"]){
this._hiliteview.setHilite(false);this._hiliteview=null
};this.setAttribute("doesenter",false);if(this.onselect)this.onselect.sendEvent($0)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","baselist","attributes",new LzInheritedHash($lzc$class_baseformitem.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onfocus","_doFocus",null,"onblur","_doblur",null,"onkeydown","_dokeydown",null],__focusfromchild:false,__itemclass:null,_contentview:null,_hiliteview:null,_initialselection:null,_selector:null,dataoption:"none",defaultselection:null,itemclassname:"",multiselect:false,onitemclassname:LzDeclaredEvent,onselect:LzDeclaredEvent,toggleselected:false},$lzc$class_baselist.attributes)
}}})($lzc$class_baselist);Class.make("$lzc$class_basetrackgroup",LzView,["$m163",function($0){
var $1=this;if($1!==this["boundsref"]||!this.inited){
this.setAttribute("boundsref",$1)
}},"$m164",function(){
return []
},"boundsref",void 0,"$lzc$set_boundsref",function($0){
this.setBoundsRef($0)
},"onboundsref",void 0,"trackingrate",void 0,"tracking",void 0,"$lzc$set_tracking",function($0){
with(this){
setTracking($0)
}},"ontracking",void 0,"_trackgroup",void 0,"_boundstrackgroup",void 0,"activateevents",void 0,"deactivateevents",void 0,"_activateDL",void 0,"_deactivateDL",void 0,"_repeattrackDL",void 0,"_destroyDL",void 0,"onmousetrackoutbottom",void 0,"onmousetrackouttop",void 0,"onmousetrackoutright",void 0,"onmousetrackoutleft",void 0,"construct",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,$0,$1);this._activateDL=new LzDelegate(this,"activateTrackgroup");this._deactivateDL=new LzDelegate(this,"deactivateTrackgroup");this._repeattrackDL=new LzDelegate(this,"trackingout");this._destroyDL=new LzDelegate(this,"destroyitem");this._trackgroup="tg"+this.getUID();this._boundstrackgroup="btg"+this.getUID()
}},"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);lz.Track.register(this.boundsref,this._boundstrackgroup)
}},"destroy",function(){
with(this){
this.setTracking(false);lz.Track.unregister(this.boundsref,this._boundstrackgroup);if(this._destroyDL){
this._destroyDL.unregisterAll();this._destroyDL=null
};if(this._activateDL){
this._activateDL.unregisterAll();this._activateDL=null
};if(this._deactivateDL){
this._deactivateDL.unregisterAll();this._deactivateDL=null
};if(this._repeattrackDL){
this._repeattrackDL.unregisterAll();this._repeattrackDL=null
};this.activateevents=null;this.deactivateevents=null;(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
}},"setBoundsRef",function($0){
this.boundsref=$0;if(this.onboundsref)this.onboundsref.sendEvent()
},"setTracking",function($0){
with(this){
this.tracking=$0;if(this.isinited){
if($0){
lz.Track.activate(this._trackgroup);lz.Track.activate(this._boundstrackgroup)
}else{
lz.Track.deactivate(this._trackgroup);lz.Track.deactivate(this._boundstrackgroup)
}};if(this.ontracking)this.ontracking.sendEvent($0)
}},"activateTrackgroup",function($0){
this.setTracking(true);this._destroyDL.register($0,"ondestroy")
},"deactivateTrackgroup",function($0){
this.setTracking(false)
},"destroyitem",function($0){
this.setTracking(false)
},"$m165",function($0){
with(this){
lz.Track.register($0,this._trackgroup);if(this.activateevents){
var $1=this._activateDL;var $2=this.activateevents;for(var $3=0;$3<$2.length;++$3){
$1.register($0,$2[$3])
}};if(this.deactivateevents){
var $4=this._deactivateDL;var $5=this.deactivateevents;for(var $3=0;$3<$5.length;++$3){
$4.register($0,$5[$3])
}}}},"$m166",function($0){
with(this){
lz.Track.unregister($0,this._trackgroup);if(this.activateevents){
var $1=this._activateDL;var $2=this.activateevents;for(var $3=0;$3<$2.length;++$3){
$1.unregisterFrom($0[$2[$3]])
}};if(this.deactivateevents){
var $4=this._deactivateDL;var $5=this.deactivateevents;for(var $3=0;$3<$5.length;++$3){
$4.unregisterFrom($0[$5[$3]])
}}}},"$m167",function(){
var $0=this.boundsref;return $0
},"trackingout",function($0){
with(this){
if(this.tracking){
lz.Timer.addTimer(this._repeattrackDL,this.trackingrate)
};var $1=this.boundsref.getMouse("x");var $2=this.boundsref.getMouse("y");if($1<=0){
if(this.boundsref.onmousetrackoutleft)this.boundsref.onmousetrackoutleft.sendEvent($1)
}else if($1>=this.boundsref.width){
if(this.boundsref.onmousetrackoutright)this.boundsref.onmousetrackoutright.sendEvent($1)
};if($2<=0){
if(this.boundsref.onmousetrackouttop)this.boundsref.onmousetrackouttop.sendEvent($2)
}else if($2>=this.boundsref.height){
if(this.boundsref.onmousetrackoutbottom)this.boundsref.onmousetrackoutbottom.sendEvent($2-this.boundsref.height)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basetrackgroup","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onaddsubview","$m165",null,"onremovesubview","$m166",null,"onmousetrackout","trackingout","$m167"],_activateDL:null,_deactivateDL:null,_destroyDL:null,_repeattrackDL:null,activateevents:["onmousedown"],boundsref:new LzAlwaysExpr("$m163","$m164"),deactivateevents:["onmouseup"],onboundsref:LzDeclaredEvent,onmousetrackoutbottom:LzDeclaredEvent,onmousetrackoutleft:LzDeclaredEvent,onmousetrackoutright:LzDeclaredEvent,onmousetrackouttop:LzDeclaredEvent,ontracking:LzDeclaredEvent,tracking:true,trackingrate:150},$lzc$class_basetrackgroup.attributes)
}}})($lzc$class_basetrackgroup);Class.make("$lzc$class_m182",$lzc$class_basescrolltrack,["$m170",function($0){
with(this){
var $1=parent.thumb.y;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m171",function(){
with(this){
return [parent.thumb,"y"]
}},"$m172",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m173",function(){
with(this){
return [parent,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_basescrolltrack.attributes)]);Class.make("$lzc$class_m183",LzView,["$m174",function($0){
with(this){
var $1=Math.min(200,parent.height-16);if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m175",function(){
with(this){
return [parent,"height"].concat(Math["$lzc$min_dependencies"]?Math["$lzc$min_dependencies"](this,Math,200,parent.height-16):[])
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m184",$lzc$class_basescrolltrack,["$m176",function($0){
with(this){
var $1=parent.thumb.y+parent.thumb.height;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m177",function(){
with(this){
return [parent.thumb,"y",parent.thumb,"height"]
}},"$m178",function($0){
with(this){
var $1=parent.height-parent.thumb.y-parent.thumb.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m179",function(){
with(this){
return [parent,"height",parent.thumb,"y",parent.thumb,"height"]
}},"$m180",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m181",function(){
with(this){
return [parent,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_basescrolltrack.attributes)]);Class.make("$lzc$class_vscrollbar",$lzc$class_basescrollbar,["disabledbgcolor",void 0,"$m168",function(){
with(this){
var $0=canvas;return $0
}},"$m169",function($0){
this._showEnabled()
},"_showEnabled",function(){
with(this){
if(!_enabled){
var $0=this.disabledbgcolor;if($0==null){
var $1=immediateparent;while($1.bgcolor==null&&$1!=canvas){
$1=$1.immediateparent
};$0=$1.bgcolor;if($0==null)$0=16777215
};this.setAttribute("bgcolor",$0)
}else{
this.setAttribute("bgcolor",5855577)
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_showEnabled"]||this.nextMethod(arguments.callee,"_showEnabled")).call(this)
}},"toparrow",void 0,"scrolltrack",void 0,"bottomarrow",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","vscrollbar","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,name:"toparrow"},children:[{attrs:{$classrootdepth:2,direction:-1,resource:"lzscrollbar_ybuttontop_rsc",x:1,y:1},"class":$lzc$class_basescrollarrow}],"class":LzView},{attrs:{$classrootdepth:1,bottom:void 0,name:"scrolltrack",thumb:void 0,top:void 0},children:[{attrs:{$classrootdepth:2,direction:-1,disabledResourceNumber:3,downResourceNumber:2,height:new LzAlwaysExpr("$m170","$m171"),name:"top",overResourceNumber:0,resource:"lzscrollbar_ytrack_rsc",stretches:"height",width:new LzAlwaysExpr("$m172","$m173"),x:1},"class":$lzc$class_m182},{attrs:{$classrootdepth:2,name:"thumb",x:1},children:[{attrs:{$classrootdepth:3,resource:"lzscrollbar_ythumbtop_rsc"},"class":LzView},{attrs:{$classrootdepth:3,resource:"lzscrollbar_ythumbmiddle_rsc",stretches:"both"},"class":LzView},{attrs:{$classrootdepth:3,resource:"lzscrollbar_ythumbbottom_rsc"},"class":LzView},{attrs:{$classrootdepth:3,axis:"y"},"class":$lzc$class_stableborderlayout},{attrs:{$classrootdepth:3,clip:true,height:new LzAlwaysExpr("$m174","$m175"),resource:"lzscrollbar_ythumbgripper_rsc",valign:"middle",width:11,x:1},"class":$lzc$class_m183}],"class":$lzc$class_basescrollthumb},{attrs:{$classrootdepth:2,disabledResourceNumber:3,downResourceNumber:2,height:new LzAlwaysExpr("$m178","$m179"),name:"bottom",overResourceNumber:0,resource:"lzscrollbar_ytrack_rsc",stretches:"height",width:new LzAlwaysExpr("$m180","$m181"),x:1,y:new LzAlwaysExpr("$m176","$m177")},"class":$lzc$class_m184}],"class":LzView},{attrs:{$classrootdepth:1,height:14,name:"bottomarrow"},children:[{attrs:{$classrootdepth:2,resource:"lzscrollbar_ybuttonbottom_rsc",x:1},"class":$lzc$class_basescrollarrow}],"class":LzView},{attrs:{$classrootdepth:1,axis:"y"},"class":$lzc$class_stableborderlayout}],$lzc$class_basescrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_basescrollbar.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m169","$m168"],axis:"y",bgcolor:LzColorUtils.convertColor("0x595959"),disabledbgcolor:null,width:14},$lzc$class_vscrollbar.attributes)
}}})($lzc$class_vscrollbar);Class.make("$lzc$class_simplelayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"inset",void 0,"$lzc$set_inset",function($0){
this.inset=$0;if(this.subviews&&this.subviews.length)this.update();if(this["oninset"])this.oninset.sendEvent(this.inset)
},"spacing",void 0,"$lzc$set_spacing",function($0){
this.spacing=$0;if(this.subviews&&this.subviews.length)this.update();if(this["onspacing"])this.onspacing.sendEvent(this.spacing)
},"setAxis",function($0){
if(this["axis"]==null||this.axis!=$0){
this.axis=$0;this.sizeAxis=$0=="x"?"width":"height";if(this.subviews.length)this.update();if(this["onaxis"])this.onaxis.sendEvent(this.axis)
}},"addSubview",function($0){
this.updateDelegate.register($0,"on"+this.sizeAxis);this.updateDelegate.register($0,"onvisible");if(!this.locked){
var $1=null;var $2=this.subviews;for(var $3=$2.length-1;$3>=0;--$3){
if($2[$3].visible){
$1=$2[$3];break
}};if($1){
var $4=$1[this.axis]+$1[this.sizeAxis]+this.spacing
}else{
var $4=this.inset
};$0.setAttribute(this.axis,$4)
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0)
},"update",function($0){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;var $1=this.subviews.length;var $2=this.inset;for(var $3=0;$3<$1;$3++){
var $4=this.subviews[$3];if(!$4.visible)continue;if($4[this.axis]!=$2){
$4.setAttribute(this.axis,$2)
};if($4.usegetbounds){
$4=$4.getBounds()
};$2+=this.spacing+$4[this.sizeAxis]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","simplelayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({axis:"y",inset:0,spacing:0},$lzc$class_simplelayout.attributes)
}}})($lzc$class_simplelayout);Class.make("$lzc$class_m215",$lzc$class_simplelayout,["$m207",function($0){
with(this){
var $1=classroot.spacing;if($1!==this["spacing"]||!this.inited){
this.setAttribute("spacing",$1)
}}},"$m208",function(){
with(this){
return [classroot,"spacing"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_simplelayout.attributes)]);Class.make("$lzc$class_m214",$lzc$class_basetrackgroup,["$m201",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m202",function(){
with(this){
return [immediateparent,"width"]
}},"$m203",function($0){
with(this){
var $1=classroot.tracking;if($1!==this["tracking"]||!this.inited){
this.setAttribute("tracking",$1)
}}},"$m204",function(){
with(this){
return [classroot,"tracking"]
}},"$m205",function($0){
with(this){
var $1=parent;if($1!==this["boundsref"]||!this.inited){
this.setAttribute("boundsref",$1)
}}},"$m206",function(){
return [this,"parent"]
},"$m209",function($0){
with(this){
if(classroot.itemclassname==""){
classroot.__itemclass=$0.constructor
};if((classroot.dataoption=="lazy"||classroot.dataoption=="resize")&&!classroot._itemheight){
classroot._itemheight=$0.height
};classroot.adjustmyheight()
}},"$m210",function($0){
with(this){
classroot.adjustmyheight()
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:3,axis:"y",spacing:new LzAlwaysExpr("$m207","$m208")},"class":$lzc$class_m215}],"attributes",new LzInheritedHash($lzc$class_basetrackgroup.attributes)]);Class.make("$lzc$class_m213",LzView,["$m193",function($0){
with(this){
var $1=classroot.border_left;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m194",function(){
with(this){
return [classroot,"border_left"]
}},"$m195",function($0){
with(this){
var $1=classroot.border_top;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m196",function(){
with(this){
return [classroot,"border_top"]
}},"$m197",function($0){
with(this){
var $1=classroot.width-classroot.border_right-classroot.border_left;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m198",function(){
with(this){
return [classroot,"width",classroot,"border_right",classroot,"border_left"]
}},"$m199",function($0){
with(this){
var $1=classroot._bgcolor;if($1!==this["bgcolor"]||!this.inited){
this.setAttribute("bgcolor",$1)
}}},"$m200",function(){
with(this){
return [classroot,"_bgcolor"]
}},"_sbar",void 0,"onmousetrackoutleft",void 0,"onmousetrackoutright",void 0,"content",void 0,"ensurevscrollbar",function(){
with(this){
if(this._sbar==null){
var $0=classroot.scrollbarclassname;if($0==""){
$0="vscrollbar"
};if($0&&lz[$0]){
this._sbar=new (lz[$0])(this,{stepsize:"20"})
}}}},"showvscrollbar",function(){
with(this){
if(this._sbar==null){
this.ensurevscrollbar()
};this._sbar.setAttribute("visible",true);classroot.setAttribute("rightinset",this._sbar.width)
}},"hidevscrollbar",function(){
with(this){
if(this._sbar!=null){
this._sbar.setAttribute("visible",false)
};classroot.setAttribute("rightinset",0)
}},"vscrollbarisvisible",function(){
return this._sbar!=null&&this._sbar.visible
},"$m211",function($0){
with(this){
if(this.vscrollbarisvisible())_sbar.step(1)
}},"$m212",function($0){
with(this){
if(this.vscrollbarisvisible())_sbar.step(-1)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,$delegates:["onaddsubview","$m209",null,"onheight","$m210",null],boundsref:new LzAlwaysExpr("$m205","$m206"),deactivateevents:["onmouseup","onselect"],name:"content",tracking:new LzAlwaysExpr("$m203","$m204"),width:new LzAlwaysExpr("$m201","$m202")},"class":$lzc$class_m214}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_list",$lzc$class_baselist,["rightinset",void 0,"bordersize",void 0,"$m185",function($0){
var $1=this.bordersize;if($1!==this["border_top"]||!this.inited){
this.setAttribute("border_top",$1)
}},"$m186",function(){
return [this,"bordersize"]
},"border_top",void 0,"$m187",function($0){
var $1=this.bordersize;if($1!==this["border_left"]||!this.inited){
this.setAttribute("border_left",$1)
}},"$m188",function(){
return [this,"bordersize"]
},"border_left",void 0,"$m189",function($0){
var $1=this.bordersize;if($1!==this["border_right"]||!this.inited){
this.setAttribute("border_right",$1)
}},"$m190",function(){
return [this,"bordersize"]
},"border_right",void 0,"$m191",function($0){
var $1=this.bordersize;if($1!==this["border_bottom"]||!this.inited){
this.setAttribute("border_bottom",$1)
}},"$m192",function(){
return [this,"bordersize"]
},"border_bottom",void 0,"tracking",void 0,"spacing",void 0,"minheight",void 0,"shownitems",void 0,"$lzc$set_shownitems",function($0){
this._setShownItems($0)
},"onshownitems",void 0,"scrollable",void 0,"autoscrollbar",void 0,"scrollbarclassname",void 0,"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);if(this._hasSetHeight)this.setAttribute("height",this.height);adjustmyheight()
}},"interior",void 0,"_setShownItems",function($0){
with(this){
this.shownitems=$0;if(onshownitems)this.onshownitems.sendEvent();if(this._initcomplete)this.adjustmyheight()
}},"select",function($0){
with(this){
var $1=$0;if($1&&$1["length"]>0){
$1=$1[0]
};ensureItemInView($1);(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["select"]||this.nextMethod(arguments.callee,"select")).call(this,$0)
}},"_doFocus",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_doFocus"]||this.nextMethod(arguments.callee,"_doFocus")).call(this,$0);if(!this.__focusfromchild){
var $1=getSelection();if(this.multiselect){
$1=$1.length==0?null:$1[0]
};ensureItemInView($1)
}}},"ensureItemInView",function($0){
with(this){
if(!$0)return;if(_initcomplete){
_selector.ensureItemInView($0)
}}},"_itemheight",void 0,"calcMyHeight",function(){
with(this){
var $0=getNumItems();if($0==0){
return this.minheight
};var $1;if(shownitems>-1&&shownitems<$0||dataoption=="lazy"||dataoption=="resize"){
var $2;if(this.dataoption=="lazy"||dataoption=="resize"){
$2=this._itemheight;if(shownitems<$0)$0=shownitems
}else{
$0=shownitems;$2=this.interior.content.subviews[0].height
};$1=$2*$0+spacing*($0-1)
}else{
$1=this.interior.content.height
};return $1
}},"_hasSetHeight",void 0,"_heightinternal",void 0,"$lzc$set_height",function($0){
with(this){
if($0!=null&&!this._heightinternal){
this._hasSetHeight=true
}else{
this._hasSetHeight=false;if(!this._heightinternal){
var $1=this.calcMyHeight();$0=$1+border_top+border_bottom
}};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_height"]||this.nextMethod(arguments.callee,"$lzc$set_height")).call(this,$0);if(this._initcomplete){
interior.setAttribute("height",$0-border_top-border_bottom);checkscrollbar()
}}},"checkscrollbar",function(){
if(this.autoscrollbar){
if(this._contentview.height>this.interior.height){
this.interior.showvscrollbar()
}else{
this.interior.hidevscrollbar()
}}},"adjustmyheight",function(){
with(this){
if(!this._initcomplete)return;if(this._hasSetHeight){
checkscrollbar()
}else{
var $0=this.calcMyHeight();this._heightinternal=true;this.setAttribute("height",$0+border_top+border_bottom);this._heightinternal=false
}}},"addItem",function($0,$1){
switch(arguments.length){
case 1:
$1=null;

};if(this.itemclassname==""){
this.setAttribute("itemclassname","textlistitem")
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addItem"]||this.nextMethod(arguments.callee,"addItem")).call(this,$0,$1);this.adjustmyheight()
},"_setbordercolor",void 0,"_bgcolor",void 0,"$lzc$set_bgcolor",function($0){
if(this._setbordercolor){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_bgcolor"]||this.nextMethod(arguments.callee,"$lzc$set_bgcolor")).call(this,$0)
}else{
this._bgcolor=$0;var $1=this["onbgcolor"];if($1&&$1.ready){
$1.sendEvent($0)
}}},"_applystyle",function($0){
if(this.style!=null){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_applystyle"]||this.nextMethod(arguments.callee,"_applystyle")).call(this,$0);this._setbordercolor=true;this.setAttribute("bgcolor",$0.bordercolor);this._setbordercolor=false;if(this._bgcolor==null)this.interior.setAttribute("bgcolor",$0.bgcolor)
}},"destroy",function(){
if(this.autoscrollbar)this.setAttribute("autoscrollbar",false);if(this.shownitems!=-1)this.setAttribute("shownitems",-1);(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","list","children",[{attrs:{$classrootdepth:1,$delegates:["onmousetrackoutbottom","$m211",null,"onmousetrackouttop","$m212",null],_sbar:null,bgcolor:new LzAlwaysExpr("$m199","$m200"),clip:true,content:void 0,name:"interior",onmousetrackoutleft:LzDeclaredEvent,onmousetrackoutright:LzDeclaredEvent,width:new LzAlwaysExpr("$m197","$m198"),x:new LzAlwaysExpr("$m193","$m194"),y:new LzAlwaysExpr("$m195","$m196")},"class":$lzc$class_m213},{attrs:"content","class":$lzc$class_userClassPlacement}],"attributes",new LzInheritedHash($lzc$class_baselist.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_bgcolor:null,_hasSetHeight:false,_heightinternal:false,_itemheight:null,_setbordercolor:false,autoscrollbar:true,border_bottom:new LzAlwaysExpr("$m191","$m192"),border_left:new LzAlwaysExpr("$m187","$m188"),border_right:new LzAlwaysExpr("$m189","$m190"),border_top:new LzAlwaysExpr("$m185","$m186"),bordersize:1,minheight:24,onshownitems:LzDeclaredEvent,rightinset:0,scrollable:false,scrollbarclassname:"vscrollbar",shownitems:-1,spacing:0,tracking:false,width:100},$lzc$class_list.attributes)
}}})($lzc$class_list);Class.make("$lzc$class_baselistitem",$lzc$class_basevaluecomponent,["selected",void 0,"$lzc$set_selected",function($0){
this._setSelected($0)
},"onselected",void 0,"onselect",void 0,"_selectonevent",void 0,"$lzc$set__selectonevent",function($0){
this.setSelectOnEvent($0)
},"$lzc$set_datapath",function($0){
with(this){
if(null!=this.datapath){
this.datapath.setXPath($0)
}else{
var $1={xpath:$0};if(this._parentcomponent.dataoption=="lazy"||this._parentcomponent.dataoption=="resize"){
$1.replication=_parentcomponent.dataoption;if(parent["spacing"])$1.spacing=parent.spacing
}else if(this._parentcomponent.dataoption=="pooling"){
$1.pooling=true
};new (lz.datapath)(this,$1)
}}},"_valuedatapath",void 0,"_textdatapath",void 0,"dataBindAttribute",function($0,$1,$2){
if(this._parentcomponent.dataoption=="lazy"||this._parentcomponent.dataoption=="resize"){
if($0=="text"){
this._textdatapath=$1
}else if($0=="value")this._valuedatapath=$1
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["dataBindAttribute"]||this.nextMethod(arguments.callee,"dataBindAttribute")).call(this,$0,$1,$2)
},"setSelectOnEvent",function($0){
with(this){
this._selectDL=new LzDelegate(this,"doClick",this,$0)
}},"doClick",function($0){
if(this._parentcomponent){
this._parentcomponent.select(this)
}},"_doMousedown",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_doMousedown"]||this.nextMethod(arguments.callee,"_doMousedown")).call(this,$0);var $1=this._parentcomponent;if(!this.focusable&&$1&&$1.focusable){
$1.__focusfromchild=true;lz.Focus.setFocus($1,false);$1.__focusfromchild=false
}}},"setSelected",function($0){
this.selected=$0;if(this.onselect.ready)this.onselect.sendEvent(this);if(this.onselected.ready)this.onselected.sendEvent(this)
},"_setSelected",function($0){
with(this){
this.selected=$0;if($0){
parent.select(this)
}}},"setHilite",function($0){},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","baselistitem","attributes",new LzInheritedHash($lzc$class_basevaluecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_selectonevent:"onclick",_textdatapath:null,_valuedatapath:null,clickable:true,focusable:false,onselect:LzDeclaredEvent,onselected:LzDeclaredEvent,selected:false},$lzc$class_baselistitem.attributes)
}}})($lzc$class_baselistitem);Class.make("$lzc$class_listitem",$lzc$class_baselistitem,["$m216",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m217",function(){
with(this){
return [immediateparent,"width"]
}},"$m218",function($0){
with(this){
this.setAttribute("_ipclassroot",immediateparent.classroot)
}},"_ipclassroot",void 0,"hilited",void 0,"$m219",function($0){
with(this){
_ipclassroot.setHilite(this)
}},"$m220",function($0){
with(this){
if(!immediateparent.tracking){
_ipclassroot.setHilite(this)
}}},"$m221",function($0){
with(this){
if(!immediateparent.tracking){
_ipclassroot.setHilite(null)
}}},"setSelected",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["setSelected"]||this.nextMethod(arguments.callee,"setSelected")).call(this,$0);if(this._initcomplete)_applystyle(this.style)
}},"setHilite",function($0){
with(this){
if($0!=this.hilited){
this.hilited=$0;if(this._initcomplete)_applystyle(this.style)
}}},"$m222",function(){
with(this){
var $0=immediateparent.classroot;return $0
}},"$m223",function($0){
with(this){
lz.Track.register(this,$0)
}},"_applystyle",function($0){
with(this){
if(this.style!=null){
if(_enabled){
if(selected){
this.setAttribute("bgcolor",style.selectedcolor)
}else if(hilited){
this.setAttribute("bgcolor",style.hilitecolor)
}else this.setAttribute("bgcolor",style.textfieldcolor)
}else{
this.setAttribute("bgcolor",style.textfieldcolor)
}}}},"_showEnabled",function(){
with(this){
setAttribute("clickable",_enabled);if(_initcomplete){
_applystyle(this.style)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","listitem","attributes",new LzInheritedHash($lzc$class_baselistitem.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousetrackover","$m219",null,"onmouseover","$m220",null,"onmouseout","$m221",null,"ontrackgroup","$m223","$m222"],_ipclassroot:new LzOnceExpr("$m218"),_selectonevent:"onmousetrackup",clickable:true,height:20,hilited:false,width:new LzAlwaysExpr("$m216","$m217")},$lzc$class_listitem.attributes)
}}})($lzc$class_listitem);Class.make("$lzc$class_m234",LzText,["$m226",function($0){
with(this){
var $1=classroot.text_x;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m227",function(){
with(this){
return [classroot,"text_x"]
}},"$m228",function($0){
with(this){
var $1=classroot.text_y;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m229",function(){
with(this){
return [classroot,"text_y"]
}},"$m230",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m231",function(){
with(this){
return [immediateparent,"width"]
}},"$m232",function($0){
with(this){
var $1=parent.text;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m233",function(){
with(this){
return [parent,"text"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_textlistitem",$lzc$class_listitem,["text_x",void 0,"$m224",function($0){
var $1=this.height/2-this._title.height/2;if($1!==this["text_y"]||!this.inited){
this.setAttribute("text_y",$1)
}},"$m225",function(){
return [this,"height",this._title,"height"]
},"text_y",void 0,"_title",void 0,"_applystyle",function($0){
var $1=this["style"];if($1!=null){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_applystyle"]||this.nextMethod(arguments.callee,"_applystyle")).call(this,$0);var $2;if(this._enabled){
if(this.hilited){
$2=$1.texthilitecolor
}else if(this.selected){
$2=$1.textselectedcolor
}else{
$2=$1.textcolor
}}else{
$2=$1.textdisabledcolor
};this._title.setAttribute("fgcolor",$2)
}},"_showEnabled",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_showEnabled"]||this.nextMethod(arguments.callee,"_showEnabled")).call(this);if(_initcomplete){
_applystyle(this.style)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","textlistitem","children",[{attrs:{$classrootdepth:1,name:"_title",text:new LzAlwaysExpr("$m232","$m233"),width:new LzAlwaysExpr("$m230","$m231"),x:new LzAlwaysExpr("$m226","$m227"),y:new LzAlwaysExpr("$m228","$m229")},"class":$lzc$class_m234}],"attributes",new LzInheritedHash($lzc$class_listitem.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({text_x:4,text_y:new LzAlwaysExpr("$m224","$m225")},$lzc$class_textlistitem.attributes)
}}})($lzc$class_textlistitem);Class.make("$lzc$class_basefloatinglist",$lzc$class_list,["owner",void 0,"wouldbename",void 0,"attach",void 0,"_currentattachy",void 0,"_currentattachx",void 0,"attachtarget",void 0,"$lzc$set_attachtarget",function($0){
this.setAttachTarget($0)
},"attachoffset",void 0,"_updateAttachPosDel",void 0,"_origshownitems",void 0,"$m235",function($0){
with(this){
var $1=attachtarget?attachtarget["style"]:null;if($1!==this["style"]||!this.inited){
this.setAttribute("style",$1)
}}},"$m236",function(){
return [this,"attachtarget"]
},"setAttachTarget",function($0){
with(this){
this.attachtarget=$0;if(visible)updateAttachLocation();var $1=this._updateAttachPosDel;if($1){
$1.unregisterAll();if($0){
for(var $2=$0;$2!==canvas;$2=$2.immediateparent){
$1.register($2,"onx");$1.register($2,"ony")
}}}}},"construct",function($0,$1){
with(this){
this.owner=$0;if(typeof $1.name!="undefined"){
var $2=$1.name;$1.name=null;this.wouldbename=$2;this.owner[$2]=this
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["construct"]||this.nextMethod(arguments.callee,"construct")).call(this,canvas,$1);var $3=this.owner;while($3!==canvas){
if($3 instanceof lz.basecomponent){
this._parentcomponent=$3;break
};$3=$3.immediateparent
};new (lz.Delegate)(this,"_handledestroy",$0,"ondestroy");this._updateAttachPosDel=new (lz.Delegate)(this,"_doUpdateAttachLocation")
}},"_handledestroy",function($0){
this.destroy()
},"destroy",function(){
with(this){
if(this.owner!=null){
if(this["wouldbename"]!=null){
this.owner[this.wouldbename]=null
};this.owner=null
};this.setAttachTarget(null);this._updateAttachPosDel=null;var $0=canvas.subviews;for(var $1=$0.length-1;$1>=0;$1--){
if($0[$1]===this){
$0.splice($1,1);break
}};var $2=canvas.subnodes;for(var $1=$2.length-1;$1>=0;$1--){
if($2[$1]===this){
$2.splice($1,1);break
}};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["destroy"]||this.nextMethod(arguments.callee,"destroy")).call(this)
}},"init",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);if(this.attachtarget==null){
this.setAttachTarget(owner)
}}},"getMenuCapHeight",function(){
return 0
},"_constraintsApplied",void 0,"_setScroll",function(){
with(this){
var $0=this.interior.content.subviews[0];var $1=$0?$0.height:20;var $2=attachtarget.getAttributeRelative("y",canvas);var $3=getMenuCapHeight();var $4=$2;var $5=canvas.height-($2+attachtarget.height+attachoffset+$3);var $6=0;var $7="top";var $8=$4;if($5>$4){
$7="bottom";$6=$2+attachtarget.height+attachoffset;$8=$5
};var $9=Math.floor(($8+spacing-$3)/($1+spacing));var $a=$9*($1+spacing)-spacing+$3;if(attach=="left"||attach=="right"){
$a+=$1+spacing;$9++;if($7=="bottom"){
$6-=attachtarget.height+attachoffset
}else{
$6+=attachtarget.height
}};if($7=="top"){
$6+=$4-$a+$3
};this.setAttribute("y",$6);this.setAttribute("_currentattachy",$7);this._keepshownitems=true;this._setShownItems($9);this._keepshownitems=false;this.setAttribute("scrollable",true)
}},"_keepshownitems",void 0,"_setShownItems",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_setShownItems"]||this.nextMethod(arguments.callee,"_setShownItems")).call(this,$0);if(this._origshownitems==-2||!this._keepshownitems){
this._origshownitems=$0
}},"_doUpdateAttachLocation",function($0){
this.updateAttachLocation()
},"updateAttachLocation",function(){
with(this){
if(!isinited)return;if(!attachtarget)return;var $0=false;var $1=attach;var $2=attachtarget.getAttributeRelative("x",canvas);while(true){
if($1=="bottom"||$1=="top"){
var $3=$2;if($3<0){
$3=0
}else if($3+attachtarget.width>canvas.width){
$3=canvas.width-this.width
}else if($3+this.width>canvas.width){
$3=$2+attachtarget.width-this.width
};this.setAttribute("x",$3);break
}else if($1=="left"){
var $3=$2-this.width;if($3>0){
this.setAttribute("x",$3);this.setAttribute("_currentattachx","left");break
}else{
$1="right"
}};if($1=="right"){
var $3=$2+attachtarget.width;if($3+this.width<canvas.width){
this.setAttribute("x",$3);this.setAttribute("_currentattachx","right");break
}else{
if(!$0){
$1="left";$0=true
}else{
break
}}}};this._keepshownitems=true;this._setShownItems(this._origshownitems);this._keepshownitems=false;this.setAttribute("scrollable",false);var $0=false;var $1=attach;var $4=getMenuCapHeight();var $5=this.calcMyHeight()+$4;var $6=attachtarget.getAttributeRelative("y",canvas);while(true){
if($1=="left"||$1=="right"){
if($6+$5<canvas.height){
this.setAttribute("y",$6);this.setAttribute("_currentattachy","bottom");break
}else{
$1="top"
}}else if($1=="bottom"){
var $7=$6+attachtarget.height+attachoffset;if($7+$5<canvas.height){
this.setAttribute("y",$7);this.setAttribute("_currentattachy","bottom");break
}else{
$1="top"
}};if($1=="top"){
var $7=$6-$5;if(attach=="right"||attach=="left")$7+=attachtarget.height;if($7>0){
this.setAttribute("y",$7+$4);this.setAttribute("_currentattachy","top");break
}else{
if(!$0){
$1="bottom";$0=true
}else{
this._setScroll();break
}}}}}},"$m237",function($0){
this.owner.onmousedown.sendEvent();this.bringToFront()
},"$m238",function($0){
this.updateAttachLocation()
},"$m239",function($0){
this.updateAttachLocation()
},"$m240",function($0){
with(this){
if($0){
updateAttachLocation()
}else{
this.setAttribute("x",-1000);this.setAttribute("y",-1000)
}}},"toString",function(){
return "floatinglist: wouldbename,owner = "+this.wouldbename+","+this.owner
},"getNextSelection",function(){
with(this){
var $0=owner.getNextSelection();return $0
}},"getPrevSelection",function(){
with(this){
return owner.resolveSelection()
}},"$m241",function($0){
if(this.owner!="undefined"&&this.owner["onblur"]){
this.owner.onblur.sendEvent()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","basefloatinglist","children",LzNode.mergeChildren([],$lzc$class_list["children"]),"attributes",new LzInheritedHash($lzc$class_list.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousedown","$m237",null,"onwidth","$m238",null,"onheight","$m239",null,"onvisible","$m240",null,"onblur","$m241",null],_constraintsApplied:false,_currentattachx:"bottom",_currentattachy:"bottom",_keepshownitems:false,_origshownitems:-2,_updateAttachPosDel:null,attach:"bottom",attachoffset:0,attachtarget:null,clickable:true,options:{ignorelayout:true},style:new LzAlwaysExpr("$m235","$m236"),wouldbename:"",x:-1000,y:-1000},$lzc$class_basefloatinglist.attributes)
}}})($lzc$class_basefloatinglist);Class.make("$lzc$class_m255",LzView,["$m243",function($0){
with(this){
var $1=classroot.offsety;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m244",function(){
with(this){
return [classroot,"offsety"]
}},"$m245",function($0){
with(this){
var $1=parent.width-classroot.inset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m246",function(){
with(this){
return [parent,"width",classroot,"inset"]
}},"$m247",function($0){
with(this){
var $1=parent.height-classroot.offsety;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m248",function(){
with(this){
return [parent,"height",classroot,"offsety"]
}},"top",void 0,"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,name:"top",resource:"shadowTR"},"class":LzView},{attrs:{$classrootdepth:2,resource:"shadowMR",stretches:"height"},"class":LzView},{attrs:{$classrootdepth:2,height:0},"class":LzView},{attrs:{$classrootdepth:2,axis:"y"},"class":$lzc$class_stableborderlayout}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m256",LzView,["$m249",function($0){
with(this){
var $1=parent.bottomvisible;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m250",function(){
with(this){
return [parent,"bottomvisible"]
}},"$m251",function($0){
with(this){
var $1=parent.height;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m252",function(){
with(this){
return [parent,"height"]
}},"$m253",function($0){
with(this){
var $1=parent.width+parent.right.width-classroot.inset;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m254",function(){
with(this){
return [parent,"width",parent.right,"width",classroot,"inset"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,resource:"shadowBL"},"class":LzView},{attrs:{$classrootdepth:2,resource:"shadowBM",stretches:"width"},"class":LzView},{attrs:{$classrootdepth:2,resource:"shadowBR"},"class":LzView},{attrs:{$classrootdepth:2,axis:"x"},"class":$lzc$class_stableborderlayout}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class__floatshadow",LzView,["$m242",function($0){
with(this){
this.setCornerResourceNumber(cornerresourcenumber)
}},"inset",void 0,"offsety",void 0,"shadowsize",void 0,"bottomvisible",void 0,"$lzc$set_bottomvisible",function($0){
this.setBottomVisible($0)
},"onbottomvisible",void 0,"cornerresourcenumber",void 0,"$lzc$set_cornerresourcenumber",function($0){
this.setCornerResourceNumber($0)
},"oncornerresourcenumber",void 0,"right",void 0,"bottom",void 0,"setBottomVisible",function($0){
with(this){
this.bottomvisible=$0;if(onbottomvisible)this.onbottomvisible.sendEvent($0);this.setCornerResourceNumber(this.cornerresourcenumber)
}},"setCornerResourceNumber",function($0){
with(this){
if(!this.bottomvisible){
this.cornerresourcenumber=3
}else{
this.cornerresourcenumber=2
};if(!isinited)return;this.right.top.setAttribute("frame",this.cornerresourcenumber);if(oncornerresourcenumber)this.oncornerresourcenumber.sendEvent()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","_floatshadow","children",[{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m247","$m248"),name:"right",top:void 0,x:new LzAlwaysExpr("$m245","$m246"),y:new LzAlwaysExpr("$m243","$m244")},"class":$lzc$class_m255},{attrs:{$classrootdepth:1,name:"bottom",visible:new LzAlwaysExpr("$m249","$m250"),width:new LzAlwaysExpr("$m253","$m254"),y:new LzAlwaysExpr("$m251","$m252")},"class":$lzc$class_m256}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m242",null],bottomvisible:true,cornerresourcenumber:0,inset:10,offsety:10,onbottomvisible:LzDeclaredEvent,oncornerresourcenumber:LzDeclaredEvent,shadowsize:5},$lzc$class__floatshadow.attributes)
}}})($lzc$class__floatshadow);Class.make("$lzc$class_m287",$lzc$class__floatshadow,["$m267",function($0){
with(this){
var $1=classroot._currentattachy!="top";if($1!==this["bottomvisible"]||!this.inited){
this.setAttribute("bottomvisible",$1)
}}},"$m268",function(){
with(this){
return [classroot,"_currentattachy"]
}},"$m269",function($0){
with(this){
var $1=classroot.shadowoffsety;if($1!==this["offsety"]||!this.inited){
this.setAttribute("offsety",$1)
}}},"$m270",function(){
with(this){
return [classroot,"shadowoffsety"]
}},"$m271",function($0){
with(this){
var $1=classroot.shadowcrn;if($1!==this["cornerresourcenumber"]||!this.inited){
this.setAttribute("cornerresourcenumber",$1)
}}},"$m272",function(){
with(this){
return [classroot,"shadowcrn"]
}},"$m273",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m274",function(){
with(this){
return [immediateparent,"width"]
}},"$m275",function($0){
with(this){
var $1=immediateparent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m276",function(){
with(this){
return [immediateparent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class__floatshadow["children"]),"attributes",new LzInheritedHash($lzc$class__floatshadow.attributes)]);Class.make("$lzc$class_m288",LzView,["$m277",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m278",function(){
with(this){
return [immediateparent,"width"]
}},"$m279",function($0){
with(this){
var $1=immediateparent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m280",function(){
with(this){
return [immediateparent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m286",LzView,["$m263",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m264",function(){
with(this){
return [immediateparent,"width"]
}},"$m265",function($0){
with(this){
var $1=immediateparent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m266",function(){
with(this){
return [immediateparent,"height"]
}},"shdw",void 0,"borderview",void 0,"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,bottomvisible:new LzAlwaysExpr("$m267","$m268"),cornerresourcenumber:new LzAlwaysExpr("$m271","$m272"),height:new LzAlwaysExpr("$m275","$m276"),name:"shdw",offsety:new LzAlwaysExpr("$m269","$m270"),opacity:0.6,width:new LzAlwaysExpr("$m273","$m274")},"class":$lzc$class_m287},{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0x808080"),height:new LzAlwaysExpr("$m279","$m280"),name:"borderview",width:new LzAlwaysExpr("$m277","$m278")},"class":$lzc$class_m288}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m289",LzView,["$m281",function($0){
with(this){
var $1=parent._currentattachy=="top"?-height+1:parent.height;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m282",function(){
with(this){
return [parent,"_currentattachy",this,"height",parent,"height"]
}},"$m283",function($0){
with(this){
var $1=classroot.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m284",function(){
with(this){
return [classroot,"width"]
}},"$m285",function($0){
this.setAttribute("frame",1)
},"$lzc$set_frame",function($0){
var $1=this.subviews.length;for(var $2=0;$2<$1;$2++){
this.subviews[$2].setAttribute("frame",$0)
}},"$lzc$set_y",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_y"]||this.nextMethod(arguments.callee,"$lzc$set_y")).call(this,$0);if($0<0){
this.setAttribute("frame",1)
}else{
this.setAttribute("frame",2)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,resource:"menucap_lft"},"class":LzView},{attrs:{$classrootdepth:2,resource:"menucap_mid",stretches:"width"},"class":LzView},{attrs:{$classrootdepth:2,resource:"menucap_rt"},"class":LzView},{attrs:{$classrootdepth:2,axis:"x"},"class":$lzc$class_stableborderlayout}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_floatinglist",$lzc$class_basefloatinglist,["$m257",function($0){
var $1=this._currentattachy=="bottom"?0:this.bordersize;if($1!==this["border_bottom"]||!this.inited){
this.setAttribute("border_bottom",$1)
}},"$m258",function(){
return [this,"_currentattachy",this,"bordersize"]
},"$m259",function($0){
var $1=this._currentattachy=="top"?0:this.bordersize;if($1!==this["border_top"]||!this.inited){
this.setAttribute("border_top",$1)
}},"$m260",function(){
return [this,"_currentattachy",this,"bordersize"]
},"shadowcrn",void 0,"$m261",function($0){
var $1=this._currentattachy=="bottom"?3:-9;if($1!==this["shadowoffsety"]||!this.inited){
this.setAttribute("shadowoffsety",$1)
}},"$m262",function(){
return [this,"_currentattachy"]
},"shadowoffsety",void 0,"bkgnd",void 0,"menucap",void 0,"_applystyle",function($0){
with(this){
if(this.style!=null){
setTint(this.menucap,$0.basecolor)
}}},"init",function(){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this.bkgnd.sendToBack();this.bringToFront()
},"$lzc$set_bgcolor",function($0){
with(this){
if(!isinited)return;this.bkgnd.borderview.setAttribute("bgcolor",$0)
}},"getMenuCapHeight",function(){
with(this){
return menucap.height
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","floatinglist","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,borderview:void 0,height:new LzAlwaysExpr("$m265","$m266"),ignoreplacement:true,name:"bkgnd",shdw:void 0,width:new LzAlwaysExpr("$m263","$m264")},"class":$lzc$class_m286},{attrs:{$classrootdepth:1,$delegates:["oninit","$m285",null],ignoreplacement:true,name:"menucap",width:new LzAlwaysExpr("$m283","$m284"),y:new LzAlwaysExpr("$m281","$m282")},"class":$lzc$class_m289}],$lzc$class_basefloatinglist["children"]),"attributes",new LzInheritedHash($lzc$class_basefloatinglist.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({border_bottom:new LzAlwaysExpr("$m257","$m258"),border_top:new LzAlwaysExpr("$m259","$m260"),shadowcrn:0,shadowoffsety:new LzAlwaysExpr("$m261","$m262")},$lzc$class_floatinglist.attributes)
}}})($lzc$class_floatinglist);Class.make("$lzc$class_menutrackgroup",$lzc$class_basetrackgroup,["addSubview",function($0){
with(this){
if(!($0 instanceof lz.menu)){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menutrackgroup","attributes",new LzInheritedHash($lzc$class_basetrackgroup.attributes)]);Class.make("$lzc$class_m306",LzView,["$m292",function($0){
with(this){
var $1=parent.width-1;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m293",function(){
with(this){
return [parent,"width"]
}},"$m294",function($0){
with(this){
var $1=parent.height-1;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m295",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m307",LzView,["$m296",function($0){
with(this){
var $1=parent.width-3;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m297",function(){
with(this){
return [parent,"width"]
}},"$m298",function($0){
with(this){
var $1=parent.height-3;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m299",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m308",LzView,["$m300",function($0){
with(this){
var $1=parent.width-4;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m301",function(){
with(this){
return [parent,"width"]
}},"$m302",function($0){
with(this){
var $1=parent.height-4;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m303",function(){
with(this){
return [parent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m309",$lzc$class_menutrackgroup,["$m304",function($0){
with(this){
var $1=immediateparent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m305",function(){
with(this){
return [immediateparent,"height"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_menutrackgroup.attributes)]);Class.make("$lzc$class_menubar",$lzc$class_basecomponent,["$m290",function($0){
with(this){
var $1=immediateparent.width+1;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m291",function(){
with(this){
return [immediateparent,"width"]
}},"_clickcounter",void 0,"_openedmenu",void 0,"_outerbezel",void 0,"_innerbezel",void 0,"_face",void 0,"mbarcontent",void 0,"openMenu",function($0,$1){
if($0==this._openedmenu){
if($1){
return
}else{
this._openedmenu._menubutton.showUp();this._openedmenu.setOpen(false);this._openedmenu=0;this.mbarcontent.deactivateTrackgroup(null);this._clickcounter=0;this._openedmenu=0
}}else{
if(this._openedmenu){
this._openedmenu._menubutton.showUp();this._openedmenu.close()
};if($0!=null){
$0.setOpen($1);if($1)this._openedmenu=$0
}else this._openedmenu=0
}},"_applystyle",function($0){
with(this){
if(this.style!=null){
setTint(_outerbezel,$0.basecolor);setTint(_innerbezel,$0.basecolor);setTint(_face,$0.basecolor)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menubar","children",[{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m294","$m295"),name:"_outerbezel",resource:"lzbutton_bezel_outer_rsc",stretches:"both",width:new LzAlwaysExpr("$m292","$m293")},"class":$lzc$class_m306},{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m298","$m299"),name:"_innerbezel",resource:"lzbutton_bezel_inner_rsc",stretches:"both",width:new LzAlwaysExpr("$m296","$m297"),x:1,y:1},"class":$lzc$class_m307},{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m302","$m303"),name:"_face",resource:"lzbutton_face_rsc",stretches:"both",width:new LzAlwaysExpr("$m300","$m301"),x:2,y:2},"class":$lzc$class_m308},{attrs:{$classrootdepth:1,deactivateevents:[],height:new LzAlwaysExpr("$m304","$m305"),layout:{axis:"x",spacing:-2},name:"mbarcontent"},"class":$lzc$class_m309},{attrs:"mbarcontent","class":$lzc$class_userClassPlacement}],"attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({_clickcounter:0,_openedmenu:0,focusable:false,height:20,width:new LzAlwaysExpr("$m290","$m291")},$lzc$class_menubar.attributes)
}}})($lzc$class_menubar);Class.make("$lzc$class_menuarrow",LzView,["$m310",function($0){
with(this){
var $1=parent.width-width-5;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m311",function(){
with(this){
return [parent,"width",this,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menuarrow","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({resource:"menuarrow_rsrc",x:new LzAlwaysExpr("$m310","$m311"),y:5},$lzc$class_menuarrow.attributes)
}}})($lzc$class_menuarrow);Class.make("$lzc$class_menubutton",$lzc$class_button,["$m312",function($0){
var $1=this._menuref.text;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}},"$m313",function(){
return [this._menuref,"text"]
},"$m314",function($0){
with(this){
var $1=parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m315",function(){
with(this){
return [parent,"height"]
}},"_menuref",void 0,"registerEventHandlers",function(){},"$m316",function($0){
this.parent._clickcounter+=1;if(this.parent._clickcounter>1)this.parent._clickcounter=0;this.showDown()
},"showDown",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["showDown"]||this.nextMethod(arguments.callee,"showDown")).call(this);if(this._menuref)parent.openMenu(this._menuref,true)
}},"$m317",function($0){
with(this){
if(this.parent._clickcounter==0){
this.showOver();if(this._menuref)parent.openMenu(this._menuref,false)
}}},"$m318",function($0){
if(this.parent._clickcounter){
this.showDown()
}else{
this.showOver()
}},"showout",function($0){
if(!this.parent._clickcounter){
this.showUp()
}},"$m319",function($0){
this.showDown()
},"$m320",function($0){},"toString",function(){
return "MENU BUTTON title ="+this.text
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menubutton","children",LzNode.mergeChildren([],$lzc$class_button["children"]),"attributes",new LzInheritedHash($lzc$class_button.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousedown","$m316",null,"onmousetrackup","$m317",null,"onmouseover","$m318",null,"onmouseout","showout",null,"onmousetrackover","$m319",null,"onmousetrackout","$m320",null],_menuref:null,clickable:true,doesenter:false,focusable:false,height:new LzAlwaysExpr("$m314","$m315"),respondtomouseout:false,text:new LzAlwaysExpr("$m312","$m313")},$lzc$class_menubutton.attributes)
}}})($lzc$class_menubutton);Class.make("$lzc$class_m331",LzView,["$m323",function($0){
with(this){
var $1=parent.xinset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m324",function(){
with(this){
return [parent,"xinset"]
}},"$m325",function($0){
with(this){
var $1=parent.width-2*parent.xinset;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m326",function(){
with(this){
return [parent,"width",parent,"xinset"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m332",LzView,["$m327",function($0){
with(this){
var $1=parent.xinset;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m328",function(){
with(this){
return [parent,"xinset"]
}},"$m329",function($0){
with(this){
var $1=parent.width-2*parent.xinset;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m330",function(){
with(this){
return [parent,"width",parent,"xinset"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_menuseparator",$lzc$class_basecomponent,["$m321",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m322",function(){
with(this){
return [immediateparent,"width"]
}},"xinset",void 0,"_applystyle",function($0){
if(this.style!=null){
this.setAttribute("bgcolor",$0.textfieldcolor)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menuseparator","children",[{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0x0"),height:1,opacity:0.5,width:new LzAlwaysExpr("$m325","$m326"),x:new LzAlwaysExpr("$m323","$m324"),y:3},"class":$lzc$class_m331},{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0xffffff"),height:1,width:new LzAlwaysExpr("$m329","$m330"),x:new LzAlwaysExpr("$m327","$m328"),y:4},"class":$lzc$class_m332}],"attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({focusable:false,height:6,opacity:1,width:new LzAlwaysExpr("$m321","$m322"),xinset:3},$lzc$class_menuseparator.attributes)
}}})($lzc$class_menuseparator);Class.make("$lzc$class_m343",LzText,["$m334",function($0){
with(this){
var $1=parent.width-90;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m335",function(){
with(this){
return [parent,"width"]
}},"$m336",function($0){
with(this){
var $1=classroot.text_y;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m337",function(){
with(this){
return [classroot,"text_y"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_menuitem",$lzc$class_textlistitem,["delaytime",void 0,"$m333",function($0){
this.setAttribute("command",null)
},"command",void 0,"$lzc$set_command",function($0){
this.setCommand($0)
},"_doshowsubmenu",void 0,"_submenu",void 0,"showsubmenu_del",void 0,"hidesubmenu_del",void 0,"commandstate",void 0,"init",function(){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);if(this._submenu)this._submenu.flist.setAttribute("attachtarget",this)
},"$m338",function($0){
with(this){
if(_submenu)this._opensubmenu(true)
}},"$m339",function($0){
with(this){
if(this.getMouse("x")<parent.width-6){
if(_submenu)this._opensubmenu(false)
}}},"$m340",function($0){
with(this){
if(_submenu)this._opensubmenu(true)
}},"$m341",function($0){},"$m342",function($0){
with(this){
if(!_submenu&&this.enabled){
parent.owner.open(false);if(command)command.onselect.sendEvent();if(this.onselect)this.onselect.sendEvent()
}}},"setHilite",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["setHilite"]||this.nextMethod(arguments.callee,"setHilite")).call(this,$0);if(!_initcomplete)return;if($0){
if(_submenu)this._menuarrow.setAttribute("fgcolor",this.style.texthilitecolor)
}else{
if(_submenu){
this._menuarrow.setAttribute("fgcolor",0);this._opensubmenu(false)
}}}},"showSubmenu",function($0){
with(this){
if(_doshowsubmenu)this._submenu.setOpen(true)
}},"hideSubmenu",function($0){
with(this){
if(!_doshowsubmenu)this._submenu.setOpen(false)
}},"_opensubmenu",function($0){
with(this){
if($0){
this._doshowsubmenu=true;if(!this.showsubmenu_del)this.showsubmenu_del=new LzDelegate(this,"showSubmenu");lz.Timer.addTimer(this.showsubmenu_del,delaytime)
}else{
this._doshowsubmenu=false;if(!this.hidesubmenu_del)this.hidesubmenu_del=new LzDelegate(this,"hideSubmenu");lz.Timer.addTimer(this.hidesubmenu_del,delaytime)
}}},"setSelected",function($0){},"setCommand",function($0){
with(this){
this.command=$0;if(this.command){
this.commandstate.apply();var $1=command.keysToString();this.cmdkeys.setAttribute("text",$1)
}}},"toString",function(){
return "menuitem text ="+this.text
},"_showEnabled",function(){
with(this){
if(_initcomplete){
_applystyle(this.style)
}}},"_applystyle",function($0){
with(this){
if(this.style!=null){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["_applystyle"]||this.nextMethod(arguments.callee,"_applystyle")).call(this,$0);if(command){
if(_enabled){
if(hilited){
this.cmdkeys.setAttribute("fgcolor",style.texthilitecolor)
}else if(selected){
this.cmdkeys.setAttribute("fgcolor",style.textselectedcolor)
}else{
this.cmdkeys.setAttribute("fgcolor",style.textcolor)
}}else{
this.cmdkeys.setAttribute("fgcolor",style.textdisabledcolor)
}}}}},"cmdkeys",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menuitem","children",LzNode.mergeChildren([{attrs:{$classrootdepth:1,applied:false,cmdkeys:void 0,name:"commandstate"},children:[{attrs:{$classrootdepth:1,name:"cmdkeys",width:90,x:new LzAlwaysExpr("$m334","$m335"),y:new LzAlwaysExpr("$m336","$m337")},"class":$lzc$class_m343}],"class":LzState}],$lzc$class_textlistitem["children"]),"attributes",new LzInheritedHash($lzc$class_textlistitem.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmouseover","$m338",null,"onmouseout","$m339",null,"onmousetrackover","$m340",null,"onmousetrackout","$m341",null,"onmousetrackup","$m342",null],_doshowsubmenu:false,_submenu:null,clickable:true,command:new LzOnceExpr("$m333"),delaytime:300,focusable:false,hidesubmenu_del:null,onselect:LzDeclaredEvent,showsubmenu_del:null},$lzc$class_menuitem.attributes)
}}})($lzc$class_menuitem);Class.make("$lzc$class_menufloatinglist",$lzc$class_floatinglist,["issubmenu",void 0,"_updateifsubmenu",void 0,"setHilite",function($0){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["setHilite"]||this.nextMethod(arguments.callee,"setHilite")).call(this,$0);if($0){
if(issubmenu){
if(_updateifsubmenu){
owner.parent._doshowsubmenu=true;owner.parent.parent.setHilite(owner.parent);this._updateifsubmenu=false
}}}else{
if(_hiliteview){
this._hiliteview.setHilite(false);this._hiliteview=null
}}}},"$m344",function(){
var $0=this.interior;return $0
},"$m345",function($0){
this.setHilite(null)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menufloatinglist","children",LzNode.mergeChildren([],$lzc$class_floatinglist["children"]),"attributes",new LzInheritedHash($lzc$class_floatinglist.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmousetrackout","$m345","$m344"],_updateifsubmenu:true,autoscrollbar:false,bgcolor:LzColorUtils.convertColor("0x445566"),focusable:false,issubmenu:false,visible:false},$lzc$class_menufloatinglist.attributes)
}}})($lzc$class_menufloatinglist);Class.make("$lzc$class_menu",$lzc$class_basecomponent,["opened",void 0,"$lzc$set_opened",function($0){
this.setOpen($0)
},"onopened",void 0,"menu_ref",void 0,"flist",void 0,"attach",void 0,"$m346",function($0){
if(this.flist!=null)this.flist.setAttribute("attach",this.attach)
},"_menuitems",void 0,"$lzc$set_width",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzc$set_width"]||this.nextMethod(arguments.callee,"$lzc$set_width")).call(this,$0);if(this.flist!=null){
this.flist.setAttribute("width",$0)
}},"init",function(){
with(this){
var $0=width;if(this.parent instanceof lz.menubar){
this.flist=new (lz.menufloatinglist)(this,{attach:"bottom",attachoffset:-2,width:$0,shadowcrn:1},this._menuitems)
}else if(this.parent instanceof lz.menuitem){
this.flist=new (lz.menufloatinglist)(this,{attach:"right",width:$0,issubmenu:true,shadowcrn:2},this._menuitems)
}else{
var $1=this.attach;this.flist=new (lz.menufloatinglist)(this,{attach:$1,width:$0,shadowcrn:1},this._menuitems)
};this._flisttarget=this.parent;if(parent instanceof lz.menubar){
this._menubutton=new (lz.menubutton)(parent,{_menuref:this},null,false);this._flisttarget=this._menubutton
}else if(parent instanceof lz.menuitem){
new (lz.menuarrow)(this._flisttarget,{name:"_menuarrow"});this.parent._submenu=this
};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this.flist.setAttachTarget(this._flisttarget)
}},"createChildren",function($0){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["createChildren"]||this.nextMethod(arguments.callee,"createChildren")).call(this,[]);this._menuitems=$0
},"childOfMenuHierarchy",function($0){
with(this){
var $1=$0.childOf(this)||$0.childOf(flist);if($1)return true;if($0 instanceof lz.menuitem){
var $2=$0.parent.owner;while($2 instanceof lz.menu){
$1=this==$2;if($1)break;if($2.parent.parent["owner"]==null)break;$2=$2.parent.parent.owner
}};return $1
}},"passModeEvent",function($0,$1){
with(this){
if($0=="onmousedown"){
if($1!=null){
var $2=this.childOfMenuHierarchy($1)||$1.childOf(parent);if(!$2){
this.open(false)
}}else{
this.open(false)
}};return true
}},"getTopMenu",function(){
with(this){
var $0=this;var $1=$0;while($1!==canvas){
if($1 instanceof lz.menu){
$0=$1
};if($1["owner"]){
$1=$1.owner
}else $1=$1.parent
};return $0
}},"open",function($0){
with(this){
var $1=this;var $2=$1;while($2!=canvas){
if($2 instanceof lz.menu){
$1=$2;$1.setOpen($0)
}else if($2 instanceof lz.menubar){
$2.openMenu($1,$0)
};if($2["owner"]){
$2=$2.owner
}else $2=$2.parent
}}},"close",function(){
with(this){
var $0;for(var $1=0;$1<this.flist.interior.content.subviews.length;$1++){
$0=this.flist.interior.content.subviews[$1];if($0 instanceof lz.menuitem){
if($0._submenu)$0._submenu.close()
}};this.setOpen(false)
}},"setOpen",function($0){
with(this){
if(_initcomplete){
if(this.opened==$0)return;this.opened=$0;if($0){
lz.ModeManager.makeModal(this);this.flist.bringToFront();this.flist.setHilite(null);this.flist.setAttribute("tracking",true);this.flist._updateifsubmenu=true;this.flist.setAttribute("visible",true)
}else{
lz.ModeManager.release(this);this.flist.setHilite(null);this.flist.setAttribute("tracking",false);this.flist.setAttribute("visible",false)
};if(onopened)this.onopened.sendEvent(this.opened)
}else this.opened=$0
}},"toggle",function(){
this.setOpen(!this.opened)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","menu","attributes",new LzInheritedHash($lzc$class_basecomponent.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onattach","$m346",null],_menuitems:null,attach:"bottom",flist:null,focusable:false,menu_ref:false,onopened:LzDeclaredEvent,opened:false,text:"menutitle",width:150},$lzc$class_menu.attributes)
}}})($lzc$class_menu);Class.make("$lzc$class_wrappinglayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"spacing",void 0,"xinset",void 0,"yinset",void 0,"$m347",function($0){
var $1=this.spacing;if($1!==this["xspacing"]||!this.inited){
this.setAttribute("xspacing",$1)
}},"$m348",function(){
return [this,"spacing"]
},"xspacing",void 0,"$m349",function($0){
var $1=this.spacing;if($1!==this["yspacing"]||!this.inited){
this.setAttribute("yspacing",$1)
}},"$m350",function(){
return [this,"spacing"]
},"yspacing",void 0,"duration",void 0,"setAxis",function($0){
this.axis=$0;this.otherAxis=$0=="x"?"y":"x";this.sizeAxis=$0=="x"?"width":"height";this.otherSizeAxis=$0=="x"?"height":"width"
},"addSubview",function($0){
this.updateDelegate.register($0,"onwidth");this.updateDelegate.register($0,"onheight");this.updateDelegate.register($0,"onvisible");this.updateDelegate.register(this.immediateparent,"onwidth");(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0);this.update()
},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;var $1=this.subviews;var $2=$1.length;var $3=[];for(var $4=0;$4<$2;$4++){
var $5=this.subviews[$4];if($5.visible){
$3.push($5)
}};var $6=this.immediateparent;if($6.usegetbounds){
$6=$6.getBounds()
};var $7=$6[this.sizeAxis];var $8=this[this.axis+"inset"];var $9=this[this.otherAxis+"inset"];var $a=0;var $b=this[this.axis+"spacing"];var $c=this[this.otherAxis+"spacing"];var $2=$3.length;for(var $4=0;$4<$2;$4++){
var $5=$3[$4];$5.animate(this.axis,$8,this.duration,false);$5.animate(this.otherAxis,$9,this.duration,false);if($4==$2-1)break;if($5.usegetbounds){
$5=$5.getBounds()
};$8+=$5[this.sizeAxis]+$b;$a=Math.max($a,$5[this.otherSizeAxis]);var $d=$1[$4+1];if($d.usegetbounds){
$d=$d.getBounds()
};if($8+$d[this.sizeAxis]>$7){
$8=this[this.axis+"inset"];$9+=$a+$c;$a=0
}}}},"toString",function(){
return "wrappinglayout for "+this.immediateparent
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","wrappinglayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({axis:"x",duration:0,spacing:1,xinset:0,xspacing:new LzAlwaysExpr("$m347","$m348"),yinset:0,yspacing:new LzAlwaysExpr("$m349","$m350")},$lzc$class_wrappinglayout.attributes)
}}})($lzc$class_wrappinglayout);Class.make("$lzc$class_dragstate",LzState,["drag_axis",void 0,"drag_min_x",void 0,"drag_max_x",void 0,"drag_min_y",void 0,"drag_max_y",void 0,"$m351",void 0,"__dragstate_ydoffset",void 0,"$m352",void 0,"$m353",void 0,"y",void 0,"$m354",void 0,"__dragstate_xdoffset",void 0,"$m355",void 0,"$m356",void 0,"x",void 0,"__dragstate_getnewpos",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","dragstate","attributes",new LzInheritedHash(LzState.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$m351:function($0){
this.setAttribute("__dragstate_ydoffset",this.yoffset-this.getMouse("y"))
},$m352:function($0){
var $1=this.drag_axis=="both"||this.drag_axis=="y"?this.__dragstate_getnewpos("y",this.immediateparent.getMouse("y")+this.__dragstate_ydoffset):this.y;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}},$m353:function(){
return [this,"drag_axis",this,"__dragstate_ydoffset",this,"y"].concat(this["$lzc$__dragstate_getnewpos_dependencies"]?this["$lzc$__dragstate_getnewpos_dependencies"](this,this,"y",this.immediateparent.getMouse("y")+this.__dragstate_ydoffset):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"y"):[])
},$m354:function($0){
this.setAttribute("__dragstate_xdoffset",this.xoffset-this.getMouse("x"))
},$m355:function($0){
var $1=this.drag_axis=="both"||this.drag_axis=="x"?this.__dragstate_getnewpos("x",this.immediateparent.getMouse("x")+this.__dragstate_xdoffset):this.x;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}},$m356:function(){
return [this,"drag_axis",this,"__dragstate_xdoffset",this,"x"].concat(this["$lzc$__dragstate_getnewpos_dependencies"]?this["$lzc$__dragstate_getnewpos_dependencies"](this,this,"x",this.immediateparent.getMouse("x")+this.__dragstate_xdoffset):[]).concat(this.immediateparent["$lzc$getMouse_dependencies"]?this.immediateparent["$lzc$getMouse_dependencies"](this,this.immediateparent,"x"):[])
},__dragstate_getnewpos:function($0,$1){
var $2=this["drag_min_"+$0];var $3=this["drag_max_"+$0];if($2!=null&&$1<$2)$1=$2;if($3!=null&&$1>$3)$1=$3;return $1
},__dragstate_xdoffset:new LzOnceExpr("$m354"),__dragstate_ydoffset:new LzOnceExpr("$m351"),drag_axis:"both",drag_max_x:null,drag_max_y:null,drag_min_x:null,drag_min_y:null,x:new LzAlwaysExpr("$m355","$m356"),y:new LzAlwaysExpr("$m352","$m353")},$lzc$class_dragstate.attributes)
}}})($lzc$class_dragstate);canvas.LzInstantiateView({"class":lz.script,attrs:{script:function(){
LzView.addProxyPolicy(function(){
return false
})
}}},1);Class.make("$lzc$class_sText",LzText,["$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","sText","attributes",new LzInheritedHash(LzText.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({selectable:true},$lzc$class_sText.attributes)
}}})($lzc$class_sText);Class.make("$lzc$class_taglink",LzView,["txt",void 0,"$m357",function($0){
with(this){
if($0==null||$0==undefined)$0="";this.txt.setAttribute("text",$0)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","taglink","children",[{attrs:{$classrootdepth:1,resource:"$LZ1"},"class":LzView},{attrs:{$classrootdepth:1,fgcolor:LzColorUtils.convertColor("0x5c5c5c"),fontsize:10,name:"txt",text:"tag title",width:100,x:15,y:-4},"class":LzText}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["ondata","$m357",null],clickable:true,width:100},$lzc$class_taglink.attributes)
}}})($lzc$class_taglink);Class.make("$lzc$class_buddyicon",LzView,["$m358",function($0){
with(this){
if(!$0)return;setSource(gGlobals.IMAGESRC.concat("/large/").concat(datapath.xpathQuery("@img")))
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","buddyicon","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["ondata","$m358",null],height:100,stretches:"both",width:200},$lzc$class_buddyicon.attributes)
}}})($lzc$class_buddyicon);Class.make("$lzc$class_m366",LzAnimator,["$m361",function($0){
with(this){
this.setAttribute("duration",parent.fadein)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m367",LzView,["$m362",function($0){
var $1=this.parent.width-2;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}},"$m363",function(){
return [this.parent,"width"]
},"$m364",function($0){
var $1=this.parent.height-2;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}},"$m365",function(){
return [this.parent,"height"]
},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_naturalimgview",LzView,["fadein",void 0,"$m359",function(){
with(this){
var $0=interior;return $0
}},"$m360",function($0){
this.setDimensions();this.setAttribute("visible",true);this.anm_opacity.doStart()
},"setDimensions",function(){
with(this){
if(interior["resourceheight"]==null){
return
};var $0=interior.resourceheight;var $1=interior.resourcewidth;if($1==0){
return
};var $2=$0/$1;var $3=this.parent.height;var $4=this.parent.width;if($4==0){
return
};var $5=$3/$4;if($5<$2){
this.setAttribute("height",$3);this.setAttribute("width",Math.round($3/$2))
}else if($5>$2){
this.setAttribute("height",Math.round($4*$2));this.setAttribute("width",$4)
}else{
this.setAttribute("height",$3);this.setAttribute("width",$4)
}}},"anm_opacity",void 0,"interior",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","naturalimgview","children",[{attrs:{$classrootdepth:1,attribute:"opacity",duration:new LzOnceExpr("$m361"),name:"anm_opacity",to:1},"class":$lzc$class_m366},{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m364","$m365"),name:"interior",stretches:"both",width:new LzAlwaysExpr("$m362","$m363"),x:1,y:1},"class":$lzc$class_m367}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onload","$m360","$m359"],bgcolor:LzColorUtils.convertColor("0x5b5b5b"),fadein:500,opacity:0},$lzc$class_naturalimgview.attributes)
}}})($lzc$class_naturalimgview);Class.make("$lzc$class_m401",LzAnimator,["$m369",function($0){
with(this){
this.setAttribute("duration",canvas.anm_multipler*300)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m402",LzAnimator,["$m370",function($0){
with(this){
this.setAttribute("duration",canvas.anm_multipler*300)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m403",LzAnimator,["$m371",function($0){
with(this){
var $1=classroot.width-classroot.info.width;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m372",function(){
with(this){
return [classroot,"width",classroot.info,"width"]
}},"$m373",function($0){
with(this){
this.setAttribute("duration",canvas.anm_multipler*300)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m404",LzAnimator,["$m374",function($0){
with(this){
var $1=classroot.height;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m375",function(){
with(this){
return [classroot,"height"]
}},"$m376",function($0){
with(this){
this.setAttribute("duration",canvas.anm_multipler*300)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m405",$lzc$class_naturalimgview,["$m377",function(){
with(this){
var $0=anm_opacity;return $0
}},"$m378",function($0){
with(this){
var $1=classroot.photosource_m;if($1)this.parent.intparent2.interior.setSource($1)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_naturalimgview["children"]),"attributes",new LzInheritedHash($lzc$class_naturalimgview.attributes)]);Class.make("$lzc$class_m406",$lzc$class_naturalimgview,["$m379",function(){
with(this){
var $0=interior;return $0
}},"$m380",function($0){
with(this){
parent.intparent.setAttribute("x",x);parent.intparent.setAttribute("y",y);parent.intparent.setAttribute("width",width);parent.intparent.setAttribute("height",height)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_naturalimgview["children"]),"attributes",new LzInheritedHash($lzc$class_naturalimgview.attributes)]);Class.make("$lzc$class_m400",LzView,["$m368",function($0){
this.intparent.setDimensions();this.intparent2.setDimensions()
},"reset",function(){
this.setAttribute("visible",true);this.intparent2.setAttribute("opacity",0);this.intparent.setAttribute("opacity",0)
},"posme",void 0,"intparent",void 0,"intparent2",void 0,"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,name:"posme",process:"simultaneous",start:false},children:[{attrs:{$classrootdepth:3,attribute:"x",duration:new LzOnceExpr("$m369"),to:43},"class":$lzc$class_m401},{attrs:{$classrootdepth:3,attribute:"y",duration:new LzOnceExpr("$m370"),to:31},"class":$lzc$class_m402},{attrs:{$classrootdepth:3,attribute:"width",duration:new LzOnceExpr("$m373"),to:new LzAlwaysExpr("$m371","$m372")},"class":$lzc$class_m403},{attrs:{$classrootdepth:3,attribute:"height",duration:new LzOnceExpr("$m376"),to:new LzAlwaysExpr("$m374","$m375")},"class":$lzc$class_m404}],"class":LzAnimatorGroup},{attrs:{$classrootdepth:2,$delegates:["onstart","$m378","$m377"],$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gPhV";gPhV=$0
}else if(gPhV===$0){
gPhV=null;$0.id=null
}},id:"gPhV",name:"intparent"},"class":$lzc$class_m405},{attrs:{$classrootdepth:2,$delegates:["onload","$m380","$m379"],fadein:250,name:"intparent2"},"class":$lzc$class_m406}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m408",LzText,["$m383",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m384",function(){
with(this){
return [parent,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m409",LzText,["$m385",function($0){
with(this){
var $1=parent.pr.x+parent.pr.width;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m386",function(){
with(this){
return [parent.pr,"x",parent.pr,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m410",LzView,["$m387",function($0){
with(this){
var $1=parent.pr.x+parent.pr.width;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m388",function(){
with(this){
return [parent.pr,"x",parent.pr,"width"]
}},"$m389",function($0){
with(this){
var $1=parent.prn.y+parent.prn.height/2;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m390",function(){
with(this){
return [parent.prn,"y",parent.prn,"height"]
}},"$m391",function($0){
with(this){
var $1=parent.prn.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m392",function(){
with(this){
return [parent.prn,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m411",LzText,["$m393",function($0){
with(this){
var $1=parent.pr.x+parent.pr.width;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m394",function(){
with(this){
return [parent.pr,"x",parent.pr,"width"]
}},"$m395",function($0){
with(this){
if(text!=""){
parent.parent.prpr.prn.setAttribute("opacity",0.5);parent.parent.prpr.strokeprn.setAttribute("visible",true)
}else{
parent.parent.prpr.prn.setAttribute("opacity",1);parent.parent.prpr.strokeprn.setAttribute("visible",false)
}}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m412",LzView,["$m396",function($0){
with(this){
var $1=gGlobals.IMAGESRC.concat("/large/").concat(this.datapath.xpathQuery("urls/url/text()"));lz.Browser.loadURL($1,"_blank")
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:4,fgcolor:LzColorUtils.convertColor("0x324fdb"),text:"Vedi Ingrandita"},"class":LzText}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m414",$lzc$class_taglink,["$m399",function($0){
with(this){
var $1="tag::".concat(this.txt.getText());canvas.srch.setAttribute("text",$1);canvas.srch.sendsearch()
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_taglink["children"]),"attributes",new LzInheritedHash($lzc$class_taglink.attributes)]);Class.make("$lzc$class_m413",LzView,["$m397",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m398",function(){
with(this){
return [parent,"width"]
}},"tl",void 0,"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:4,spacing:3},"class":$lzc$class_wrappinglayout},{attrs:{$classrootdepth:4,$delegates:["onclick","$m399",null],clickable:true,datapath:"tag[1-19]/text()",name:"tl"},"class":$lzc$class_m414}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m407",LzView,["$m381",function($0){
with(this){
var $1=classroot.ph.x+classroot.ph.intparent.width+50;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m382",function(){
with(this){
return [classroot.ph,"x",classroot.ph.intparent,"width"]
}},"trademarkInfo",void 0,"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,axis:"y",spacing:7},"class":$lzc$class_simplelayout},{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0x5b5b5b"),datapath:"infods:/rsp/photo/marchio/",height:102,name:"trademarkInfo",width:202},children:[{attrs:{$classrootdepth:3,$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gBud";gBud=$0
}else if(gBud===$0){
gBud=null;$0.id=null
}},datapath:"infods:/rsp/photo/marchio/",id:"gBud",x:1,y:1},"class":$lzc$class_buddyicon}],"class":LzView},{attrs:{$classrootdepth:2,prpr:void 0,txt:void 0,width:290,y:40},children:[{attrs:{$classrootdepth:3,axis:"y",spacing:1},"class":$lzc$class_simplelayout},{attrs:{$classrootdepth:3,datapath:"infods:/rsp/photo/description/",fgcolor:LzColorUtils.convertColor("0x5c5c5c"),fontsize:12,fontstyle:"bold",height:23,text:"Descrizione"},"class":LzText},{attrs:{$classrootdepth:3,fgcolor:LzColorUtils.convertColor("0x5c5c5c"),prodId:void 0},children:[{attrs:{$classrootdepth:4,axis:"x"},"class":$lzc$class_simplelayout},{attrs:{$classrootdepth:4,text:"Articolo"},"class":LzText},{attrs:{$classrootdepth:4,datapath:"infods:/rsp/photo/@id",name:"prodId"},"class":LzText},{attrs:{$classrootdepth:4,text:":"},"class":LzText}],"class":LzView},{attrs:{$classrootdepth:3,datapath:"infods:/rsp/photo/description/text()",fgcolor:LzColorUtils.convertColor("0x5c5c5c"),multiline:true,name:"txt",width:new LzAlwaysExpr("$m383","$m384")},"class":$lzc$class_m408},{attrs:{$classrootdepth:3,datapath:"infods:/rsp/photo/price/",name:"prpr",pr:void 0,prn:void 0,strokeprn:void 0},children:[{attrs:{$classrootdepth:4,fgcolor:LzColorUtils.convertColor("0x5c5c5c"),fontstyle:"bold",name:"pr",text:"Prezzo \u20AC"},"class":LzText},{attrs:{$classrootdepth:4,datapath:"text()",fgcolor:LzColorUtils.convertColor("0x5c5c5c"),name:"prn",x:new LzAlwaysExpr("$m385","$m386")},"class":$lzc$class_m409},{attrs:{$classrootdepth:4,bgcolor:LzColorUtils.convertColor("0x0"),height:1,name:"strokeprn",visible:false,width:new LzAlwaysExpr("$m391","$m392"),x:new LzAlwaysExpr("$m387","$m388"),y:new LzAlwaysExpr("$m389","$m390")},"class":$lzc$class_m410}],"class":LzView},{attrs:{$classrootdepth:3,datapath:"infods:/rsp/photo/promoprice/",pr:void 0},children:[{attrs:{$classrootdepth:4,fgcolor:LzColorUtils.convertColor("0x5c5c5c"),fontstyle:"bold",name:"pr",text:"In promozione \u20AC"},"class":LzText},{attrs:{$classrootdepth:4,$delegates:["ontext","$m395",null],datapath:"text()",fgcolor:LzColorUtils.convertColor("0x5c5c5c"),x:new LzAlwaysExpr("$m393","$m394")},"class":$lzc$class_m411}],"class":LzView},{attrs:{$classrootdepth:3,height:4},"class":LzView},{attrs:{$classrootdepth:3,$delegates:["onclick","$m396",null],clickable:true,datapath:"infods:/rsp/photo"},"class":$lzc$class_m412},{attrs:{$classrootdepth:3,height:7},"class":LzView},{attrs:{$classrootdepth:3,datapath:"infods:/rsp/photo/tags/",fgcolor:LzColorUtils.convertColor("0x5c5c5c"),fontsize:12,fontstyle:"bold",height:23,text:"Tags"},"class":LzText},{attrs:{$classrootdepth:3,datapath:"infods:/rsp/photo/tags/",tl:void 0,width:new LzAlwaysExpr("$m397","$m398"),x:5},"class":$lzc$class_m413}],"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_detailsview",LzView,["photosource_t",void 0,"photosource_m",void 0,"setImage",function($0,$1){
this.setAttribute("photosource_t",$0);this.setAttribute("photosource_m",$1);this.ph.reset();this.ph.intparent.interior.setSource($0)
},"loadDetails",function($0){
with(this){
var $1=[{argname:"productId",argvalue:$0}];gDataMan.doDSXRequest(canvas.infods,$1)
}},"title",void 0,"ph",void 0,"info",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","detailsview","children",[{attrs:{$classrootdepth:1,datapath:"infods:/rsp/photo/title/text()",fgcolor:LzColorUtils.convertColor("0x191c43"),fontsize:14,fontstyle:"bold",name:"title",width:500,x:-300,y:1},"class":LzText},{attrs:{$classrootdepth:1,$delegates:["onheight","$m368",null],intparent:void 0,intparent2:void 0,name:"ph",posme:void 0},"class":$lzc$class_m400},{attrs:{$classrootdepth:1,clip:true,height:400,name:"info",trademarkInfo:void 0,width:300,x:new LzAlwaysExpr("$m381","$m382"),y:31},"class":$lzc$class_m407}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({photosource_m:"",photosource_t:"",visible:false},$lzc$class_detailsview.attributes)
}}})($lzc$class_detailsview);Class.make("$lzc$class_albumlayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"spacing",void 0,"xinset",void 0,"yinset",void 0,"$m415",function($0){
var $1=this.spacing;if($1!==this["xspacing"]||!this.inited){
this.setAttribute("xspacing",$1)
}},"$m416",function(){
return [this,"spacing"]
},"xspacing",void 0,"$m417",function($0){
var $1=this.spacing;if($1!==this["yspacing"]||!this.inited){
this.setAttribute("yspacing",$1)
}},"$m418",function(){
return [this,"spacing"]
},"yspacing",void 0,"photoscale",void 0,"photodimension",void 0,"skew",void 0,"skewindex",void 0,"isgrid",void 0,"duration",void 0,"setAxis",function($0){
this.axis=$0;this.otherAxis=$0=="x"?"y":"x";this.sizeAxis=$0=="x"?"width":"height";this.otherSizeAxis=$0=="x"?"height":"width"
},"addSubview",function($0){
this.updateDelegate.register($0,"onwidth");this.updateDelegate.register($0,"onheight");this.updateDelegate.register(this.immediateparent,"onwidth");this.updateDelegate.register(this,"onphotoscale");this.updateDelegate.register(this,"onyspacing");this.updateDelegate.register(this,"onxspacing");this.updateDelegate.register(this,"onskew");(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["addSubview"]||this.nextMethod(arguments.callee,"addSubview")).call(this,$0);this.update()
},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;this.locked=true;var $1=this.subviews.length;var $2=this.immediateparent[this.sizeAxis];var $3=0;var $4=($3-skewindex)*skew*$2;var $5=$4+$2;var $6=this[this.axis+"inset"]+$4;var $7=this[this.otherAxis+"inset"];var $8=0;var $9=this[this.axis+"spacing"];var $a=this[this.otherAxis+"spacing"];for(var $b=0;$b<$1;$b++){
var $c=this.subviews[$b];$c.setAttribute(this.axis,$6);$c.setAttribute(this.otherAxis,$7);$c.setAttribute("width",Math.round(photodimension*photoscale));$c.setAttribute("height",Math.round(photodimension*photoscale));$6+=$c[this.sizeAxis];if($b<$1-1){
$6+=$9;$8=Math.max($8,$c[this.otherSizeAxis]);if($6>$5||$6+this.subviews[$b+1][this.sizeAxis]>$5){
$3+=1;$4=($3-skewindex)*skew*$2;$5=$4+$2;$6=this[this.axis+"inset"]+($3-skewindex)*skew*$2;$7+=$8+$a;$8=0
}}};this.locked=false
}},"toString",function(){
return "wrappinglayout for "+this.immediateparent
},"reset",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.photoscale=1;this.photodimension=70;this.skew=0;this.skewindex=1;this.spacing=52;this.xspacing=this.spacing;this.yspacing=this.spacing;this.yinset=35
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","albumlayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({axis:"x",duration:0,isgrid:true,photodimension:70,photoscale:1,skew:0,skewindex:1,spacing:1,xinset:0,xspacing:new LzAlwaysExpr("$m415","$m416"),yinset:0,yspacing:new LzAlwaysExpr("$m417","$m418")},$lzc$class_albumlayout.attributes)
}}})($lzc$class_albumlayout);Class.make("$lzc$class_pivotlayout",LzLayout,["axis",void 0,"$lzc$set_axis",function($0){
this.setAxis($0)
},"spacing",void 0,"xinset",void 0,"yinset",void 0,"dimmer",void 0,"$m419",function($0){
var $1=this.spacing;if($1!==this["xspacing"]||!this.inited){
this.setAttribute("xspacing",$1)
}},"$m420",function(){
return [this,"spacing"]
},"xspacing",void 0,"$m421",function($0){
var $1=this.spacing;if($1!==this["yspacing"]||!this.inited){
this.setAttribute("yspacing",$1)
}},"$m422",function(){
return [this,"spacing"]
},"yspacing",void 0,"photoscale",void 0,"photodimension",void 0,"skewindex",void 0,"isgrid",void 0,"textvisible",void 0,"skew",void 0,"pivotrow",void 0,"pivotindex",void 0,"pivot_x",void 0,"pivot_y",void 0,"startpivot_x",void 0,"startpivot_y",void 0,"pagebegin",void 0,"pageend",void 0,"pagesize",void 0,"totalitems",void 0,"currentpage",void 0,"perpage",void 0,"totalpages",void 0,"duration",void 0,"calcpageparams",void 0,"$m423",function($0){
this.regUpdateDelegate()
},"setAxis",function($0){
this.axis=$0;this.otherAxis=$0=="x"?"y":"x";this.sizeAxis=$0=="x"?"width":"height";this.otherSizeAxis=$0=="x"?"height":"width"
},"regUpdateDelegate",function(){
this.updateDelegate.register(this.immediateparent,"onwidth");this.updateDelegate.register(this,"onphotodimension");this.updateDelegate.register(this,"onphotoscale");this.updateDelegate.register(this,"onyspacing");this.updateDelegate.register(this,"onxspacing");this.updateDelegate.register(this,"onpivotindex");this.updateDelegate.register(this,"onskew");this.updateDelegate.register(this,"onpivot_x");this.updateDelegate.register(this,"onpivot_y");this.updateDelegate.register(this,"ondimmer")
},"$m424",function($0){
with(this){
this.setAttribute("coidel",new LzDelegate(this,"update"))
}},"coidel",void 0,"updateOnIdle",function($0){
with(this){
if($0){
this.updateDelegate.unregisterAll();coidel.register(lz.Idle,"onidle")
}else{
this.regUpdateDelegate();coidel.unregisterAll()
}}},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;this.locked=true;var $1=this.immediateparent.width;var $2=Math.round(photodimension*photoscale);var $3=Math.floor($1/($2+this.xspacing));$1=$3*($2+this.xspacing);var $4=this.immediateparent.height;if(this.isgrid){
var $5=Math.floor($4/($2+this.xspacing))
}else var $5=1;this.setAttribute("totalitems",$3*$5);var $6=Math.floor(this.pivotindex/$3);var $7=this.pivot_x-this.pivotindex%$3*($2+this.xspacing);var $8=0;var $9=($8-$6)*this.skew*$1+$7;var $a=$9+$1;var $b=$9;var $c=this.pivot_y-($2+this.yspacing)*$6+this.yinset;var $d=-1;var $e=-1;var $f=this.subviews.length;for(var $g=0;$g<$f;$g++){
var $h=this.subviews[$g];$h.index=$g;if(!$h.intparent)continue;if($g!=pivotindex){
$h.intparent.setAttribute("opacity",dimmer)
};$h.setAttribute("x",$b);$h.setAttribute("y",$c);if($h.txt){
$h.txt.setAttribute("visible",this.textvisible)
};$h.setAttribute("width",$2);$h.setAttribute("height",$2);if(calcpageparams){
var $i=$h.x+$h.width<0;$i=$i||$h.x>this.immediateparent.width;$i=$i||$h.y+$h.height>this.immediateparent.height;if($i){
$h.setAttribute("visible",false);if($e==-1&&$d>-1)$e=$g-1
}else{
$h.setAttribute("visible",true);if($d==-1)$d=$g
}};$b+=$2;if($g<$f-1){
$b+=xspacing;if($b>$a||$b+$2>$a){
$8+=1;$9=($8-$6)*skew*$1+$7;$a=$9+$1;$b=$9;$c+=$2+yspacing
}}};if(calcpageparams){
if($e==-1)$e=$f-1;this.setAttribute("pagebegin",$d);this.setAttribute("pageend",$e);this.setAttribute("pagesize",$e-$d+1)
};this.locked=false
}},"toString",function(){
return "pivotlayout for "+this.immediateparent
},"dim",function($0){
with(this){
var $1=this.subviews.length;for(var $2=0;$2<$1;$2++){
var $3;if($2!=pivotindex){
$3=this.subviews[$2];$3.intparent.setAttribute("opacity",$0)
}}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","pivotlayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m423",null],axis:"x",calcpageparams:true,coidel:new LzOnceExpr("$m424"),currentpage:1,dimmer:1,duration:0,isgrid:true,pagebegin:-1,pageend:-1,pagesize:0,perpage:0,photodimension:70,photoscale:1,pivot_x:0,pivot_y:0,pivotindex:0,pivotrow:0,skew:0,skewindex:1,spacing:1,startpivot_x:50,startpivot_y:50,textvisible:true,totalitems:0,totalpages:0,xinset:0,xspacing:new LzAlwaysExpr("$m419","$m420"),yinset:0,yspacing:new LzAlwaysExpr("$m421","$m422")},$lzc$class_pivotlayout.attributes)
}}})($lzc$class_pivotlayout);Class.make("$lzc$class_m448",LzView,["$m438",function($0){
this.setAttribute("x",this.classroot.border)
},"$m439",function($0){
this.setAttribute("y",this.classroot.border)
},"interior",void 0,"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,bgcolor:LzColorUtils.convertColor("0xd9d9d9"),name:"interior",stretches:"both"},children:[{attrs:{$classrootdepth:3,$delegates:["onmousedown","$m442",null,"onmouseup","$m443",null],$m440:function($0){
var $1=this.classroot.doesdrag;if($1!==this["applied"]||!this.inited){
this.setAttribute("applied",$1)
}},$m441:function(){
return [this.classroot,"doesdrag"]
},$m442:function($0){
this.classroot.onmousedown.sendEvent()
},$m443:function($0){
this.classroot.onmouseup.sendEvent()
},applied:new LzAlwaysExpr("$m440","$m441"),clickable:true},"class":LzState}],"class":LzView}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m449",LzText,["$m444",function($0){
with(this){
var $1=classroot.height+3;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m445",function(){
with(this){
return [classroot,"height"]
}},"$m446",function($0){
this.adjustDimensions()
},"$m447",function($0){
this.adjustDimensions()
},"adjustDimensions",function(){
with(this){
this.setAttribute("width",classroot.width+20);var $0=Math.min(this.getTextWidth(),width);var $1=(classroot.width-$0)/2;this.setAttribute("x",$1)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_photo",LzView,["$m425",function($0){
with(this){
var $1=!tls.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m426",function(){
with(this){
return [tls,"waitforload"]
}},"text",void 0,"clipfactor",void 0,"$lzc$set_clipfactor",function($0){
this.setClipFactor($0)
},"loaded",void 0,"aspect",void 0,"_wmult",void 0,"_hmult",void 0,"_iwmult",void 0,"_ihmult",void 0,"_diwmult",void 0,"_dihmult",void 0,"_mdtime",void 0,"_ddcxp",void 0,"_ddcyp",void 0,"$m427",function($0){
var $1=this._wmult*this.width;if($1!==this["intwidth"]||!this.inited){
this.setAttribute("intwidth",$1)
}},"$m428",function(){
return [this,"_wmult",this,"width"]
},"intwidth",void 0,"$m429",function($0){
var $1=this._hmult*this.height;if($1!==this["intheight"]||!this.inited){
this.setAttribute("intheight",$1)
}},"$m430",function(){
return [this,"_hmult",this,"height"]
},"intheight",void 0,"_lastw",void 0,"_lasth",void 0,"border",void 0,"onplainclick",void 0,"doesdrag",void 0,"draginitiator",void 0,"$m431",function($0){
with(this){
this.setAttribute("ddcdel",new LzDelegate(this,"doDragCheck"))
}},"ddcdel",void 0,"mousedownBeforeDrag",void 0,"$m432",function($0){
if(this["txt"]){
if(this.txt.visible)this.txt.adjustDimensions()
}},"$m433",function($0){
if(this.doesdrag)this.startDragCheck()
},"$m434",function($0){
if(this.draginitiator){
this.stopDrag()
}else{
this.ddcdel.unregisterAll();if(this.onplainclick)this.onplainclick.sendEvent(this)
}},"$m435",function($0){
if(!$0)return;var $1=this.datapath;this.txt.setAttribute("text",$1.xpathQuery("@title"));this.setImage(this.getImageURL("t"))
},"$m436",function(){
with(this){
var $0=intparent.interior;return $0
}},"$m437",function($0){
with(this){
var $1=intparent.interior.resourcewidth;var $2=intparent.interior.resourceheight;if($1>$2){
this.setAttribute("_wmult",1);this.setAttribute("_hmult",$2/$1);this.setAttribute("_iwmult",$1/$2);this.setAttribute("_ihmult",1)
}else{
this.setAttribute("_wmult",$1/$2);this.setAttribute("_hmult",1);this.setAttribute("_iwmult",1);this.setAttribute("_ihmult",$2/$1)
};this.setAttribute("_diwmult",_iwmult-_wmult);this.setAttribute("_dihmult",_ihmult-_hmult);this.updateX(true);this.updateY(true);this.setAttribute("loaded",true);intparent.interior.setAttribute("opacity",0);if(tls.waitforload!=true){
this.intparent.setAttribute("visible",true)
};intparent.interior.animate("opacity",1,200);this.txt.adjustDimensions()
}},"getImageURL",function($0){
with(this){
var $1=gGlobals.IMAGESRC;if($0!=""){
$1=$1.concat("/thumb/")
}else $1=$1.concat("/large/");$1=$1.concat(this.datapath.xpathQuery("@path"));return $1
}},"setImage",function($0){
this.intparent.setAttribute("visible",false);this.intparent.interior.setSource($0)
},"startDragCheck",function(){
with(this){
this._mdtime=LzTimeKernel.getTimer();this.ddcdel.register(lz.Idle,"onidle");this._ddcxp=this.getMouse("x");this._ddcyp=this.getMouse("y")
}},"doDragCheck",function($0){
with(this){
var $1=this.getMouse("x")-this._ddcxp;var $2=this.getMouse("y")-this._ddcyp;if(LzTimeKernel.getTimer()-this._mdtime>mousedownBeforeDrag||5<Math.abs($1)+Math.abs($2)){
this.startDrag($1,$2)
}}},"startDrag",function($0,$1){
this.ddcdel.unregisterAll();this.setAttribute("draginitiator",true)
},"stopDrag",function(){
this.setAttribute("draginitiator",false)
},"setClipFactor",function($0){
this.clipfactor=$0;this.updateX(true);this.updateY(true)
},"updateX",function($0){
with(this){
if(!this.isinited)return;if(height!=width)this.setAttribute("height",width);if(_lastw==width&&$0!=true){
return
};this._lastw=width;var $1=_wmult+clipfactor*_diwmult;var $2=$1*(this.width-2*this.border);var $3=this.width/2-$2/2;var $4=$2+clipfactor*(width-$2-2*border);var $5=this.width/2-$4/2;intparent.setAttribute("x",$5);intparent.setAttribute("width",$4);borderbg.setAttribute("x",$5-this.border);borderbg.setAttribute("width",$4+2*this.border);intparent.interior.setAttribute("x",$3-$5);intparent.interior.setAttribute("width",$2);shadow.setAttribute("x",border+$5);shadow.setAttribute("width",$4+this.border)
}},"updateY",function($0){
with(this){
if(!this.isinited)return;if(width!=height)this.setAttribute("width",height);if(_lasth==height&&$0!=true){
return
};this._lasth=height;var $1=_hmult+clipfactor*_dihmult;var $2=$1*(this.height-2*this.border);var $3=this.height/2-$2/2;var $4=$2+clipfactor*(height-$2-2*border);var $5=this.height/2-$4/2;intparent.setAttribute("y",$5);intparent.setAttribute("height",$4);borderbg.setAttribute("y",$5-this.border);borderbg.setAttribute("height",$4+2*this.border);intparent.interior.setAttribute("height",$2);intparent.interior.setAttribute("y",$3-$5);shadow.setAttribute("y",border+$5);shadow.setAttribute("height",$4+this.border)
}},"shadow",void 0,"borderbg",void 0,"intparent",void 0,"txt",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","photo","children",[{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0x0"),name:"shadow",opacity:0.5},"class":LzView},{attrs:{$classrootdepth:1,bgcolor:LzColorUtils.convertColor("0xffffff"),name:"borderbg"},"class":LzView},{attrs:{$classrootdepth:1,clip:true,interior:void 0,name:"intparent",x:new LzOnceExpr("$m438"),y:new LzOnceExpr("$m439")},"class":$lzc$class_m448},{attrs:{$classrootdepth:1,$delegates:["ontext","$m446",null,"onvisible","$m447",null],fgcolor:LzColorUtils.convertColor("0x1b1191"),fontsize:10,name:"txt",text:"",visible:false,width:100,y:new LzAlwaysExpr("$m444","$m445")},"class":$lzc$class_m449},{attrs:"interior","class":$lzc$class_userClassPlacement}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onwidth","updateX",null,"onheight","updateY",null,"onwidth","$m432",null,"onmousedown","$m433",null,"onmouseup","$m434",null,"ondata","$m435",null,"onload","$m437","$m436"],_dihmult:0,_diwmult:0,_hmult:0,_ihmult:0,_iwmult:0,_lasth:null,_lastw:null,_wmult:0,aspect:1,border:2,clickable:true,clipfactor:0,ddcdel:new LzOnceExpr("$m431"),doesdrag:false,draginitiator:false,height:75,intheight:new LzAlwaysExpr("$m429","$m430"),intwidth:new LzAlwaysExpr("$m427","$m428"),loaded:false,mousedownBeforeDrag:300,onplainclick:null,text:"title",visible:new LzAlwaysExpr("$m425","$m426"),width:75},$lzc$class_photo.attributes)
}}})($lzc$class_photo);Class.make("$lzc$class_clipboardlayout",LzLayout,["$m450",function($0){
with(this){
this.updateDelegate.register(parent,"onheight");this.updateDelegate.register(parent,"onwidth")
}},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(this.locked)return;this.locked=true;parent.update();this.locked=false
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","clipboardlayout","attributes",new LzInheritedHash(LzLayout.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m450",null]},$lzc$class_clipboardlayout.attributes)
}}})($lzc$class_clipboardlayout);Class.make("$lzc$class_photocollection",LzView,["$m451",function($0){
with(this){
this.setAttribute("photoclass",lz.photo)
}},"photoclass",void 0,"$m452",function($0){
this.setAttribute("activephotos",[])
},"activephotos",void 0,"addSelection",function($0){
with(this){
var $1=activephotos.length;for(var $2=0;$2<$0.length;$2++){
var $3=this.getPhoto();$3.sendToBack();activephotos.push($3)
};for(var $2=0;$2<$0.length;$2++){
var $4=$0[$2];activephotos[$1+$2].datapath.setFromPointer($4.datapath)
}}},"$m453",function($0){
this.setAttribute("photopool",[])
},"photopool",void 0,"getPhoto",function(){
with(this){
var $0;if(photopool.length){
$0=photopool.pop()
}else{
$0=new photoclass(this,{visible:false});new (lz.datapath)($0);$0.txt.setAttribute("visible",false)
};showdel.register($0,"onloaded");return $0
}},"$m454",function($0){
with(this){
this.setAttribute("showdel",new LzDelegate(this,"showAfterLoad"))
}},"showdel",void 0,"poolAll",function(){
with(this){
while(activephotos.length){
var $0=activephotos.pop();$0.setAttribute("visible",false);$0.datapath.setPointer(null);photopool.push($0)
};showdel.unregisterAll()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","photocollection","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({activephotos:new LzOnceExpr("$m452"),photoclass:new LzOnceExpr("$m451"),photopool:new LzOnceExpr("$m453"),showdel:new LzOnceExpr("$m454")},$lzc$class_photocollection.attributes)
}}})($lzc$class_photocollection);Class.make("$lzc$class_clipboardphoto",$lzc$class_photo,["$m455",function($0){
with(this){
shadow.setAttribute("visible",false)
}},"$m456",function($0){
with(this){
canvas.details.setImage(this.getImageURL("t"),this.getImageURL(""));if(photoscontainer.lyt.isgrid){
photoscontainer.transitionToDetails(this)
}else{
photoscontainer.showPhotoDetails(true,this)
};photoscontainer.detailphoto=this
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","clipboardphoto","children",LzNode.mergeChildren([],$lzc$class_photo["children"]),"attributes",new LzInheritedHash($lzc$class_photo.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m455",null,"onclick","$m456",null],border:1,clickable:true,clipfactor:1,height:30,width:30},$lzc$class_clipboardphoto.attributes)
}}})($lzc$class_clipboardphoto);Class.make("$lzc$class_m475",LzAnimator,["$m462",function($0){
with(this){
this.setAttribute("to",parent.initialheight)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m476",LzAnimator,["$m463",function($0){
with(this){
this.setAttribute("to",parent.raiseheight)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m477",LzAnimator,["$m464",function($0){
with(this){
this.setAttribute("to",parent.initialheight)
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m478",$lzc$class_photocollection,["$m465",function($0){
with(this){
this.setAttribute("photoclass",lz.clipboardphoto)
}},"$m466",function($0){
with(this){
lz.Track.register(this,"photos")
}},"amtrackedover",void 0,"$m467",function($0){
this.setAttribute("amtrackedover",true)
},"$m468",function($0){
this.setAttribute("amtrackedover",false)
},"showAfterLoad",function($0){
with(this){
for(var $1=activephotos.length-1;$1>=0;$1--){
var $2=activephotos[$1];if(!($2.loaded&&!$2.visible))continue;$2.setAttribute("visible",true)
}}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,axis:"x",spacing:3,xinset:3,yinset:3},"class":$lzc$class_wrappinglayout}],"attributes",new LzInheritedHash($lzc$class_photocollection.attributes)]);Class.make("$lzc$class_mybutton",LzView,["enabled",void 0,"normalResourceNumber",void 0,"overResourceNumber",void 0,"downResourceNumber",void 0,"disabledResourceNumber",void 0,"_msdown",void 0,"_msin",void 0,"_enabled",void 0,"_callShow",function(){
if(this._msdown&&this._msin){
this.showDown()
}else if(this._msin){
this.showOver()
}else{
this.showUp()
}},"init",function(){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["init"]||this.nextMethod(arguments.callee,"init")).call(this);this._callShow()
},"$m479",function($0){
this.setAttribute("_msin",true);this._callShow()
},"$m480",function($0){
this.setAttribute("_msin",false);this._callShow()
},"$m481",function($0){
this.setAttribute("_msdown",true);this._callShow()
},"$m482",function($0){
this.setAttribute("_msdown",false);this._callShow()
},"_showEnabled",function(){
with(this){
showUp()
}},"showDown",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.setAttribute("frame",this.downResourceNumber)
},"showUp",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};if(!_enabled&&this.disabledResourceNumber){
this.setAttribute("frame",this.disabledResourceNumber)
}else{
this.setAttribute("frame",this.normalResourceNumber)
}}},"showOver",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.setAttribute("frame",this.overResourceNumber)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","mybutton","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onmouseover","$m479",null,"onmouseout","$m480",null,"onmousedown","$m481",null,"onmouseup","$m482",null],_enabled:true,_msdown:false,_msin:false,clickable:true,disabledResourceNumber:4,downResourceNumber:3,enabled:true,normalResourceNumber:1,overResourceNumber:2,resource:"mybutton_rsc"},$lzc$class_mybutton.attributes)
}}})($lzc$class_mybutton);Class.make("$lzc$class_m483",$lzc$class_mybutton,["$m469",function($0){
with(this){
classroot.toggle()
}},"$m470",function($0){
with(this){
var $1=!classroot.isopen;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m471",function(){
with(this){
return [classroot,"isopen"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);Class.make("$lzc$class_m484",$lzc$class_mybutton,["$m472",function($0){
with(this){
classroot.toggle()
}},"$m473",function($0){
with(this){
var $1=classroot.isopen;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m474",function(){
with(this){
return [classroot,"isopen"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);Class.make("$lzc$class_clipboard",LzView,["$m457",function($0){
this.update()
},"initialheight",void 0,"raiseheight",void 0,"$m458",function($0){
this.setAttribute("height",this.initialheight)
},"isopen",void 0,"$m459",function($0){
with(this){
this.setAttribute("raisedel",new LzDelegate(this,"setRaise"))
}},"raisedel",void 0,"$m460",function(){
with(this){
var $0=anm_close;return $0
}},"$m461",function($0){
with(this){
scrn.setAttribute("visible",false)
}},"dragStarted",function(){
with(this){
lz.Timer.addTimer(raisedel,250)
}},"dragFinished",function($0){
var $1=this.interior.amtrackedover;if($1){
this.interior.addSelection($0)
};this.interior.setAttribute("amtrackedover",false);this.setRaise(true);return $1
},"setRaise",function($0){
with(this){
if($0!=true){
anm_slightraise.doStart()
}else anm_slightlower.doStart()
}},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};this.tp.setAttribute("x",this.tplft.width);this.tprgt.setAttribute("x",width-this.tprgt.width);this.tp.setAttribute("width",width-this.tplft.width-this.tprgt.width);this.lft.setAttribute("y",this.tplft.height);this.lft.setAttribute("height",this.height-this.tplft.height);this.rgt.setAttribute("x",width-this.rgt.width);this.rgt.setAttribute("y",this.tprgt.height);this.rgt.setAttribute("height",height-this.tprgt.height);this.interior.setAttribute("x",this.lft.width-4);this.interior.setAttribute("y",this.tp.height-4);this.interior.setAttribute("width",width-this.interior.x-this.rgt.width+4);this.interior.setAttribute("height",height-this.interior.y);this.setAttribute("y",canvas.height-height)
}},"toggle",function(){
with(this){
if(isopen){
this.anm_close.doStart()
}else{
anm_open.doStart();scrn.setAttribute("visible",true);bringToFront();gDragged.bringToFront()
};this.setAttribute("isopen",!this.isopen)
}},"anm_open",void 0,"anm_close",void 0,"anm_slightraise",void 0,"anm_slightlower",void 0,"interior",void 0,"tplft",void 0,"tp",void 0,"tprgt",void 0,"lft",void 0,"rgt",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","clipboard","children",[{attrs:{$classrootdepth:1,attribute:"height",duration:"700",name:"anm_open",start:false,to:300},"class":LzAnimator},{attrs:{$classrootdepth:1,attribute:"height",duration:"700",name:"anm_close",start:false,to:new LzOnceExpr("$m462")},"class":$lzc$class_m475},{attrs:{$classrootdepth:1,attribute:"height",duration:"400",name:"anm_slightraise",start:false,to:new LzOnceExpr("$m463")},"class":$lzc$class_m476},{attrs:{$classrootdepth:1,attribute:"height",duration:"400",name:"anm_slightlower",start:false,to:new LzOnceExpr("$m464")},"class":$lzc$class_m477},{attrs:{$classrootdepth:1},"class":$lzc$class_clipboardlayout},{attrs:{$classrootdepth:1,$delegates:["oninit","$m466",null,"onmousetrackover","$m467",null,"onmousetrackout","$m468",null],amtrackedover:false,bgcolor:LzColorUtils.convertColor("0xf0f0f0"),clickable:true,clip:true,name:"interior",photoclass:new LzOnceExpr("$m465")},"class":$lzc$class_m478},{attrs:{$classrootdepth:1,name:"tplft",resource:"$LZ2"},"class":LzView},{attrs:{$classrootdepth:1,name:"tp",resource:"$LZ3",stretches:"width"},"class":LzView},{attrs:{$classrootdepth:1,icon_grow:void 0,icon_shrink:void 0,name:"tprgt",resource:"$LZ4"},children:[{attrs:{$classrootdepth:2,$delegates:["onclick","$m469",null],clickable:true,name:"icon_grow",resource:"icon_plus_rsc",visible:new LzAlwaysExpr("$m470","$m471"),x:11,y:4},"class":$lzc$class_m483},{attrs:{$classrootdepth:2,$delegates:["onclick","$m472",null],clickable:true,name:"icon_shrink",resource:"icon_minus_rsc",visible:new LzAlwaysExpr("$m473","$m474"),x:11,y:4},"class":$lzc$class_m484}],"class":LzView},{attrs:{$classrootdepth:1,name:"lft",resource:"$LZ5",stretches:"height"},"class":LzView},{attrs:{$classrootdepth:1,name:"rgt",resource:"$LZ6",stretches:"height"},"class":LzView},{attrs:{$classrootdepth:1,fgcolor:LzColorUtils.convertColor("0xffffff"),fontsize:11,fontstyle:"bold",opacity:0.5,text:"Clips",x:26,y:19},"class":LzText},{attrs:{$classrootdepth:1,fgcolor:LzColorUtils.convertColor("0x463e9d"),fontsize:11,fontstyle:"bold",text:"Clips",x:25,y:18},"class":LzText},{attrs:"interior","class":$lzc$class_userClassPlacement}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["oninit","$m457",null,"onstop","$m461","$m460"],height:new LzOnceExpr("$m458"),initialheight:45,isopen:false,raisedel:new LzOnceExpr("$m459"),raiseheight:80},$lzc$class_clipboard.attributes)
}}})($lzc$class_clipboard);Class.make("$lzc$class_m507",$lzc$class_mybutton,["$m488",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m489",function(){
with(this){
return [parent,"width"]
}},"$m490",function($0){
with(this){
var $1=parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m491",function(){
with(this){
return [parent,"height"]
}},"$m492",function($0){
with(this){
if(classroot.getText()==""){
alert("Non hai inserito la chiave di ricerca!");return
};photoscontainer.lyt.setAttribute("currentpage",1);tls.resetOnLoad();classroot.doSearch()
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);Class.make("$lzc$class_m509",LzView,["$m498",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m499",function(){
with(this){
return [parent,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m510",LzView,["$m500",function($0){
with(this){
var $1=parent.width-width;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m501",function(){
with(this){
return [parent,"width",this,"width"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m511",LzInputText,["$m502",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m503",function(){
with(this){
return [parent,"width"]
}},"$m504",function($0){
with(this){
var $1=parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m505",function(){
with(this){
return [parent,"height"]
}},"$m506",function($0){
with(this){
if(classroot.getText()==""){
alert("Non hai inserito la chiave di ricerca!");return
};if($0==13){
photoscontainer.lyt.setAttribute("currentpage",1);tls.resetOnLoad();classroot.doSearch()
}}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzInputText.attributes)]);Class.make("$lzc$class_m508",LzView,["$m493",function($0){
with(this){
this.setAttribute("x",parent.title.width)
}},"$m494",function($0){
with(this){
var $1=classroot.width-parent.title.x;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m495",function(){
with(this){
return [classroot,"width",parent.title,"x"]
}},"$m496",function($0){
with(this){
var $1=parent.height-1;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m497",function(){
with(this){
return [parent,"height"]
}},"txt",void 0,"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$classrootdepth:2,resource:"$LZ10",stretches:"width",width:new LzAlwaysExpr("$m498","$m499")},"class":$lzc$class_m509},{attrs:{$classrootdepth:2,resource:"$LZ11",x:new LzAlwaysExpr("$m500","$m501")},"class":$lzc$class_m510},{attrs:{$classrootdepth:2,$delegates:["onkeyup","$m506",null],height:new LzAlwaysExpr("$m504","$m505"),name:"txt",width:new LzAlwaysExpr("$m502","$m503"),y:3},"class":$lzc$class_m511}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_search",LzView,["$m485",function($0){
with(this){
this.setAttribute("sendsearch_del",new LzDelegate(this,"sendsearch"))
}},"sendsearch_del",void 0,"$m486",function($0){
var $1=null;if($1!==this["lastsearchterm"]||!this.inited){
this.setAttribute("lastsearchterm",$1)
}},"$m487",function(){
return []
},"lastsearchterm",void 0,"doSearch",function(){
with(this){
canvas.freshNClean();interior.setAttribute("visible",true);if(!canvas.isopen){
sendsearch_del.unregisterAll();var $0=canvas.setOpen(true);sendsearch_del.register($0,"onstop")
}else{
sendsearch()
}}},"sendsearch",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};sendsearch_del.unregisterAll();var $1=[{argname:"service",argvalue:"search"},{argname:"searchKey",argvalue:getText()}];gDataMan.doDSXRequest(photods,$1);tls.displayPage()
}},"getText",function(){
return this.content.txt.getText()
},"$lzc$set_text",function($0){
this.content.txt.setAttribute("text",$0)
},"title",void 0,"content",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","search","children",[{attrs:{$classrootdepth:1,name:"title",width:88},children:[{attrs:{$classrootdepth:2,axis:"x"},"class":$lzc$class_stableborderlayout},{attrs:{$classrootdepth:2,resource:"$LZ7"},"class":LzView},{attrs:{$classrootdepth:2,resource:"$LZ8",stretches:"width"},"class":LzView},{attrs:{$classrootdepth:2,resource:"$LZ9"},"class":LzView}],"class":LzView},{attrs:{$classrootdepth:1,x:35,y:7},children:[{attrs:{$classrootdepth:2,align:"center",fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:10,fontstyle:"bold",text:"Cerca ",valign:"middle"},"class":LzText}],"class":LzView},{attrs:{$classrootdepth:1,$delegates:["onclick","$m492",null],clickable:true,height:new LzAlwaysExpr("$m490","$m491"),resource:"transparent_rsc",width:new LzAlwaysExpr("$m488","$m489")},"class":$lzc$class_m507},{attrs:{$classrootdepth:1,height:new LzAlwaysExpr("$m496","$m497"),name:"content",txt:void 0,width:new LzAlwaysExpr("$m494","$m495"),x:new LzOnceExpr("$m493")},"class":$lzc$class_m508}],"attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({lastsearchterm:new LzAlwaysExpr("$m486","$m487"),sendsearch_del:new LzOnceExpr("$m485")},$lzc$class_search.attributes)
}}})($lzc$class_search);Class.make("$lzc$class_draggedphotos",$lzc$class_photocollection,["stackoffset",void 0,"sellist",void 0,"startDrag",function($0,$1,$2,$3){
with(this){
this.setAttribute("x",$0.getAttributeRelative("x",this)+$2);this.setAttribute("y",$0.getAttributeRelative("y",this)+$3);if(activephotos.length)this.poolAll();this.sellist=$1.concat();this.addSelection(sellist);ds.setAttribute("applied",true);lz.Track.activate("photos");gClipboard.dragStarted()
}},"stopDrag",function(){
with(this){
ds.setAttribute("applied",false);this.poolAll();lz.Track.deactivate("photos");gClipboard.dragFinished(sellist)
}},"showAfterLoad",function($0){
with(this){
var $1=250;var $2=activephotos.length>1?$1:0;for(var $3=activephotos.length-1;$3>=0;$3--){
var $4=activephotos[$3];if(!($4.loaded&&!$4.visible))continue;var $5=sellist[$3];$4.setAttribute("visible",true);$4.setAttribute("x",$5.getAttributeRelative("x",$4));$4.setAttribute("y",$5.getAttributeRelative("y",$4));$4.setAttribute("clipfactor",0);$4.animate("x",$3*stackoffset,$2);$4.animate("y",$3*stackoffset,$2);$4.animate("clipfactor",1,$1)
}}},"ds",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","draggedphotos","children",[{attrs:{$classrootdepth:1,name:"ds"},"class":$lzc$class_dragstate}],"attributes",new LzInheritedHash($lzc$class_photocollection.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({stackoffset:4},$lzc$class_draggedphotos.attributes)
}}})($lzc$class_draggedphotos);canvas.LzInstantiateView({"class":lz.script,attrs:{script:function(){
req=void 0;actualDs=void 0;xmlFileName=void 0;processReqChange=void 0;loadXMLDoc=void 0;deleteOldXml=void 0;gDebug=void 0;processReqChange=function(){
if(req.readyState==4){
if(req.status==200){
xmlFileName=req.responseText.split("\n").join("").split(" ").join("");actualDs.setAttribute("src",gGlobals.DATAURL.concat(xmlFileName));actualDs.doRequest();actualDs.updateData()
}else{
Debug.write("There was a problem retrieving the XML data:\n".concat(req.statusText),req.status)
}}};loadXMLDoc=function($0,$1){
actualDs=$0;req=new XMLHttpRequest();req.onreadystatechange=processReqChange;req.open("GET",$1,true);req.setRequestHeader("If-Modified-Since","Sat, 1 Jan 2000 00:00:00 GMT");req.send(null)
};deleteOldXml=function($0){
var $1=new XMLHttpRequest();var $2=gGlobals.RESTSRC.concat("?deleteDataId=".concat($0.substr($0.lastIndexOf("/")+1)));$1.open("GET",$2,true);$1.setRequestHeader("If-Modified-Since","Sat, 1 Jan 2000 00:00:00 GMT");$1.send(null)
};gDebug=function($0,$1,$2,$3){
switch(arguments.length){
case 2:
$2="";
case 3:
$3="";

};var $4="";if($2!="")$4=" with: ".concat($2);if($3!="")$4=" say: ".concat($3);Debug.write("called ",$0,"::",$1,$4)
};req=null;actualDs=null;xmlFileName=null
}}},1);Class.make("$lzc$class_m515",LzNode,["HOMEURL",void 0,"$m512",function($0){
with(this){
this.setAttribute("BASEURL",getBaseUrl())
}},"BASEURL",void 0,"$m513",function($0){
with(this){
this.setAttribute("RESTSRC",BASEURL.concat("classes/param.jsp"))
}},"RESTSRC",void 0,"$m514",function($0){
with(this){
this.setAttribute("DATAURL",BASEURL.concat("dati/"))
}},"DATAURL",void 0,"IMAGESRC",void 0,"SHOULDCONNECT",void 0,"getBaseUrl",function(){
with(this){
setAttribute("HOMEURL",window.location.href);var $0=HOMEURL.indexOf("?")+1;var $1=HOMEURL.lastIndexOf("/")+1;if($1<$0){
return HOMEURL.substr(0,$1)
}else{
var $2=HOMEURL.substr(0,$0);return $2.substr(0,$2.lastIndexOf("/")+1)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzNode.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gGlobals";gGlobals=$0
}else if(gGlobals===$0){
gGlobals=null;$0.id=null
}},BASEURL:new LzOnceExpr("$m512"),DATAURL:new LzOnceExpr("$m514"),HOMEURL:void 0,IMAGESRC:"http://www.mobilquattrosud.com/dbinterface/img/",RESTSRC:new LzOnceExpr("$m513"),SHOULDCONNECT:true,id:"gGlobals"},"class":$lzc$class_m515},1);Class.make("$lzc$class_m517",LzNode,["errdel",void 0,"$m516",function($0){
with(this){
this.errdel=new LzDelegate(this,"dsError");errdel.register(photods,"onerror");errdel.register(infods,"onerror");errdel.register(trademarkds,"onerror");errdel.register(catalogds,"onerror");errdel.register(photods,"ontimeout");errdel.register(infods,"ontimeout");errdel.register(trademarkds,"ontimeout");errdel.register(catalogds,"ontimeout")
}},"dsError",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};gDebug(this,"dsError",$0,$0.getErrorString()+" "+$0.src);gError.show()
}},"doDSXRequest",function($0,$1){
with(this){
var $2="?";for(var $3=0;$3<$1.length;$3++){
var $4=$1[$3];$2=$2.concat($4.argname);$2=$2.concat("=");$2=$2.concat($4.argvalue);if($3<$1.length-1)$2=$2.concat("&")
};loadXMLDoc($0,gGlobals.RESTSRC.concat($2))
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzNode.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["oninit","$m516",null],$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gDataMan";gDataMan=$0
}else if(gDataMan===$0){
gDataMan=null;$0.id=null
}},errdel:void 0,id:"gDataMan"},"class":$lzc$class_m517},1);Class.make("$lzc$class_m521",LzDataset,["$m518",function($0){
with(this){
Debug.write("photods: data received");deleteOldXml(this.src)
}},"$m519",function($0){
with(this){
deleteOldXml(this.src)
}},"$m520",function($0){
with(this){
deleteOldXml(this.src)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzDataset.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["ondata","$m518",null,"onerror","$m519",null,"ontimeout","$m520",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
photods=$0
}else if(photods===$0){
photods=null
}},name:"photods",type:"http"},"class":$lzc$class_m521},1);Class.make("$lzc$class_m525",LzDataset,["$m522",function($0){
with(this){
Debug.write("infods: data received");deleteOldXml(this.src)
}},"$m523",function($0){
with(this){
deleteOldXml(this.src)
}},"$m524",function($0){
with(this){
deleteOldXml(this.src)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzDataset.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["ondata","$m522",null,"onerror","$m523",null,"ontimeout","$m524",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
infods=$0
}else if(infods===$0){
infods=null
}},name:"infods",type:"http"},"class":$lzc$class_m525},1);Class.make("$lzc$class_m529",LzDataset,["$m526",function($0){
with(this){
Debug.write("trademarkds:",this.rawdata);deleteOldXml(this.src)
}},"$m527",function($0){
with(this){
deleteOldXml(this.src)
}},"$m528",function($0){
with(this){
deleteOldXml(this.src)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzDataset.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["ondata","$m526",null,"onerror","$m527",null,"ontimeout","$m528",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
trademarkds=$0
}else if(trademarkds===$0){
trademarkds=null
}},name:"trademarkds",type:"http"},"class":$lzc$class_m529},1);Class.make("$lzc$class_m533",LzDataset,["$m530",function($0){
with(this){
Debug.write("catalogds: data received");deleteOldXml(this.src)
}},"$m531",function($0){
with(this){
deleteOldXml(this.src)
}},"$m532",function($0){
with(this){
deleteOldXml(this.src)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzDataset.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["ondata","$m530",null,"onerror","$m531",null,"ontimeout","$m532",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
catalogds=$0
}else if(catalogds===$0){
catalogds=null
}},name:"catalogds",type:"http"},"class":$lzc$class_m533},1);Class.make("$lzc$class_m537",LzDataset,["$m534",function($0){
with(this){
Debug.write("catalogds: data received");deleteOldXml(this.src)
}},"$m535",function($0){
with(this){
deleteOldXml(this.src)
}},"$m536",function($0){
with(this){
deleteOldXml(this.src)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzDataset.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["ondata","$m534",null,"onerror","$m535",null,"ontimeout","$m536",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
statds=$0
}else if(statds===$0){
statds=null
}},name:"statds",type:"http"},"class":$lzc$class_m537},1);Class.make("$lzc$class_spinner",LzView,["$m538",function($0){
this.play()
},"counter",void 0,"$m539",function(){
with(this){
var $0=lz.Idle;return $0
}},"$m540",function($0){
if(!this.visible)return;this.setAttribute("counter",(this.counter+1)%6);if(this.counter==0){
var $1=(this.frame+1)%6;this.setAttribute("frame",$1==0?6:$1)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","spinner","attributes",new LzInheritedHash(LzView.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({$delegates:["onclick","$m538",null,"onidle","$m540","$m539"],clickable:true,counter:1,resource:"spinner_rsc",visible:false},$lzc$class_spinner.attributes)
}}})($lzc$class_spinner);Class.make("$lzc$class_m543",LzText,["$m541",function($0){
with(this){
var $1=classroot.label;if($1!==this["text"]||!this.inited){
this.setAttribute("text",$1)
}}},"$m542",function(){
with(this){
return [classroot,"label"]
}},"$classrootdepth",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_linkbutton",$lzc$class_mybutton,["label",void 0,"buttonText",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["tagname","linkbutton","children",[{attrs:{$classrootdepth:1,fgcolor:LzColorUtils.convertColor("0x324fdb"),name:"buttonText",text:new LzAlwaysExpr("$m541","$m542")},"class":$lzc$class_m543}],"attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);(function($0){
with($0)with($0.prototype){
{
LzNode.mergeAttributes({height:20,label:"linkbutton",resource:"transparent_rsc"},$lzc$class_linkbutton.attributes)
}}})($lzc$class_linkbutton);Class.make("$lzc$class_m624",LzView,["$m548",function($0){
with(this){
var $1=canvas.divider.y;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m549",function(){
with(this){
return [canvas.divider,"y"]
}},"$m550",function($0){
with(this){
var $1=canvas.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m551",function(){
with(this){
return [canvas,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m625",LzView,["$m552",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m553",function(){
with(this){
return [parent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:10,text:"Stato Ricerca: ",x:130,y:68},"class":LzText},{attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gResultsCountLabel";gResultsCountLabel=$0
}else if(gResultsCountLabel===$0){
gResultsCountLabel=null;$0.id=null
}},fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:10,fontstyle:"bold",id:"gResultsCountLabel",width:200,x:208,y:68},"class":LzText}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m626",LzView,["$m554",function($0){
with(this){
var $1=parent.tools.height;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m555",function(){
with(this){
return [parent.tools,"height"]
}},"$m556",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m557",function(){
with(this){
return [parent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m629",LzAnimator,["$m571",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m572",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m630",LzAnimator,["$m573",function($0){
with(this){
var $1=canvas.anm_multipler*450;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m574",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m631",LzAnimator,["$m575",function($0){
with(this){
var $1=canvas.anm_multipler*550;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m576",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m633",LzAnimator,["$m578",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m579",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m634",LzAnimator,["$m580",function($0){
with(this){
var $1=canvas.height-290;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m581",function(){
with(this){
return [canvas,"height"]
}},"$m582",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m583",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m635",LzAnimator,["$m584",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m585",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m636",LzAnimator,["$m586",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m587",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m637",LzAnimator,["$m588",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m589",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m632",LzAnimatorGroup,["$m577",function($0){
with(this){
canvas.details.ph.posme.doStart()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{attribute:"pivot_x",duration:new LzAlwaysExpr("$m578","$m579"),relative:true,to:-40},"class":$lzc$class_m633},{attrs:{attribute:"pivot_y",duration:new LzAlwaysExpr("$m582","$m583"),to:new LzAlwaysExpr("$m580","$m581")},"class":$lzc$class_m634},{attrs:{attribute:"photodimension",duration:new LzAlwaysExpr("$m584","$m585"),to:50},"class":$lzc$class_m635},{attrs:{attribute:"photoscale",duration:new LzAlwaysExpr("$m586","$m587"),to:1},"class":$lzc$class_m636},{attrs:{attribute:"yspacing",duration:new LzAlwaysExpr("$m588","$m589"),to:-50},"class":$lzc$class_m637}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m639",LzAnimator,["$m592",function($0){
with(this){
var $1=canvas.details.title;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m593",function(){
with(this){
return [canvas.details,"title"]
}},"$m594",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m595",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m640",LzAnimator,["$m596",function($0){
with(this){
var $1=canvas.height-80;if($1!==this["to"]||!this.inited){
this.setAttribute("to",$1)
}}},"$m597",function(){
with(this){
return [canvas,"height"]
}},"$m598",function($0){
with(this){
var $1=canvas.divider;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m599",function(){
with(this){
return [canvas,"divider"]
}},"$m600",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m601",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m638",LzAnimatorGroup,["$m590",function($0){
with(this){
var $1=canvas.anm_multipler*350;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m591",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{attribute:"x",duration:new LzAlwaysExpr("$m594","$m595"),target:new LzAlwaysExpr("$m592","$m593"),to:40},"class":$lzc$class_m639},{attrs:{attribute:"y",duration:new LzAlwaysExpr("$m600","$m601"),target:new LzAlwaysExpr("$m598","$m599"),to:new LzAlwaysExpr("$m596","$m597")},"class":$lzc$class_m640}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m642",LzAnimator,["$m606",function($0){
with(this){
this.setAttribute("target",tls)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m643",LzAnimator,["$m607",function($0){
with(this){
var $1=canvas.details.title;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m608",function(){
with(this){
return [canvas.details,"title"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m644",LzAnimator,["$m609",function($0){
with(this){
var $1=canvas.details.info;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m610",function(){
with(this){
return [canvas.details,"info"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m645",LzAnimator,["$m611",function($0){
with(this){
var $1=canvas.divider;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m612",function(){
with(this){
return [canvas,"divider"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m641",LzAnimatorGroup,["$m604",function($0){
with(this){
var $1=canvas.anm_multipler*300;if($1!==this["duration"]||!this.inited){
this.setAttribute("duration",$1)
}}},"$m605",function(){
with(this){
return [canvas,"anm_multipler"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{attribute:"opacity",target:new LzOnceExpr("$m606"),to:1},"class":$lzc$class_m642},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m607","$m608"),to:-300},"class":$lzc$class_m643},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m609","$m610"),to:1500},"class":$lzc$class_m644},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m611","$m612"),to:0},"class":$lzc$class_m645}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m628",$lzc$class_pivotlayout,["$m569",function($0){
with(this){
var $1=canvas.tools.zoomscale;if($1!==this["photoscale"]||!this.inited){
this.setAttribute("photoscale",$1)
}}},"$m570",function(){
with(this){
return [canvas.tools,"zoomscale"]
}},"myreset",function(){
with(this){
if(isgrid){
this.photoscale=canvas.tools.zoomscale;this.photodimension=70;this.spacing=50;this.skew=0;this.pivot_x=50;yinset=50;xinset=0;this.pivot_y=0;this.yspacing=50;this.xspacing=50;this.pivotindex=0
}else{
this.photodimension=50;this.photoscale=1;this.skew=1;this.pivot_y=405;this.yspacing=-50;this.xspacing=10;pivotindex=0
};this.update()
}},"transitiontolinear_anm",void 0,"$m602",function(){
with(this){
var $0=transitiontolinear_anm;return $0
}},"$m603",function($0){
this.animate("dimmer",1,500);this.updateOnIdle(false)
},"transitiontogrid_anm",void 0,"pageNext",void 0,"pagePrev",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{grp:void 0,name:"transitiontolinear_anm",start:false},children:[{attrs:{name:"grp",process:"simultaneous",yspacing:void 0},children:[{attrs:{attribute:"skew",duration:new LzAlwaysExpr("$m571","$m572"),from:0,motion:"easeout",to:1},"class":$lzc$class_m629},{attrs:{attribute:"yspacing",duration:new LzAlwaysExpr("$m573","$m574"),name:"yspacing",to:-70},"class":$lzc$class_m630},{attrs:{attribute:"xspacing",duration:new LzAlwaysExpr("$m575","$m576"),to:11},"class":$lzc$class_m631}],"class":LzAnimatorGroup},{attrs:{$delegates:["onstop","$m577",null],process:"simultaneous"},"class":$lzc$class_m632},{attrs:{duration:new LzAlwaysExpr("$m590","$m591"),process:"simultaneous"},"class":$lzc$class_m638}],"class":LzAnimatorGroup},{attrs:{duration:new LzAlwaysExpr("$m604","$m605"),name:"transitiontogrid_anm",process:"simultaneous",start:false},"class":$lzc$class_m641},{attrs:{attribute:"pivot_x",duration:"1000",name:"pageNext",relative:true,start:false,to:-canvas.width},"class":LzAnimator},{attrs:{attribute:"pivot_x",duration:"1000",name:"pagePrev",relative:true,start:false,to:canvas.width},"class":LzAnimator}],"attributes",new LzInheritedHash($lzc$class_pivotlayout.attributes)]);Class.make("$lzc$class_m646",LzSelectionManager,["isMultiSelect",function($0){
with(this){
return lz.Keys.isKeyDown("control")||lz.Keys.isKeyDown("shift")||parent.isRectangleSelecting
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzSelectionManager.attributes)]);Class.make("$lzc$class_m647",$lzc$class_photo,["selected",void 0,"isselected",void 0,"rerunxpath",void 0,"$m613",function($0){
with(this){
if(parent.selman.isMultiSelect(null)){
parent.selman.select(this)
}else{
if(parent.lyt.isgrid){
canvas.details.setImage(this.getImageURL("t"),this.getImageURL(""));photoscontainer.lyt.setAttribute("dimmer",0.2);parent.transitionToDetails(this);parent.showPhotoDetails(true,this)
}else{
if(photoscontainer.detailphoto==this){
return
}else{
canvas.details.setImage(this.getImageURL("t"),this.getImageURL(""));parent.showPhotoDetails(true,this)
}};photoscontainer.detailphoto=this
}}},"startDrag",function($0,$1){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["startDrag"]||this.nextMethod(arguments.callee,"startDrag")).call(this,$0,$1);if(!isselected)parent.selman.select(this);gDragged.startDrag(this,parent.selman.getSelection(),$0,$1)
}},"stopDrag",function(){
with(this){
(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["stopDrag"]||this.nextMethod(arguments.callee,"stopDrag")).call(this);gDragged.stopDrag();parent.selman.clearSelection()
}},"setSelected",function($0){
this.setAttribute("isselected",$0);this.intparent.setAttribute("opacity",$0?0.5:1)
},"intersectsRectangle",function($0,$1,$2,$3){
with(this){
return x+width>$0&&x<$2&&y+height>$1&&y<$3
}},"$datapath",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_photo["children"]),"attributes",new LzInheritedHash($lzc$class_photo.attributes)]);Class.make("$lzc$class_m648",LzView,["_sx",void 0,"_sy",void 0,"$m614",function($0){
with(this){
this.setAttribute("updel",new LzDelegate(this,"update"))
}},"updel",void 0,"starter",function(){
with(this){
this.setAttribute("visible",true);this._sx=parent.getMouse("x");this._sy=parent.getMouse("y");this.updel.register(lz.Idle,"onidle");this.bringToFront();this.update()
}},"update",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};var $1=this.parent.getMouse("x");var $2=this.parent.getMouse("y");if($1<_sx){
this.setAttribute("x",$1);this.setAttribute("width",this._sx-$1-1)
}else{
this.setAttribute("x",this._sx);this.setAttribute("width",$1-this._sx+1)
};if($2<this._sy){
this.setAttribute("y",$2);this.setAttribute("height",this._sy-$2-1)
}else{
this.setAttribute("y",this._sy);this.setAttribute("height",$2-this._sy+1)
}}},"stopper",function($0,$1){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;

};this.setAttribute("visible",false);this.updel.unregisterAll();this.parent.selectInRectangle(this._sx,this._sy,this.parent.getMouse("x"),this.parent.getMouse("y"))
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m627",LzView,["$m558",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m559",function(){
with(this){
return [parent,"width"]
}},"$m560",function($0){
with(this){
var $1=parent.height-93;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m561",function(){
with(this){
return [parent,"height"]
}},"detailphoto",void 0,"initialDone",void 0,"$m562",function($0){
with(this){
this.setAttribute("doneDel",new LzDelegate(this,"initialReplicationDone"))
}},"doneDel",void 0,"isRectangleSelecting",void 0,"$m563",function($0){
with(this){
if(lyt.isgrid)rubberband.starter()
}},"$m564",function($0){
with(this){
rubberband.stopper()
}},"$m565",function(){
var $0=this.lyt.transitiontogrid_anm;return $0
},"$m566",function($0){
with(this){
canvas.interior.details_bkgnd.setAttribute("height",0);canvas.interior.details_bkgnd.setAttribute("visible",false);this.lyt.isgrid=true;this.lyt.myreset();this.lyt.setAttribute("textvisible",true);this.lyt.update();this.showPhotoDetails(false);tls.enableZoom()
}},"$m567",function(){
with(this){
var $0=photods;return $0
}},"$m568",function($0){
with(this){
this.initialReplicationDone();if(!this.pagecounter_dp.p)return;var $1=this.pagecounter_dp.p.childNodes.length;var $2=this.pagecounter_dp.xpathQuery("@page");var $3=this.pagecounter_dp.xpathQuery("@perpage");var $4=this.pagecounter_dp.xpathQuery("@pages");photoscontainer.lyt.setAttribute("currentpage",$2);photoscontainer.lyt.setAttribute("perpage",$3);photoscontainer.lyt.setAttribute("totalpages",$4);tls.setpPagingParams($1);gResultsCountLabel.setAttribute("text",this.buildResultsString($1));this.watchforlast();photoscontainer.lyt.calcpageparams=true
}},"displaytext",function($0){
with(this){
var $1;for($1 in photoscontainer.lyt.subviews){
if(photoscontainer.lyt.subviews[$1].txt&&photoscontainer.lyt.subviews[$1].txt.setVisible)photoscontainer.lyt.subviews[$1].txt.setAttribute("visible",$0)
}}},"transitionToDetails",function($0){
with(this){
var $1=$0.intparent.interior;canvas.details.ph.setAttribute("x",$1.getAttributeRelative("x",canvas)-1);canvas.details.ph.setAttribute("y",$1.getAttributeRelative("y",canvas)-101);var $2=$1.width>$1.height?$1.width+2:$1.height+2;canvas.details.ph.setAttribute("width",$2);canvas.details.ph.setAttribute("height",$2);canvas.details.setAttribute("visible",true);this.lyt.setAttribute("textvisible",false);this.lyt.setAttribute("isgrid",false);this.lyt.updateOnIdle(true);canvas.interior.details_bkgnd.setAttribute("height",2);canvas.interior.details_bkgnd.setAttribute("visible",true);this.lyt.transitiontolinear_anm.grp.yspacing.setAttribute("to",-photoscontainer.lyt.photodimension*photoscontainer.lyt.photoscale);this.lyt.transitiontolinear_anm.doStart()
}},"transitionToGrid",function(){
this.lyt.transitiontogrid_anm.doStart()
},"showPhotoDetails",function($0,$1){
with(this){
switch(arguments.length){
case 1:
$1=null;

};if($0){
var $2=$1.datapath.p.getAttr("id");canvas.details.setAttribute("visible",true);canvas.details.loadDetails($2);tls.disableZoom()
}else{
canvas.details.setAttribute("visible",false);canvas.details.info.setAttribute("x",canvas.width+10)
}}},"watchforlast",function(){
if(!this.ph["clones"])return;var $0=this.ph.clones[this.ph.clones.length-1];this.doneDel.unregisterAll();if($0!=null){
this.doneDel.register($0,"oninit")
}},"initialReplicationDone",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};photoscontainer.lyt.unlock();photoscontainer.lyt.update();photoscontainer.setAttribute("visible",true);spnr.setAttribute("visible",false)
}},"buildResultsString",function($0){
var $1="Nessun articolo trovato";if($0==1){
$1="Trovato un articolo"
}else if($0>=100){
$1="Trovati piu' di 100 articoli"
}else if($0>=0){
$1=$0+" articoli trovati"
};return $1
},"selectInRectangle",function($0,$1,$2,$3){
with(this){
if(!selman.isMultiSelect(null)){
selman.clearSelection()
};this.setAttribute("isRectangleSelecting",true);if($0>$2){
var $4=$0;$0=$2;$2=$4
};if($1>$3){
var $4=$1;$1=$3;$3=$4
};for(var $5=subviews.length-1;$5>=0;$5--){
var $6=subviews[$5];if(!($6 instanceof lz.photo))continue;if($6.intersectsRectangle($0,$1,$2,$3)){
selman.select($6)
}};this.setAttribute("isRectangleSelecting",false)
}},"pagecounter_dp",void 0,"lyt",void 0,"selman",void 0,"ph",void 0,"rubberband",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{name:"pagecounter_dp",xpath:"photods:/rsp/photos/"},"class":LzDatapointer},{attrs:{$delegates:["onstop","$m603","$m602"],name:"lyt",pageNext:void 0,pagePrev:void 0,photodimension:70,photoscale:new LzAlwaysExpr("$m569","$m570"),pivot_x:50,pivot_y:0,pivotindex:0,skew:0,spacing:50,transitiontogrid_anm:void 0,transitiontolinear_anm:void 0,xinset:0,yinset:50},"class":$lzc$class_m628},{attrs:{name:"selman",toggle:false},"class":$lzc$class_m646},{attrs:{$datapath:{attrs:{xpath:"photods:/rsp/photos/photo[1-18]"},"class":LzDatapath},$delegates:["onplainclick","$m613",null],datapath:LzNode._ignoreAttribute,doesdrag:true,isselected:false,name:"ph",rerunxpath:true,selected:false},"class":$lzc$class_m647},{attrs:{_sx:0,_sy:0,bgcolor:LzColorUtils.convertColor("0xbb"),name:"rubberband",opacity:0.3,options:{ignorelayout:true},updel:new LzOnceExpr("$m614"),visible:false},"class":$lzc$class_m648}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m650",LzView,["$m619",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m620",function(){
with(this){
return [immediateparent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m651",LzView,["$m621",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m622",function(){
with(this){
return [immediateparent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m649",LzView,["$m615",function($0){
with(this){
var $1=immediateparent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m616",function(){
with(this){
return [immediateparent,"width"]
}},"$m617",function($0){
with(this){
var $1=canvas.bottom.y-2;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m618",function(){
with(this){
return [canvas.bottom,"y"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{bgcolor:LzColorUtils.convertColor("0x9993ee"),height:1,width:new LzAlwaysExpr("$m619","$m620")},"class":$lzc$class_m650},{attrs:{bgcolor:LzColorUtils.convertColor("0xffffff"),height:1,width:new LzAlwaysExpr("$m621","$m622"),y:1},"class":$lzc$class_m651}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m623",LzView,["$m544",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m545",function(){
with(this){
return [parent,"width"]
}},"$m546",function($0){
with(this){
var $1=parent.height;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m547",function(){
with(this){
return [parent,"height"]
}},"details_bkgnd",void 0,"tools",void 0,"beveled_divider",void 0,"photos",void 0,"btm",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{height:new LzAlwaysExpr("$m548","$m549"),name:"details_bkgnd",width:new LzAlwaysExpr("$m550","$m551")},"class":$lzc$class_m624},{attrs:{bgcolor:LzColorUtils.convertColor("0xbac0f8"),height:91,name:"tools",width:new LzAlwaysExpr("$m552","$m553")},"class":$lzc$class_m625},{attrs:{name:"beveled_divider",resource:"$LZ12",stretches:"width",width:new LzAlwaysExpr("$m556","$m557"),y:new LzAlwaysExpr("$m554","$m555")},"class":$lzc$class_m626},{attrs:{$delegates:["onmousedown","$m563",null,"onmouseup","$m564",null,"onstop","$m566","$m565","ondata","$m568","$m567"],$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="photoscontainer";photoscontainer=$0
}else if(photoscontainer===$0){
photoscontainer=null;$0.id=null
}},clickable:true,detailphoto:null,doneDel:new LzOnceExpr("$m562"),height:new LzAlwaysExpr("$m560","$m561"),id:"photoscontainer",initialDone:false,isRectangleSelecting:false,lyt:void 0,name:"photos",pagecounter_dp:void 0,ph:void 0,rubberband:void 0,selman:void 0,showhandcursor:false,visible:false,width:new LzAlwaysExpr("$m558","$m559"),y:93},"class":$lzc$class_m627},{attrs:{height:88,name:"btm",width:new LzAlwaysExpr("$m615","$m616"),y:new LzAlwaysExpr("$m617","$m618")},"class":$lzc$class_m649}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
interior=$0
}else if(interior===$0){
interior=null
}},beveled_divider:void 0,btm:void 0,details_bkgnd:void 0,height:new LzAlwaysExpr("$m546","$m547"),name:"interior",photos:void 0,tools:void 0,width:new LzAlwaysExpr("$m544","$m545")},"class":$lzc$class_m623},42);Class.make("$lzc$class_m656",$lzc$class_detailsview,["$m652",function($0){
with(this){
var $1=canvas.height-300;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m653",function(){
with(this){
return [canvas,"height"]
}},"$m654",function($0){
with(this){
var $1=canvas.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m655",function(){
with(this){
return [canvas,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_detailsview["children"]),"attributes",new LzInheritedHash($lzc$class_detailsview.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
details=$0
}else if(details===$0){
details=null
}},height:new LzAlwaysExpr("$m652","$m653"),name:"details",visible:false,width:new LzAlwaysExpr("$m654","$m655"),y:100},"class":$lzc$class_m656},44);Class.make("$lzc$class_m671",LzView,["$m657",function($0){
with(this){
var $1=canvas.width-245;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m658",function(){
with(this){
return [canvas,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m672",LzView,["$m659",function($0){
with(this){
var $1=canvas.width-245;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m660",function(){
with(this){
return [canvas,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m673",LzView,["$m661",function($0){
with(this){
var $1=canvas.width-270;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m662",function(){
with(this){
return [canvas,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m675",LzView,["$m666",function($0){
with(this){
parent.onmouseover.sendEvent()
}},"$m667",function($0){
with(this){
parent.onmouseout.sendEvent()
}},"$m668",function($0){
with(this){
parent.onmousedown.sendEvent()
}},"$m669",function($0){
with(this){
parent.onmouseup.sendEvent()
}},"$m670",function($0){
with(this){
photoscontainer.transitionToGrid()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m674",$lzc$class_mybutton,["$m663",function($0){
with(this){
var $1=canvas.width-249;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m664",function(){
with(this){
return [canvas,"width"]
}},"$m665",function($0){
this.returnToGridView()
},"returnToGridView",function(){
with(this){
photoscontainer.transitionToGrid()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{fgcolor:LzColorUtils.convertColor("0x463e9d"),font:"Verdana,Vera,sans-serif",fontsize:9,fontstyle:"plain",text:"Nascondi dettagli",width:100,x:15,y:-1},"class":LzText},{attrs:{$delegates:["onmouseover","$m666",null,"onmouseout","$m667",null,"onmousedown","$m668",null,"onmouseup","$m669",null,"onclick","$m670",null],clickable:true,height:20,width:120},"class":$lzc$class_m675}],"attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
divider=$0
}else if(divider===$0){
divider=null
}},anmb:void 0,hideDetailsButton:void 0,name:"divider",y:0},children:[{attrs:{attribute:"y",duration:"1000",name:"anmb",start:false,to:0},"class":LzAnimator},{attrs:{bgcolor:LzColorUtils.convertColor("0x9993ee"),height:2,width:new LzAlwaysExpr("$m657","$m658"),y:-1},"class":$lzc$class_m671},{attrs:{bgcolor:LzColorUtils.convertColor("0xffffff"),height:1,width:new LzAlwaysExpr("$m659","$m660"),y:1},"class":$lzc$class_m672},{attrs:{resource:"$LZ13",stretches:"width",width:130,x:new LzAlwaysExpr("$m661","$m662"),y:-4},"class":$lzc$class_m673},{attrs:{$delegates:["onclick","$m665",null],clickable:true,name:"hideDetailsButton",resource:"hidedetails_rsc",width:120,x:new LzAlwaysExpr("$m663","$m664"),y:-4},"class":$lzc$class_m674}],"class":LzView},8);Class.make("$lzc$class_m703",LzAnimator,["$m682",function($0){
with(this){
var $1=gClipboard;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m683",function(){
return [this,"gClipboard"]
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m704",LzAnimator,["$m684",function($0){
with(this){
var $1=canvas.tools;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m685",function(){
with(this){
return [canvas,"tools"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m705",LzAnimator,["$m686",function($0){
with(this){
var $1=canvas.srch;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m687",function(){
with(this){
return [canvas,"srch"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m706",LzAnimator,["$m688",function($0){
with(this){
var $1=canvas.srch;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m689",function(){
with(this){
return [canvas,"srch"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m707",LzAnimator,["$m690",function($0){
with(this){
var $1=canvas.srch;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m691",function(){
with(this){
return [canvas,"srch"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m708",LzAnimator,["$m692",function($0){
with(this){
var $1=gClipboard;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m693",function(){
return [this,"gClipboard"]
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m709",LzAnimator,["$m694",function($0){
with(this){
var $1=canvas.tools;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m695",function(){
with(this){
return [canvas,"tools"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m710",LzAnimator,["$m696",function($0){
with(this){
var $1=canvas.srch;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m697",function(){
with(this){
return [canvas,"srch"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m711",LzAnimator,["$m698",function($0){
with(this){
var $1=canvas.srch;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m699",function(){
with(this){
return [canvas,"srch"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m712",LzAnimator,["$m700",function($0){
with(this){
var $1=canvas.srch;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m701",function(){
with(this){
return [canvas,"srch"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m702",LzView,["$m676",function($0){
with(this){
var $1=Math.max(canvas.height-height,canvas.links.y+canvas.links.height);if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m677",function(){
with(this){
return [canvas,"height",this,"height",canvas.links,"y",canvas.links,"height"].concat(Math["$lzc$max_dependencies"]?Math["$lzc$max_dependencies"](this,Math,canvas.height-height,canvas.links.y+canvas.links.height):[])
}},"$m678",function($0){
with(this){
var $1=canvas.height-this.y;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m679",function(){
with(this){
return [canvas,"height",this,"y"]
}},"$m680",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m681",function(){
with(this){
return [parent,"width"]
}},"anm",void 0,"anmb",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{duration:"1000",name:"anm",process:"simultaneous",start:false},children:[{attrs:{attribute:"height",start:false,to:43},"class":LzAnimator},{attrs:{attribute:"x",start:false,target:new LzAlwaysExpr("$m682","$m683"),to:0},"class":$lzc$class_m703},{attrs:{attribute:"x",start:false,target:new LzAlwaysExpr("$m684","$m685"),to:594},"class":$lzc$class_m704},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m686","$m687"),to:65},"class":$lzc$class_m705},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m688","$m689"),to:568},"class":$lzc$class_m706},{attrs:{attribute:"width",target:new LzAlwaysExpr("$m690","$m691"),to:117},"class":$lzc$class_m707}],"class":LzAnimatorGroup},{attrs:{duration:"1000",name:"anmb",process:"simultaneous",start:false},children:[{attrs:{attribute:"height",start:false,to:43},"class":LzAnimator},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m692","$m693"),to:-800},"class":$lzc$class_m708},{attrs:{attribute:"opacity",target:new LzAlwaysExpr("$m694","$m695"),to:0},"class":$lzc$class_m709},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m696","$m697"),to:315},"class":$lzc$class_m710},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m698","$m699"),to:475},"class":$lzc$class_m711},{attrs:{attribute:"width",target:new LzAlwaysExpr("$m700","$m701"),to:156},"class":$lzc$class_m712}],"class":LzAnimatorGroup}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
bottom=$0
}else if(bottom===$0){
bottom=null
}},anm:void 0,anmb:void 0,bgcolor:LzColorUtils.convertColor("0xf0f0f0"),height:new LzAlwaysExpr("$m678","$m679"),name:"bottom",width:new LzAlwaysExpr("$m680","$m681"),y:new LzAlwaysExpr("$m676","$m677")},"class":$lzc$class_m702},15);Class.make("$lzc$class_m727",$lzc$class_menuitem,["itemId",void 0,"$m716",function($0){
this.setAttribute("text",this.datapath.xpathQuery("@nome"));this.itemId=this.datapath.xpathQuery("@id")
},"$m717",function($0){
with(this){
canvas.selectItem(itemId)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_menuitem["children"]),"attributes",new LzInheritedHash($lzc$class_menuitem.attributes)]);Class.make("$lzc$class_m726",$lzc$class_linkbutton,["$m715",function($0){
this.catalogMenu.setOpen(true)
},"catalogMenu",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([{attrs:{attach:"right",name:"catalogMenu"},children:[{attrs:{$delegates:["ondata","$m716",null,"onclick","$m717",null],clickable:true,datapath:"catalogds:/rsp/list/item[@tipocatalogo='d']",itemId:"0"},"class":$lzc$class_m727}],"class":$lzc$class_menu}],$lzc$class_linkbutton["children"]),"attributes",new LzInheritedHash($lzc$class_linkbutton.attributes)]);Class.make("$lzc$class_m729",$lzc$class_menuitem,["itemId",void 0,"$m719",function($0){
with(this){
parent.parent.setAttribute("visible",true);setAttribute("text",datapath.xpathQuery("@nome"));itemId=datapath.xpathQuery("@id")
}},"$m720",function($0){
with(this){
canvas.selectItem(itemId)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_menuitem["children"]),"attributes",new LzInheritedHash($lzc$class_menuitem.attributes)]);Class.make("$lzc$class_m728",$lzc$class_linkbutton,["$m718",function($0){
this.promoMenu.setOpen(true)
},"promoMenu",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([{attrs:{attach:"right",name:"promoMenu"},children:[{attrs:{$delegates:["ondata","$m719",null,"onclick","$m720",null],clickable:true,datapath:"catalogds:/rsp/list/item[@tipocatalogo='p']",itemId:"0"},"class":$lzc$class_m729}],"class":$lzc$class_menu}],$lzc$class_linkbutton["children"]),"attributes",new LzInheritedHash($lzc$class_linkbutton.attributes)]);Class.make("$lzc$class_m730",$lzc$class_linkbutton,["$m721",function($0){
with(this){
canvas.selectItem("aboutus")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_linkbutton["children"]),"attributes",new LzInheritedHash($lzc$class_linkbutton.attributes)]);Class.make("$lzc$class_m731",$lzc$class_linkbutton,["$m722",function($0){
with(this){
canvas.selectItem("where")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_linkbutton["children"]),"attributes",new LzInheritedHash($lzc$class_linkbutton.attributes)]);Class.make("$lzc$class_m732",$lzc$class_linkbutton,["$m723",function($0){
with(this){
canvas.selectItem("story")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_linkbutton["children"]),"attributes",new LzInheritedHash($lzc$class_linkbutton.attributes)]);Class.make("$lzc$class_m733",$lzc$class_linkbutton,["$m724",function($0){
with(this){
canvas.selectItem("credit")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_linkbutton["children"]),"attributes",new LzInheritedHash($lzc$class_linkbutton.attributes)]);Class.make("$lzc$class_m725",LzView,["$m713",function(){
with(this){
var $0=canvas;return $0
}},"$m714",function($0){
with(this){
if(canvas.isUpToDate){
argobj=[{argname:"service",argvalue:"allCatalogList"}];gDataMan.doDSXRequest(catalogds,argobj)
}}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{spacing:5},"class":$lzc$class_simplelayout},{attrs:{$delegates:["onmousedown","$m715",null],catalogMenu:void 0,clickable:true,label:"Cataloghi",width:100},"class":$lzc$class_m726},{attrs:{$delegates:["onmousedown","$m718",null],clickable:true,label:"Promozioni",promoMenu:void 0,visible:false,width:100},"class":$lzc$class_m728},{attrs:{$delegates:["onclick","$m721",null],clickable:true,label:"Chi Siamo",width:100},"class":$lzc$class_m730},{attrs:{$delegates:["onclick","$m722",null],clickable:true,label:"Contatti",width:100},"class":$lzc$class_m731},{attrs:{$delegates:["onclick","$m723",null],clickable:true,label:"Storia",width:100},"class":$lzc$class_m732},{attrs:{$delegates:["onclick","$m724",null],clickable:true,label:"Credits",width:100},"class":$lzc$class_m733}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onisUpToDate","$m714","$m713"],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
links=$0
}else if(links===$0){
links=null
}},height:155,name:"links",x:475,y:350},"class":$lzc$class_m725},22);Class.make("$lzc$class_m789",LzAnimator,["$m738",function($0){
with(this){
var $1=canvas.logo1;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m739",function(){
with(this){
return [canvas,"logo1"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m790",LzAnimator,["$m740",function($0){
with(this){
var $1=canvas.logo1;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m741",function(){
with(this){
return [canvas,"logo1"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m791",LzAnimator,["$m742",function($0){
with(this){
var $1=canvas.logo1;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m743",function(){
with(this){
return [canvas,"logo1"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m792",LzAnimator,["$m744",function($0){
with(this){
var $1=canvas.logo1;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m745",function(){
with(this){
return [canvas,"logo1"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m793",LzAnimator,["$m746",function($0){
with(this){
var $1=canvas.logo2;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m747",function(){
with(this){
return [canvas,"logo2"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m794",LzAnimator,["$m748",function($0){
with(this){
var $1=canvas.logo2;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m749",function(){
with(this){
return [canvas,"logo2"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m795",LzAnimator,["$m750",function($0){
with(this){
var $1=canvas.logo2;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m751",function(){
with(this){
return [canvas,"logo2"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m796",LzAnimator,["$m752",function($0){
with(this){
var $1=canvas.logo2;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m753",function(){
with(this){
return [canvas,"logo2"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m797",LzAnimator,["$m754",function($0){
with(this){
var $1=canvas.logo3;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m755",function(){
with(this){
return [canvas,"logo3"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m798",LzAnimator,["$m756",function($0){
with(this){
var $1=canvas.logo3;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m757",function(){
with(this){
return [canvas,"logo3"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m799",LzAnimator,["$m758",function($0){
with(this){
var $1=canvas.logo3;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m759",function(){
with(this){
return [canvas,"logo3"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m800",LzAnimator,["$m760",function($0){
with(this){
var $1=canvas.logo3;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m761",function(){
with(this){
return [canvas,"logo3"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m801",LzAnimator,["$m762",function($0){
with(this){
var $1=canvas.large_cam;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m763",function(){
with(this){
return [canvas,"large_cam"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m802",LzAnimator,["$m764",function($0){
with(this){
var $1=canvas.large_cam;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m765",function(){
with(this){
return [canvas,"large_cam"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m788",LzAnimatorGroup,["$m736",function($0){
with(this){
canvas.logo1.setAttribute("stretches","both");canvas.logo2.setAttribute("stretches","both");canvas.logo3.setAttribute("stretches","both")
}},"$m737",function($0){
with(this){
canvas.large_cam.setAttribute("visible",false);canvas.little_cam.setAttribute("visible",true)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{attribute:"height",to:60},"class":LzAnimator},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m738","$m739"),to:130},"class":$lzc$class_m789},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m740","$m741"),to:10},"class":$lzc$class_m790},{attrs:{attribute:"width",target:new LzAlwaysExpr("$m742","$m743"),to:97},"class":$lzc$class_m791},{attrs:{attribute:"height",target:new LzAlwaysExpr("$m744","$m745"),to:30},"class":$lzc$class_m792},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m746","$m747"),to:235},"class":$lzc$class_m793},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m748","$m749"),to:12},"class":$lzc$class_m794},{attrs:{attribute:"width",target:new LzAlwaysExpr("$m750","$m751"),to:115},"class":$lzc$class_m795},{attrs:{attribute:"height",target:new LzAlwaysExpr("$m752","$m753"),to:32},"class":$lzc$class_m796},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m754","$m755"),to:355},"class":$lzc$class_m797},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m756","$m757"),to:10},"class":$lzc$class_m798},{attrs:{attribute:"width",target:new LzAlwaysExpr("$m758","$m759"),to:71},"class":$lzc$class_m799},{attrs:{attribute:"height",target:new LzAlwaysExpr("$m760","$m761"),to:34},"class":$lzc$class_m800},{attrs:{attribute:"width",target:new LzAlwaysExpr("$m762","$m763"),to:104},"class":$lzc$class_m801},{attrs:{attribute:"height",target:new LzAlwaysExpr("$m764","$m765"),to:89},"class":$lzc$class_m802}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m804",LzAnimator,["$m767",function($0){
with(this){
var $1=canvas.logo1;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m768",function(){
with(this){
return [canvas,"logo1"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m805",LzAnimator,["$m769",function($0){
with(this){
var $1=canvas.logo1;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m770",function(){
with(this){
return [canvas,"logo1"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m806",LzAnimator,["$m771",function($0){
with(this){
var $1=canvas.logo2;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m772",function(){
with(this){
return [canvas,"logo2"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m807",LzAnimator,["$m773",function($0){
with(this){
var $1=canvas.logo2;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m774",function(){
with(this){
return [canvas,"logo2"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m808",LzAnimator,["$m775",function($0){
with(this){
var $1=canvas.logo3;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m776",function(){
with(this){
return [canvas,"logo3"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m809",LzAnimator,["$m777",function($0){
with(this){
var $1=canvas.logo3;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m778",function(){
with(this){
return [canvas,"logo3"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m810",LzAnimator,["$m779",function($0){
with(this){
var $1=canvas.large_cam;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m780",function(){
with(this){
return [canvas,"large_cam"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m811",LzAnimator,["$m781",function($0){
with(this){
var $1=canvas.large_cam;if($1!==this["target"]||!this.inited){
this.setAttribute("target",$1)
}}},"$m782",function(){
with(this){
return [canvas,"large_cam"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzAnimator.attributes)]);Class.make("$lzc$class_m803",LzAnimatorGroup,["$m766",function($0){
with(this){
canvas.logo1.setAttribute("stretches","none");canvas.logo2.setAttribute("stretches","none");canvas.logo3.setAttribute("stretches","none");canvas.large_cam.setAttribute("visible",true);canvas.little_cam.setAttribute("visible",false)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{attribute:"height",to:300},"class":LzAnimator},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m767","$m768"),to:460},"class":$lzc$class_m804},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m769","$m770"),to:40},"class":$lzc$class_m805},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m771","$m772"),to:520},"class":$lzc$class_m806},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m773","$m774"),to:106},"class":$lzc$class_m807},{attrs:{attribute:"x",target:new LzAlwaysExpr("$m775","$m776"),to:469},"class":$lzc$class_m808},{attrs:{attribute:"y",target:new LzAlwaysExpr("$m777","$m778"),to:170},"class":$lzc$class_m809},{attrs:{attribute:"width",target:new LzAlwaysExpr("$m779","$m780"),to:387},"class":$lzc$class_m810},{attrs:{attribute:"height",target:new LzAlwaysExpr("$m781","$m782"),to:299},"class":$lzc$class_m811}],"attributes",new LzInheritedHash(LzAnimatorGroup.attributes)]);Class.make("$lzc$class_m812",LzView,["$m783",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m784",function(){
with(this){
return [parent,"width"]
}},"$m785",function($0){
with(this){
var $1=parent.height-1;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m786",function(){
with(this){
return [parent,"height"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m787",LzView,["$m734",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m735",function(){
with(this){
return [parent,"width"]
}},"anm",void 0,"anmb",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{$delegates:["onstart","$m736",null,"onstop","$m737",null],duration:"1000",name:"anm",process:"simultaneous",start:false},"class":$lzc$class_m788},{attrs:{$delegates:["onstart","$m766",null],duration:"1000",name:"anmb",process:"simultaneous",start:false},"class":$lzc$class_m803},{attrs:{bgcolor:LzColorUtils.convertColor("0xffffff"),height:1,width:new LzAlwaysExpr("$m783","$m784"),y:new LzAlwaysExpr("$m785","$m786")},"class":$lzc$class_m812}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
topview=$0
}else if(topview===$0){
topview=null
}},anm:void 0,anmb:void 0,bgcolor:LzColorUtils.convertColor("0x1f13b1"),height:300,name:"topview",opacity:1,width:new LzAlwaysExpr("$m734","$m735")},"class":$lzc$class_m787},28);Class.make("$lzc$class_m817",LzView,["$m813",function($0){
with(this){
var $1=parent.topview.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m814",function(){
with(this){
return [parent.topview,"width"]
}},"$m815",function($0){
with(this){
var $1=parent.topview.height;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m816",function(){
with(this){
return [parent.topview,"height"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{resource:"$LZ14",stretches:"width",width:new LzAlwaysExpr("$m813","$m814"),y:new LzAlwaysExpr("$m815","$m816")},"class":$lzc$class_m817},1);Class.make("$lzc$class_m819",LzView,["$m818",function($0){
with(this){
canvas.setOpen(false)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onclick","$m818",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
large_cam=$0
}else if(large_cam===$0){
large_cam=null
}},clickable:true,name:"large_cam",resource:"$LZ15",stretches:"both",x:6},"class":$lzc$class_m819},1);Class.make("$lzc$class_m821",LzView,["$m820",function($0){
with(this){
canvas.setOpen(false)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onclick","$m820",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
little_cam=$0
}else if(little_cam===$0){
little_cam=null
}},clickable:true,name:"little_cam",resource:"$LZ16",visible:false,x:3},"class":$lzc$class_m821},1);Class.make("$lzc$class_m823",LzView,["$m822",function($0){
with(this){
canvas.setOpen(false)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onclick","$m822",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
logo1=$0
}else if(logo1===$0){
logo1=null
}},clickable:true,name:"logo1",resource:"$LZ17",x:460,y:40},"class":$lzc$class_m823},1);Class.make("$lzc$class_m825",LzView,["$m824",function($0){
with(this){
canvas.setOpen(false)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onclick","$m824",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
logo2=$0
}else if(logo2===$0){
logo2=null
}},clickable:true,name:"logo2",resource:"$LZ18",x:520,y:106},"class":$lzc$class_m825},1);Class.make("$lzc$class_m827",LzView,["$m826",function($0){
with(this){
canvas.setOpen(false)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onclick","$m826",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
logo3=$0
}else if(logo3===$0){
logo3=null
}},clickable:true,name:"logo3",resource:"$LZ19",x:469,y:170},"class":$lzc$class_m827},1);Class.make("$lzc$class_m828",$lzc$class_search,["firsttime",void 0,"sendsearch",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};photoscontainer.setAttribute("visible",false);photoscontainer.lyt.isgrid=true;photoscontainer.lyt.myreset();photoscontainer.lyt.lock();canvas.spnr.setAttribute("visible",true);photoscontainer.lyt.transitiontogrid_anm.doStart();canvas.details.setAttribute("visible",false);if(this.firsttime){
this.firsttime=false;photoscontainer.lyt.calcpageparams=true
}else{
photoscontainer.lyt.calcpageparams=false
};tls.reset();tls.enableZoom();gResultsCountLabel.setAttribute("text","...searching...");(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["sendsearch"]||this.nextMethod(arguments.callee,"sendsearch")).call(this)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_search["children"]),"attributes",new LzInheritedHash($lzc$class_search.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gSearch";gSearch=$0
}else if(gSearch===$0){
gSearch=null;$0.id=null
}},$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
srch=$0
}else if(srch===$0){
srch=null
}},firsttime:true,id:"gSearch",name:"srch",width:156,x:475,y:315},"class":$lzc$class_m828},13);Class.make("$lzc$class_m861",LzView,["$m842",function($0){
with(this){
this.setAttribute("x",90*photoscontainer.width*photoscontainer.height/(1665*829))
}},"$m843",function($0){
this.startdragging()
},"$m844",function($0){
this.stopdragging()
},"startdragging",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};dragging.setAttribute("applied",true);photoscontainer.lyt.calcpageparams=true;photoscontainer.lyt.setAttribute("textvisible",false);photoscontainer.lyt.update()
}},"stopdragging",function(){
with(this){
dragging.setAttribute("applied",false);photoscontainer.lyt.setAttribute("textvisible",true);photoscontainer.lyt.update()
}},"dragging",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{drag_axis:"x",drag_max_x:117,drag_min_x:5,name:"dragging"},"class":$lzc$class_dragstate}],"attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m862",LzView,["$m845",function($0){},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m863",LzText,["$m846",function($0){
var $1=152-this.width/2;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}},"$m847",function(){
return [this,"width"]
},"$m848",function($0){
with(this){
var $1=!parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m849",function(){
with(this){
return [parent,"waitforload"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m864",LzText,["$m850",function($0){
with(this){
var $1=!parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m851",function(){
with(this){
return [parent,"waitforload"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m865",LzText,["$m852",function($0){
with(this){
var $1=!parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m853",function(){
with(this){
return [parent,"waitforload"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m866",LzText,["$m854",function($0){
var $1=170-this.width/2;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}},"$m855",function(){
return [this,"width"]
},"$m856",function($0){
with(this){
var $1=parent.waitforload;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m857",function(){
with(this){
return [parent,"waitforload"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m867",$lzc$class_mybutton,["$m858",function($0){
with(this){
parent.displayPrevPage()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);Class.make("$lzc$class_m868",$lzc$class_mybutton,["$m859",function($0){
with(this){
parent.displayNextPage()
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);Class.make("$lzc$class_m860",LzView,["$m829",function($0){
with(this){
var $1=canvas.height-50;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m830",function(){
with(this){
return [canvas,"height"]
}},"$m831",function($0){
with(this){
var $1=Math.max(1,Math.min(1+this.thmb.x/60,3));if($1!==this["zoomscale"]||!this.inited){
this.setAttribute("zoomscale",$1)
}}},"$m832",function(){
with(this){
return [this.thmb,"x"].concat(Math["$lzc$max_dependencies"]?Math["$lzc$max_dependencies"](this,Math,1,Math.min(1+this.thmb.x/60,3)):[]).concat(Math["$lzc$min_dependencies"]?Math["$lzc$min_dependencies"](this,Math,1+this.thmb.x/60,3):[])
}},"zoomscale",void 0,"numberofpages",void 0,"pagesize",void 0,"startindex",void 0,"nextstartindex",void 0,"endindex",void 0,"nextendindex",void 0,"waitforload",void 0,"numberofphotos",void 0,"$m833",function($0){
with(this){
this.setAttribute("displayPage_del",new LzDelegate(this,"displayPage"))
}},"displayPage_del",void 0,"$m834",function(){
with(this){
var $0=photods;return $0
}},"$m835",function($0){
with(this){
this.setAttribute("waitforload",false);photoscontainer.lyt.calcparams=true
}},"$m836",function(){
with(this){
var $0=photoscontainer.lyt;return $0
}},"$m837",function($0){
with(this){
this.endindex=this.startindex+photoscontainer.lyt.pagesize-1;this.displayRange(this.startindex,this.endindex)
}},"$m838",function(){
with(this){
var $0=photoscontainer.lyt.pageNext;return $0
}},"$m839",function($0){
with(this){
this.displayPage();photoscontainer.lyt.calcpageparams=true;if(photoscontainer.lyt.isgrid){
photoscontainer.lyt.setAttribute("pivot_x",50)
}else{
photoscontainer.lyt.setAttribute("pivot_x",10);photoscontainer.lyt.setAttribute("pivotindex",0)
}}},"$m840",function(){
with(this){
var $0=photoscontainer.lyt.pagePrev;return $0
}},"$m841",function($0){
with(this){
this.displayPage();photoscontainer.lyt.calcpageparams=true;if(photoscontainer.lyt.isgrid){
photoscontainer.lyt.setAttribute("pivot_x",50)
}else{
photoscontainer.lyt.setAttribute("pivot_x",10);photoscontainer.lyt.setAttribute("pivotindex",0)
}}},"reset",function(){
with(this){
this.startindex=1;this.endindex=this.startindex+photoscontainer.lyt.pagesize-1;this.displayRange(this.startindex,this.endindex)
}},"resetOnLoad",function(){
with(this){
this.loadtext.setAttribute("text","Loading...");this.setAttribute("waitforload",true);this.setAttribute("startindex",1);this.setAttribute("nextstartindex",1);this.setAttribute("endindex",1+photoscontainer.lyt.totalitems);this.setAttribute("nextendindex",1+photoscontainer.lyt.totalitems)
}},"enableZoom",function(){
with(this){
thmb.setAttribute("visible",true);zoomScreen.setAttribute("visible",false)
}},"disableZoom",function(){
with(this){
thmb.setAttribute("visible",false);zoomScreen.setAttribute("visible",true)
}},"displayNextPage",function(){
with(this){
this.nextstartindex=this.endindex+1;this.nextendindex=Math.min(this.nextstartindex+photoscontainer.lyt.pagesize-1,this.numberofphotos);if(this.nextstartindex<=this.numberofphotos){
if(nextendindex==this.numberofphotos){
this.nextstartindex=this.nextendindex-photoscontainer.lyt.totalitems+1
};photoscontainer.lyt.calcpageparams=false;photoscontainer.lyt.pageNext.doStart()
}else if(photoscontainer.lyt.currentpage<photoscontainer.lyt.totalpages){
this.loadtext.setAttribute("text","Next 100...");this.setAttribute("waitforload",true);this.nextstartindex=1;this.nextendindex=Math.min(this.nextstartindex+photoscontainer.lyt.pagesize-1,this.numberofphotos);photoscontainer.lyt.setAttribute("currentpage",Number(photoscontainer.lyt.currentpage)+1);gSearch.doSearch()
}}},"displayPrevPage",function(){
with(this){
if(this.startindex>1){
if(photoscontainer.lyt.totalitems<=photoscontainer.lyt.pagesize){
this.nextendindex=this.startindex-1;this.nextstartindex=Math.max(this.nextendindex-photoscontainer.lyt.totalitems+1,1);if(this.nextstartindex==1)this.nextendindex=photoscontainer.lyt.totalitems
}else{
this.nextendindex=this.startindex-1;this.nextstartindex=Math.max(this.nextendindex-photoscontainer.lyt.pagesize+1,1)
};photoscontainer.lyt.calcpageparams=false;photoscontainer.lyt.pagePrev.doStart()
}else if(photoscontainer.lyt.currentpage>1){
this.loadtext.setAttribute("text","Prev 100...");this.setAttribute("waitforload",true);this.nextendindex=this.numberofphotos;this.nextstartindex=this.nextendindex-photoscontainer.lyt.totalitems+1;photoscontainer.lyt.setAttribute("currentpage",Number(photoscontainer.lyt.currentpage)-1);gSearch.doSearch()
}}},"displayPage",function($0){
with(this){
switch(arguments.length){
case 0:
$0=null;

};displayPage_del.unregisterAll();var $1=20;var $2=this.nextstartindex+$1;var $3="photods:/rsp/photos/photo["+this.nextstartindex+"-"+$2+"]";photoscontainer.ph.setAttribute("datapath",$3);photoscontainer.watchforlast();this.startindex=this.nextstartindex;this.endindex=this.nextendindex
}},"displayRange",function($0,$1){
with(this){
this.firstphotoindex.setAttribute("text",$0+photoscontainer.lyt.perpage*(photoscontainer.lyt.currentpage-1));this.lastphotoindex.setAttribute("text",$1+photoscontainer.lyt.perpage*(photoscontainer.lyt.currentpage-1))
}},"setpPagingParams",function($0){
with(this){
this.setAttribute("numberofphotos",Number($0))
}},"zoombg",void 0,"thmb",void 0,"zoomScreen",void 0,"firstphotoindex",void 0,"lastphotoindex",void 0,"loadtext",void 0,"pageprev",void 0,"pagenext",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{name:"zoombg",resource:"$LZ20",x:0,y:13},"class":LzView},{attrs:{$delegates:["onmousedown","$m843",null,"onmouseup","$m844",null],clickable:true,dragging:void 0,name:"thmb",resource:"$LZ21",x:new LzOnceExpr("$m842"),y:16},"class":$lzc$class_m861},{attrs:{$delegates:["onclick","$m845",null],bgcolor:LzColorUtils.convertColor("0x7f7f7f"),clickable:false,height:30,name:"zoomScreen",opacity:0.5,showhandcursor:false,visible:false,width:130,x:0,y:13},"class":$lzc$class_m862},{attrs:{fgcolor:LzColorUtils.convertColor("0x463e9d"),font:"Verdana,Vera,sans-serif",fontsize:9,fontstyle:"plain",name:"firstphotoindex",visible:new LzAlwaysExpr("$m848","$m849"),x:new LzAlwaysExpr("$m846","$m847"),y:7},"class":$lzc$class_m863},{attrs:{fgcolor:LzColorUtils.convertColor("0x463e9d"),font:"Verdana,Vera,sans-serif",fontsize:11,fontstyle:"plain",text:"-",visible:new LzAlwaysExpr("$m850","$m851"),x:162,y:4},"class":$lzc$class_m864},{attrs:{fgcolor:LzColorUtils.convertColor("0x463e9d"),font:"Verdana,Vera,sans-serif",fontsize:9,fontstyle:"plain",name:"lastphotoindex",visible:new LzAlwaysExpr("$m852","$m853"),x:172,y:7},"class":$lzc$class_m865},{attrs:{fgcolor:LzColorUtils.convertColor("0x463e9d"),font:"Verdana,Vera,sans-serif",fontsize:9,fontstyle:"plain",name:"loadtext",visible:new LzAlwaysExpr("$m856","$m857"),x:new LzAlwaysExpr("$m854","$m855"),y:7},"class":$lzc$class_m866},{attrs:{$delegates:["onclick","$m858",null],clickable:true,name:"pageprev",resource:"prevpgae_rsc",x:138,y:21},"class":$lzc$class_m867},{attrs:{$delegates:["onclick","$m859",null],clickable:true,name:"pagenext",resource:"nextpage_rsc",x:168,y:21},"class":$lzc$class_m868}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["ondata","$m835","$m834","onpageend","$m837","$m836","onstop","$m839","$m838","onstop","$m841","$m840"],$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="tls";tls=$0
}else if(tls===$0){
tls=null;$0.id=null
}},$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
tools=$0
}else if(tools===$0){
tools=null
}},displayPage_del:new LzOnceExpr("$m833"),endindex:18,firstphotoindex:void 0,id:"tls",lastphotoindex:void 0,loadtext:void 0,name:"tools",nextendindex:18,nextstartindex:1,numberofpages:0,numberofphotos:0,opacity:0,pagenext:void 0,pageprev:void 0,pagesize:18,startindex:1,thmb:void 0,waitforload:false,x:1000,y:new LzAlwaysExpr("$m829","$m830"),zoomScreen:void 0,zoombg:void 0,zoomscale:new LzAlwaysExpr("$m831","$m832")},"class":$lzc$class_m860},11);canvas.LzInstantiateView({attrs:{$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
spnr=$0
}else if(spnr===$0){
spnr=null
}},align:"center",name:"spnr",valign:"middle"},"class":$lzc$class_spinner},1);Class.make("$lzc$class_m873",LzView,["$m869",function($0){
with(this){
var $1=parent.width;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m870",function(){
with(this){
return [parent,"width"]
}},"$m871",function($0){
with(this){
var $1=parent.height-this.y;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m872",function(){
with(this){
return [parent,"height",this,"y"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="scrn";scrn=$0
}else if(scrn===$0){
scrn=null;$0.id=null
}},bgcolor:LzColorUtils.convertColor("0x0"),clickable:true,height:new LzAlwaysExpr("$m871","$m872"),id:"scrn",opacity:0.3,visible:false,width:new LzAlwaysExpr("$m869","$m870"),y:91},"class":$lzc$class_m873},1);Class.make("$lzc$class_m876",$lzc$class_clipboard,["$m874",function($0){
with(this){
var $1=canvas.height-50;if($1!==this["y"]||!this.inited){
this.setAttribute("y",$1)
}}},"$m875",function(){
with(this){
return [canvas,"height"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_clipboard["children"]),"attributes",new LzInheritedHash($lzc$class_clipboard.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gClipboard";gClipboard=$0
}else if(gClipboard===$0){
gClipboard=null;$0.id=null
}},id:"gClipboard",width:590,x:-800,y:new LzAlwaysExpr("$m874","$m875")},"class":$lzc$class_m876},17);canvas.LzInstantiateView({attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gDragged";gDragged=$0
}else if(gDragged===$0){
gDragged=null;$0.id=null
}},id:"gDragged"},"class":$lzc$class_draggedphotos},2);Class.make("$lzc$class_m887",$lzc$class_hscrollbar,["$m882",function($0){
with(this){
var $1=parent.cont.width>parent.width;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m883",function(){
with(this){
return [parent.cont,"width",parent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_hscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_hscrollbar.attributes)]);Class.make("$lzc$class_m888",$lzc$class_vscrollbar,["$m884",function($0){
with(this){
var $1=parent.cont.height>parent.height;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m885",function(){
with(this){
return [parent.cont,"height",parent,"height"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_vscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_vscrollbar.attributes)]);Class.make("$lzc$class_m886",LzView,["$m877",function($0){
with(this){
var $1=canvas.height-150;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m878",function(){
with(this){
return [canvas,"height"]
}},"$m879",function($0){
with(this){
var $1=canvas.width-35;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m880",function(){
with(this){
return [canvas,"width"]
}},"$m881",function($0){
with(this){
gClipboard.setAttribute("visible",!visible);if(visible){
cont.abouthtml.setAttribute("src","views/about_us.html")
}}},"cont",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{abouthtml:void 0,clip:false,name:"cont"},children:[{attrs:{height:600,name:"abouthtml",width:850},"class":$lzc$class_html}],"class":LzView},{attrs:{visible:new LzAlwaysExpr("$m882","$m883")},"class":$lzc$class_m887},{attrs:{visible:new LzAlwaysExpr("$m884","$m885")},"class":$lzc$class_m888}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onvisible","$m881",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
aboutusView=$0
}else if(aboutusView===$0){
aboutusView=null
}},clip:true,cont:void 0,height:new LzAlwaysExpr("$m877","$m878"),name:"aboutusView",visible:false,width:new LzAlwaysExpr("$m879","$m880"),x:20,y:100},"class":$lzc$class_m886},42);Class.make("$lzc$class_m899",$lzc$class_hscrollbar,["$m894",function($0){
with(this){
var $1=parent.cont.width>parent.width;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m895",function(){
with(this){
return [parent.cont,"width",parent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_hscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_hscrollbar.attributes)]);Class.make("$lzc$class_m900",$lzc$class_vscrollbar,["$m896",function($0){
with(this){
var $1=parent.cont.height>parent.height;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m897",function(){
with(this){
return [parent.cont,"height",parent,"height"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_vscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_vscrollbar.attributes)]);Class.make("$lzc$class_m898",LzView,["$m889",function($0){
with(this){
var $1=canvas.height-150;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m890",function(){
with(this){
return [canvas,"height"]
}},"$m891",function($0){
with(this){
var $1=canvas.width-35;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m892",function(){
with(this){
return [canvas,"width"]
}},"$m893",function($0){
with(this){
gClipboard.setAttribute("visible",!visible);if(visible){
cont.gmapshtml.setAttribute("src","views/map.html")
}}},"cont",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{clip:false,gmapshtml:void 0,name:"cont"},children:[{attrs:{axis:"y",spacing:10},"class":$lzc$class_simplelayout},{children:[{attrs:{multiline:true,text:'Vieni a trovarci! In via giovanni verga 24 Mistretta(ME) con piu&apos; di 2000 metri quadri di esposizione.<br/>Per maggiori informazioni contattaci <a href="mailto:web@mobilquattrosud.com">web@mobilquattrosud.com</a>'},"class":$lzc$class_sText}],"class":LzView},{attrs:{height:600,name:"gmapshtml",width:850},"class":$lzc$class_html}],"class":LzView},{attrs:{visible:new LzAlwaysExpr("$m894","$m895")},"class":$lzc$class_m899},{attrs:{visible:new LzAlwaysExpr("$m896","$m897")},"class":$lzc$class_m900}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onvisible","$m893",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
mapView=$0
}else if(mapView===$0){
mapView=null
}},clip:true,cont:void 0,height:new LzAlwaysExpr("$m889","$m890"),name:"mapView",visible:false,width:new LzAlwaysExpr("$m891","$m892"),x:20,y:100},"class":$lzc$class_m898},45);Class.make("$lzc$class_m911",$lzc$class_hscrollbar,["$m906",function($0){
with(this){
var $1=parent.cont.width>parent.width;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m907",function(){
with(this){
return [parent.cont,"width",parent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_hscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_hscrollbar.attributes)]);Class.make("$lzc$class_m912",$lzc$class_vscrollbar,["$m908",function($0){
with(this){
var $1=parent.cont.height>parent.height;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m909",function(){
with(this){
return [parent.cont,"height",parent,"height"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_vscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_vscrollbar.attributes)]);Class.make("$lzc$class_m910",LzView,["$m901",function($0){
with(this){
var $1=canvas.height-150;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m902",function(){
with(this){
return [canvas,"height"]
}},"$m903",function($0){
with(this){
var $1=canvas.width-35;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m904",function(){
with(this){
return [canvas,"width"]
}},"$m905",function($0){
with(this){
gClipboard.setAttribute("visible",!visible);if(visible){
cont.storyhtml.setAttribute("src","views/story.html")
}}},"cont",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{clip:false,name:"cont",storyhtml:void 0},children:[{attrs:{height:1500,name:"storyhtml",width:850},"class":$lzc$class_html}],"class":LzView},{attrs:{visible:new LzAlwaysExpr("$m906","$m907")},"class":$lzc$class_m911},{attrs:{visible:new LzAlwaysExpr("$m908","$m909")},"class":$lzc$class_m912}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onvisible","$m905",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
storyView=$0
}else if(storyView===$0){
storyView=null
}},clip:true,cont:void 0,height:new LzAlwaysExpr("$m901","$m902"),name:"storyView",visible:false,width:new LzAlwaysExpr("$m903","$m904"),x:20,y:100},"class":$lzc$class_m910},42);Class.make("$lzc$class_m950",LzView,["$m918",function($0){
with(this){
lz.Browser.loadURL("http://www.openlaszlo.org/")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m951",$lzc$class_sText,["$m919",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+70;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m920",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m952",LzView,["$m921",function($0){
with(this){
lz.Browser.loadURL("http://launchpad.net/olwebsuite")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m953",$lzc$class_sText,["$m922",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+162;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m923",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m954",LzView,["$m924",function($0){
with(this){
lz.Browser.loadURL("http://mysql.com/")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m955",$lzc$class_sText,["$m925",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+127;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m926",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m956",LzView,["$m927",function($0){
with(this){
lz.Browser.loadURL("http://www.php.net/")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m957",$lzc$class_sText,["$m928",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+128;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m929",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m958",LzView,["$m930",function($0){
with(this){
lz.Browser.loadURL("http://qt.nokia.com/")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m959",$lzc$class_sText,["$m931",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+164;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m932",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m960",LzView,["$m933",function($0){
with(this){
lz.Browser.loadURL("http://www.kubuntu.org/")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m961",$lzc$class_sText,["$m934",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+92;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m935",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m962",LzView,["$m936",function($0){
with(this){
lz.Browser.loadURL("http://www.inkscape.org/")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m963",$lzc$class_sText,["$m937",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+157;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m938",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m964",LzView,["$m939",function($0){
with(this){
lz.Browser.loadURL("http://www.fsf.org/licensing/licenses/gpl.html")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m965",LzView,["$m940",function($0){
with(this){
lz.Browser.loadURL("http://www.fsf.org/licensing/licenses/lgpl.html")
}},"$m941",function($0){
with(this){
var $1=parent.logo0.x+parent.logo0.width+10;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m942",function(){
with(this){
return [parent.logo0,"x",parent.logo0,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzView.attributes)]);Class.make("$lzc$class_m966",$lzc$class_sText,["$m943",function($0){
with(this){
var $1=parent.logo.x+parent.logo.width+47;if($1!==this["x"]||!this.inited){
this.setAttribute("x",$1)
}}},"$m944",function(){
with(this){
return [parent.logo,"x",parent.logo,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash($lzc$class_sText.attributes)]);Class.make("$lzc$class_m967",$lzc$class_hscrollbar,["$m945",function($0){
with(this){
var $1=parent.cont.width>parent.width;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m946",function(){
with(this){
return [parent.cont,"width",parent,"width"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_hscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_hscrollbar.attributes)]);Class.make("$lzc$class_m968",$lzc$class_vscrollbar,["$m947",function($0){
with(this){
var $1=parent.cont.height>parent.height;if($1!==this["visible"]||!this.inited){
this.setAttribute("visible",$1)
}}},"$m948",function(){
with(this){
return [parent.cont,"height",parent,"height"]
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",LzNode.mergeChildren([],$lzc$class_vscrollbar["children"]),"attributes",new LzInheritedHash($lzc$class_vscrollbar.attributes)]);Class.make("$lzc$class_m949",LzView,["$m913",function($0){
with(this){
var $1=canvas.height-150;if($1!==this["height"]||!this.inited){
this.setAttribute("height",$1)
}}},"$m914",function(){
with(this){
return [canvas,"height"]
}},"$m915",function($0){
with(this){
var $1=canvas.width-35;if($1!==this["width"]||!this.inited){
this.setAttribute("width",$1)
}}},"$m916",function(){
with(this){
return [canvas,"width"]
}},"$m917",function($0){
with(this){
gClipboard.setAttribute("visible",!visible)
}},"cont",void 0,"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{clip:false,name:"cont"},children:[{attrs:{axis:"y",spacing:20},"class":$lzc$class_simplelayout},{attrs:{x:50},children:[{attrs:{axis:"y",spacing:5},"class":$lzc$class_simplelayout},{children:[{attrs:{fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:14,fontstyle:"bold",text:"Web Engineer:"},"class":$lzc$class_sText},{attrs:{fontsize:12,text:'<a href="mailto:gmazzurco89@gmail.com">Gioacchino Mazzurco</a>.',x:360},"class":$lzc$class_sText}],"class":LzView},{children:[{attrs:{fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:14,fontstyle:"bold",text:"Database Engineer:"},"class":$lzc$class_sText},{attrs:{fontsize:12,text:'<a href="mailto:s4lv00@gmail.com">Salvatore Mazzurco</a>.',x:360},"class":$lzc$class_sText}],"class":LzView},{children:[{attrs:{fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:14,fontstyle:"bold",text:"PHP Database interface Engineer:"},"class":$lzc$class_sText},{attrs:{fontsize:12,text:'<a href="mailto:s4lv00@gmail.com">Salvatore Mazzurco</a>.',x:360},"class":$lzc$class_sText}],"class":LzView},{children:[{attrs:{fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:14,fontstyle:"bold",text:"Content Managment System GUI Engineer:"},"class":$lzc$class_sText},{attrs:{fontsize:12,text:'<a href="mailto:gmazzurco89@gmail.com">Gioacchino Mazzurco</a>.',x:360},"class":$lzc$class_sText}],"class":LzView},{children:[{attrs:{fgcolor:LzColorUtils.convertColor("0x1f13b1"),fontsize:14,fontstyle:"bold",text:"Special thanks to:"},"class":$lzc$class_sText}],"class":LzView}],"class":LzView},{attrs:{logo:void 0},children:[{attrs:{$delegates:["onclick","$m918",null],clickable:true,name:"logo",resource:"$LZ22",x:20},"class":$lzc$class_m950},{attrs:{fontsize:12,multiline:true,text:'<b>Open Laszlo sponsors and community</b><br/><a href="http://www.openlaszlo.org/">www.openlaszlo.org</a><br/>Who created LZPIX upon this site is based with a lot of modification.',width:300,x:new LzAlwaysExpr("$m919","$m920")},"class":$lzc$class_m951}],"class":LzView},{attrs:{logo:void 0},children:[{attrs:{$delegates:["onclick","$m921",null],clickable:true,name:"logo",resource:"$LZ23",x:140},"class":$lzc$class_m952},{attrs:{fontsize:12,multiline:true,text:'<b>Open Multimedia Web Suite developers</b><br/><a href="http://launchpad.net/olwebsuite">launchpad.net/olwebsuite</a><br/>Who adapted LZPIX and Open Multimedia Web Suite code to this site.',width:300,x:new LzAlwaysExpr("$m922","$m923")},"class":$lzc$class_m953}],"class":LzView},{attrs:{logo:void 0},children:[{attrs:{$delegates:["onclick","$m924",null],clickable:true,name:"logo",resource:"$LZ24",x:120},"class":$lzc$class_m954},{attrs:{fontsize:12,multiline:true,text:'<b>MySQL sponsors and community</b><br/><a href="http://mysql.com/">www.mysql.com/</a><br/>This site store data on a MySQL database.',width:300,x:new LzAlwaysExpr("$m925","$m926")},"class":$lzc$class_m955}],"class":LzView},{attrs:{logo:void 0},children:[{attrs:{$delegates:["onclick","$m927",null],clickable:true,name:"logo",resource:"$LZ25",x:120},"class":$lzc$class_m956},{attrs:{fontsize:12,multiline:true,text:'<b>PHP sponsors and community</b><br/><a href="http://www.php.net/">www.php.net</a><br/>The program who take data from DB is written in php.',width:300,x:new LzAlwaysExpr("$m928","$m929")},"class":$lzc$class_m957}],"class":LzView},{attrs:{logo:void 0},children:[{attrs:{bgcolor:LzColorUtils.convertColor("0xffffff"),height:42,width:35,x:160,y:10},"class":LzView},{attrs:{$delegates:["onclick","$m930",null],clickable:true,name:"logo",resource:"$LZ26",x:150},"class":$lzc$class_m958},{attrs:{fontsize:12,multiline:true,text:'<b>QT sponsors and community</b><br/><a href="http://qt.nokia.com/">www.qt.nokia.com</a><br/>The toolkit used to create the administration GUI.',width:300,x:new LzAlwaysExpr("$m931","$m932")},"class":$lzc$class_m959}],"class":LzView},{attrs:{logo:void 0},children:[{attrs:{$delegates:["onclick","$m933",null],clickable:true,name:"logo",resource:"$LZ27",x:50},"class":$lzc$class_m960},{attrs:{fontsize:12,multiline:true,text:'<b>Kubuntu sponsors and community</b><br/><a href="http://www.kubuntu.org/">www.kubuntu.org</a><br/>This site was created using open source software running on Kubuntu operanting system.',width:300,x:new LzAlwaysExpr("$m934","$m935")},"class":$lzc$class_m961}],"class":LzView},{attrs:{logo:void 0},children:[{attrs:{$delegates:["onclick","$m936",null],clickable:true,name:"logo",resource:"$LZ28",x:144},"class":$lzc$class_m962},{attrs:{fontsize:12,multiline:true,text:'<b>Inkscape sponsors and community</b><br/><a href="http://www.inkscape.org/">www.inkscape.org</a><br/>The program we used to design logo and some other image used on this site.',width:300,x:new LzAlwaysExpr("$m937","$m938")},"class":$lzc$class_m963}],"class":LzView},{attrs:{logo:void 0,logo0:void 0},children:[{attrs:{bgcolor:LzColorUtils.convertColor("0xffffff"),height:62,width:273,x:44},"class":LzView},{attrs:{$delegates:["onclick","$m939",null],clickable:true,name:"logo0",resource:"$LZ29",x:46},"class":$lzc$class_m964},{attrs:{$delegates:["onclick","$m940",null],clickable:true,name:"logo",resource:"$LZ30",x:new LzAlwaysExpr("$m941","$m942")},"class":$lzc$class_m965},{attrs:{fontsize:12,multiline:true,text:'<b>Free Software Foundation</b><br/><a href="http://www.fsf.org">http://www.fsf.org</a><br/>This is free software released part under GPL and part under LGPL licenses.',width:300,x:new LzAlwaysExpr("$m943","$m944")},"class":$lzc$class_m966}],"class":LzView}],"class":LzView},{attrs:{visible:new LzAlwaysExpr("$m945","$m946")},"class":$lzc$class_m967},{attrs:{visible:new LzAlwaysExpr("$m947","$m948")},"class":$lzc$class_m968}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$delegates:["onvisible","$m917",null],$lzc$bind_name:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
creditView=$0
}else if(creditView===$0){
creditView=null
}},clip:true,cont:void 0,height:new LzAlwaysExpr("$m913","$m914"),name:"creditView",visible:false,width:new LzAlwaysExpr("$m915","$m916"),x:20,y:100},"class":$lzc$class_m949},85);Class.make("$lzc$class_m974",LzText,["$m971",function($0){
with(this){
this.setAttribute("x",parent.width/2-width/2)
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["attributes",new LzInheritedHash(LzText.attributes)]);Class.make("$lzc$class_m975",$lzc$class_mybutton,["$m972",function($0){
with(this){
lz.Browser.loadURL("javascript:window.location.href=window.location.href")
}},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{font:"Verdana,Vera,sans-serif",fontsize:11,fontstyle:"plain",text:"ok",valign:"middle",x:8},"class":LzText}],"attributes",new LzInheritedHash($lzc$class_mybutton.attributes)]);Class.make("$lzc$class_m973",LzView,["$m969",function($0){
with(this){
this.setAttribute("x",parent.width/2-width/2)
}},"$m970",function($0){
with(this){
this.setAttribute("y",parent.height/2-height/2)
}},"title",void 0,"show",function($0){
switch(arguments.length){
case 0:
$0=null;

};this.setAttribute("visible",true)
},"$lzsc$initialize",function($0,$1,$2,$3){
switch(arguments.length){
case 0:
$0=null;
case 1:
$1=null;
case 2:
$2=null;
case 3:
$3=false;

};(arguments.callee["$superclass"]&&arguments.callee.$superclass.prototype["$lzsc$initialize"]||this.nextMethod(arguments.callee,"$lzsc$initialize")).call(this,$0,$1,$2,$3)
}],["children",[{attrs:{fgcolor:LzColorUtils.convertColor("0xffffff"),font:"Verdana,Vera,sans-serif",fontsize:18,fontstyle:"plain",name:"title",text:"Data source error",width:170,x:new LzOnceExpr("$m971"),y:15},"class":$lzc$class_m974},{attrs:{font:"Verdana,Vera,sans-serif",fontsize:11,fontstyle:"plain",multiline:true,text:"Premi OK per ricaricare la pagina.<br/>Se stai usando Internet Explorer,<br/>ti consigliamo di usare un browser web sicuro e affidabile come Mozilla Firefox.",width:230,x:10,y:45},"class":LzText},{attrs:{$delegates:["onclick","$m972",null],clickable:true,x:250,y:60},"class":$lzc$class_m975}],"attributes",new LzInheritedHash(LzView.attributes)]);canvas.LzInstantiateView({attrs:{$lzc$bind_id:function($0,$1){
switch(arguments.length){
case 1:
$1=true;

};if($1){
$0.id="gError";gError=$0
}else if(gError===$0){
gError=null;$0.id=null
}},height:120,id:"gError",resource:"$LZ31",stretches:"both",title:void 0,visible:false,x:new LzOnceExpr("$m969"),y:new LzOnceExpr("$m970")},"class":$lzc$class_m973},5);lz["html"]=$lzc$class_html;lz["basefocusview"]=$lzc$class_basefocusview;lz["focusoverlay"]=$lzc$class_focusoverlay;lz["_componentmanager"]=$lzc$class__componentmanager;lz["style"]=$lzc$class_style;lz["statictext"]=$lzc$class_statictext;lz["basecomponent"]=$lzc$class_basecomponent;lz["basebutton"]=$lzc$class_basebutton;lz["basebuttonrepeater"]=$lzc$class_basebuttonrepeater;lz["basescrollbar"]=$lzc$class_basescrollbar;lz["basescrollthumb"]=$lzc$class_basescrollthumb;lz["basescrollarrow"]=$lzc$class_basescrollarrow;lz["basescrolltrack"]=$lzc$class_basescrolltrack;lz["stableborderlayout"]=$lzc$class_stableborderlayout;lz["hscrollbar"]=$lzc$class_hscrollbar;lz["swatchview"]=$lzc$class_swatchview;lz["button"]=$lzc$class_button;lz["basevaluecomponent"]=$lzc$class_basevaluecomponent;lz["baseformitem"]=$lzc$class_baseformitem;lz["listselector"]=$lzc$class_listselector;lz["datalistselector"]=$lzc$class_datalistselector;lz["baselist"]=$lzc$class_baselist;lz["basetrackgroup"]=$lzc$class_basetrackgroup;lz["vscrollbar"]=$lzc$class_vscrollbar;lz["simplelayout"]=$lzc$class_simplelayout;lz["list"]=$lzc$class_list;lz["baselistitem"]=$lzc$class_baselistitem;lz["listitem"]=$lzc$class_listitem;lz["textlistitem"]=$lzc$class_textlistitem;lz["basefloatinglist"]=$lzc$class_basefloatinglist;lz["_floatshadow"]=$lzc$class__floatshadow;lz["floatinglist"]=$lzc$class_floatinglist;lz["menutrackgroup"]=$lzc$class_menutrackgroup;lz["menubar"]=$lzc$class_menubar;lz["menuarrow"]=$lzc$class_menuarrow;lz["menubutton"]=$lzc$class_menubutton;lz["menuseparator"]=$lzc$class_menuseparator;lz["menuitem"]=$lzc$class_menuitem;lz["menufloatinglist"]=$lzc$class_menufloatinglist;lz["menu"]=$lzc$class_menu;lz["wrappinglayout"]=$lzc$class_wrappinglayout;lz["dragstate"]=$lzc$class_dragstate;lz["sText"]=$lzc$class_sText;lz["taglink"]=$lzc$class_taglink;lz["buddyicon"]=$lzc$class_buddyicon;lz["naturalimgview"]=$lzc$class_naturalimgview;lz["detailsview"]=$lzc$class_detailsview;lz["albumlayout"]=$lzc$class_albumlayout;lz["pivotlayout"]=$lzc$class_pivotlayout;lz["photo"]=$lzc$class_photo;lz["clipboardlayout"]=$lzc$class_clipboardlayout;lz["photocollection"]=$lzc$class_photocollection;lz["clipboardphoto"]=$lzc$class_clipboardphoto;lz["mybutton"]=$lzc$class_mybutton;lz["clipboard"]=$lzc$class_clipboard;lz["search"]=$lzc$class_search;lz["draggedphotos"]=$lzc$class_draggedphotos;lz["spinner"]=$lzc$class_spinner;lz["linkbutton"]=$lzc$class_linkbutton;canvas.initDone();